# Russian translations for maxi-liberation package
# Английские переводы для пакета maxi-liberation.
# Copyright (C) 2021 THE maxi-liberation'S COPYRIGHT HOLDER
# This file is distributed under the same license as the maxi-liberation package.
# НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: maxi-liberation 0.27\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-09 12:07+0300\n"
"PO-Revision-Date: 2021-02-16 01:12+0300\n"
"Last-Translator: НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>\n"
"Language-Team: Russian\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Prepare some translations
#: src/mvc/ctrl.pm:197
msgid "\\(different across \\d+ titles"
msgstr "\\(разное у \\d+ книг"

#. Regex for different titles
#: src/mvc/ctrl.pm:198
msgid "missing from \\d+ titles\\)"
msgstr "отсутствует у \\d+ книг\\)"

#: src/mvc/ctrl.pm:414 src/mvc/model.pm:265 src/mvc/model.pm:426
#: src/mvc/model.pm:452 src/mvc/model.pm:622 src/mvc/model.pm:914
#: src/mvc/model.pm:1074 src/mvc/model.pm:1082 src/mvc/model.pm:1291
#: src/mvc/model.pm:1301
msgid "Author"
msgstr "Автор"

#: src/mvc/ctrl.pm:418 src/mvc/model.pm:266 src/mvc/model.pm:427
#: src/mvc/model.pm:732 src/mvc/model.pm:915 src/mvc/model.pm:1075
#: src/mvc/model.pm:1083 src/mvc/model.pm:1320
msgid "Title"
msgstr "Заголовок"

#: src/mvc/ctrl.pm:426
msgid " and "
msgstr " и ещё "

#: src/mvc/ctrl.pm:426
msgid " more † "
msgstr " книг † "

#: src/mvc/ctrl.pm:526
msgid " titles"
msgstr " книг"

#: src/mvc/ctrl.pm:568 src/mvc/ctrl.pm:730
msgid "different across"
msgstr "разное у"

#: src/mvc/ctrl.pm:568 src/mvc/ctrl.pm:570 src/mvc/ctrl.pm:730
#: src/mvc/ctrl.pm:732
msgid "titles"
msgstr "книг"

#: src/mvc/ctrl.pm:570 src/mvc/ctrl.pm:732
msgid "missing from"
msgstr "отсутствует у"

#: src/mvc/ctrl.pm:1328
msgid "The value for the tag to be added cannot be empty"
msgstr "Значение добавляемого тага не может быть пустым"

#: src/mvc/ctrl.pm:1329 src/mvc/ctrl.pm:1415 src/mvc/ctrl.pm:1497
#: src/mvc/ctrl.pm:1566
msgid "API error"
msgstr "Ошибка API"

#: src/mvc/ctrl.pm:1409
msgid "The value for the tag to be edited cannot be empty"
msgstr "Значение редактируемого тага не может быть пустым"

#: src/mvc/ctrl.pm:1495 src/mvc/ctrl.pm:1564
msgid " already exists"
msgstr " уже существует"

#: src/mvc/ctrl.pm:1495 src/mvc/ctrl.pm:1564
msgid "The cover with name "
msgstr "Обложка с названием "

#: src/mvc/ctrl.pm:1496 src/mvc/ctrl.pm:1565
msgid " read error"
msgstr " нельзя прочесть"

#: src/mvc/ctrl.pm:1496 src/mvc/ctrl.pm:1565
msgid "The cover with filepath "
msgstr "Обложку с путём "

#: src/mvc/model.pm:220 src/mvc/model.pm:243 src/mvc/model.pm:316
#: src/mvc/model.pm:375 src/mvc/model.pm:564 src/mvc/model.pm:585
#: src/mvc/model.pm:767 src/mvc/model.pm:770 src/mvc/model.pm:811
msgid "ZIP extract error"
msgstr "Ошибка распаковки ZIP-архива"

#: src/mvc/model.pm:234
msgid "Can't find file with meta tags and covers"
msgstr "Не могу найти файл с метатагами и обложками"

#: src/mvc/model.pm:267 src/mvc/model.pm:453 src/mvc/model.pm:475
#: src/mvc/model.pm:643 src/mvc/model.pm:916 src/mvc/model.pm:1120
#: src/mvc/model.pm:1292
msgid "Publisher"
msgstr "Издатель"

#: src/mvc/model.pm:268 src/mvc/model.pm:476 src/mvc/model.pm:674
#: src/mvc/model.pm:917 src/mvc/model.pm:1135 src/mvc/model.pm:1330
msgid "ISBN"
msgstr ""

#: src/mvc/model.pm:269 src/mvc/model.pm:428 src/mvc/model.pm:454
#: src/mvc/model.pm:695 src/mvc/model.pm:918 src/mvc/model.pm:1102
#: src/mvc/model.pm:1343 src/mvc/model.pm:1346
msgid "Date"
msgstr "Дата"

#: src/mvc/model.pm:270 src/mvc/model.pm:429 src/mvc/model.pm:717
#: src/mvc/model.pm:919 src/mvc/model.pm:1076 src/mvc/model.pm:1084
#: src/mvc/model.pm:1361
msgid "Genre"
msgstr "Жанр"

#: src/mvc/model.pm:271 src/mvc/model.pm:430 src/mvc/model.pm:696
#: src/mvc/model.pm:733 src/mvc/model.pm:920 src/mvc/model.pm:1077
#: src/mvc/model.pm:1085 src/mvc/model.pm:1344 src/mvc/model.pm:1376
msgid "Language"
msgstr "Язык"

#: src/mvc/model.pm:272 src/mvc/model.pm:921
msgid "Contributor"
msgstr "Участник"

#: src/mvc/model.pm:273 src/mvc/model.pm:734 src/mvc/model.pm:922
#: src/mvc/model.pm:1377
msgid "Rights"
msgstr "Права"

#: src/mvc/model.pm:274 src/mvc/model.pm:431 src/mvc/model.pm:735
#: src/mvc/model.pm:923 src/mvc/model.pm:1078 src/mvc/model.pm:1378
msgid "Description"
msgstr "Описание"

#: src/mvc/model.pm:297 src/mvc/model.pm:305 src/mvc/model.pm:404
#: src/mvc/model.pm:511 src/mvc/model.pm:515 src/mvc/model.pm:799
#: src/mvc/model.pm:805
msgid "Cover read error"
msgstr "Ошибка чтения обложки"

#: src/mvc/model.pm:577
msgid "Can't find description file"
msgstr "Не могу найти файл описания"

#: src/mvc/model.pm:838
msgid "Unknown format"
msgstr "Неизвестный формат"

#: src/mvc/model.pm:883
msgid "Can't find root XML node package"
msgstr "Не могу найти корневой XML узел пакет"

#: src/mvc/model.pm:889
msgid "Can't find root XML node metadata"
msgstr "Не могу найти корневой XML узел метаданные"

#: src/mvc/model.pm:937
msgid "Can't find root XML node manifest"
msgstr "Не могу найти корневой XML узел манифест"

#: src/mvc/model.pm:1021 src/mvc/model.pm:1261 src/mvc/model.pm:1398
#: src/mvc/model.pm:1534
msgid "ZIP write error"
msgstr "Ошибка записи ZIP-архива"

#: src/mvc/model.pm:1034 src/mvc/model.pm:1038 src/mvc/model.pm:1417
#: src/mvc/model.pm:1420 src/mvc/model.pm:1423
msgid "Cover write error"
msgstr "Ошибка записи обложки"

#: src/mvc/model.pm:1280
msgid "Can't find root XML node description"
msgstr "Не могу найти корневой XML узел описание"

#: src/mvc/model.pm:1957
msgid "General error: "
msgstr "Общая ошибка: "

#: src/mvc/model.pm:1958
msgid "Problem creating directory "
msgstr "Ошибка при создании директории "

#: src/mvc/model.pm:1964
msgid "Move failed: "
msgstr "Перемещение не удалось: "

#: src/mvc/view.pm:372
msgid "Tag"
msgstr "Таг"

#: src/mvc/view.pm:373
msgid "Value"
msgstr "Значение"

#: src/mvc/view.pm:374 src/mvc/view.pm:375
msgid "Filepath"
msgstr "Путь файла"

#: src/mvc/view.pm:376
msgid "New Filepath"
msgstr "Новый путь файла"

#: src/mvc/view.pm:448
msgid "Error"
msgstr "Ошибка"

#: src/mvc/view.pm:460
msgid "GIF images (*.gif)|*.gif"
msgstr "Изображения GIF (*.gif)|*.gif"

#: src/mvc/view.pm:461
msgid "JPEG images (*.jpeg; *.jpg)|*.jpeg;*.jpg"
msgstr "Изображения JPEG (*.jpeg; *.jpg)|*.jpeg;*.jpg"

#: src/mvc/view.pm:462
msgid "PNG images (*.png)|*.png"
msgstr "Изображения PNG (*.png)|*.png"

#: src/mvc/view.pm:465
msgid "Select a image"
msgstr "Выбрать изображение"

#. Something different was passed.
#. Error occured.
#: src/mvc/view.pm:626
msgid "Wrong parameter"
msgstr "Неправильный параметр"

#. line 6 "maxi-liberation.xrc"
#: src/mvc/view.pm:672 ввод:2 стандартный
msgid "Maxi Liberation"
msgstr "Макси Воля"

#: src/mvc/view.pm:674
msgid ""
"EPUB/FB2/FB3 book meta tag editor\n"
"\n"
"Maxi means maximum expression of digital text form.\n"
"Liberation is library."
msgstr ""
"Редактор метатагов для электронных книг в форматах EPUB/FB2/FB3\n"
"\n"
"Макси означает максимальную выразительность цифровой текстовой формы.\n"
"Воля (liberation) в английском похоже на библиотеку (library)."

#: src/mvc/view.pm:675
msgid "The program web page"
msgstr "Веб-страница программы"

#: src/mvc/view.pm:676
msgid ""
"Copyright © 2021 НЕВСКИЙ БЛЯДИНА\n"
"<neva_blyad@lovecry.pt>\n"
"<neva_blyad@lovecri.es>"
msgstr ""
"Копирайт © 2021 НЕВСКИЙ БЛЯДИНА\n"
"<neva_blyad@lovecry.pt>\n"
"<neva_blyad@lovecri.es>"

#: src/mvc/view.pm:677
msgid "НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>"
msgstr ""

#: src/mvc/view.pm:678
msgid "Invisible Light"
msgstr "Незримый Свет"

#: src/mvc/view.pm:679
msgid "I wish there was somebody else..."
msgstr "хотелось бы мне увидеть здесь ещё кого-нибудь..."

#: src/mvc/view.pm:680
msgid "Unluckily"
msgstr "К сожалению,"

#: src/mvc/view.pm:681
msgid "none"
msgstr "никто,"

#: src/mvc/view.pm:682
msgid "so"
msgstr "так"

#: src/mvc/view.pm:683
msgid "the"
msgstr "что"

#: src/mvc/view.pm:684
msgid "graphic"
msgstr "графика"

#: src/mvc/view.pm:685
msgid "is"
msgstr "пока"

#: src/mvc/view.pm:686
msgid "bad"
msgstr "плохая"

#: src/mvc/view.pm:806
msgid "Image load error"
msgstr "Ошибка загрузки изображения"

#: src/mvc/view/lst_box.pm:205
msgid "Indexing is not supported yet"
msgstr "Выбор индекса пока не поддерживается"

#: src/mvc/view/lst_ctrl.pm:100
msgid "Can't add header row. It's already present."
msgstr "Не могу добавить строку заголовка. Она уже присутствует."

#: src/mvc/view/lst_ctrl.pm:121
msgid "Can't delete header row"
msgstr "Не могу удалить строку заголовка"

#: src/mvc/view/lst_ctrl.pm:228
msgid "No metadata can set for header row"
msgstr "Не могу установить метаданные для строки заголовка"

#: src/mvc/view/lst_ctrl.pm:252
msgid "No metadata should be set for header row"
msgstr "Не могу выделить строку заголовка"

#: src/mvc/view/lst_ctrl.pm:295
msgid "Can't set selection to header row"
msgstr "Не могу выделить строку заголовка"

#: src/mvc/view/lst_ctrl.pm:321
msgid "Can't get selection of header row"
msgstr "Не могу взять выделение строки заголовка"

#. line 79 "maxi-liberation.xrc"
#: ввод:4 стандартный
msgid "Show popup menu"
msgstr "Показать всплывающее меню"

#. line 89 "maxi-liberation.xrc"
#: ввод:6 стандартный
msgid "Scan the selected directory for books"
msgstr "Сканировать выделенную директорию на наличие книг"

#. line 90 "maxi-liberation.xrc"
#: ввод:8 стандартный
msgid "S&can"
msgstr "С&канировать"

#. line 103 "maxi-liberation.xrc"
#: ввод:10 стандартный
msgid "Enter the number of levels indicating how deep the scan will be proceed"
msgstr ""
"Введи количество уровней, которое будет использоваться в качестве глубины "
"сканирования"

#. line 112 "maxi-liberation.xrc"
#: ввод:12 стандартный
msgid "levels"
msgstr "уровней"

#. line 127 "maxi-liberation.xrc"
#: ввод:14 стандартный
msgid "0/0 titles"
msgstr "0/0 книг"

#. line 150 "maxi-liberation.xrc"
#: ввод:16 стандартный
msgid "Edit Tags"
msgstr "Редактировать таги"

#. line 176 "maxi-liberation.xrc"
#. line 654 "maxi-liberation.xrc"
#: ввод:18 ввод:154 стандартный
msgid "Add a new tag"
msgstr "Добавить новый таг"

#. line 177 "maxi-liberation.xrc"
#. line 496 "maxi-liberation.xrc"
#. line 655 "maxi-liberation.xrc"
#. line 837 "maxi-liberation.xrc"
#: ввод:20 ввод:104 ввод:156 ввод:216 стандартный
msgid "&Add"
msgstr "&Добавить"

#. line 189 "maxi-liberation.xrc"
#: ввод:22 стандартный
msgid "Edit the selected tags"
msgstr "Редактировать выделенные таги"

#. line 190 "maxi-liberation.xrc"
#. line 509 "maxi-liberation.xrc"
#. line 742 "maxi-liberation.xrc"
#. line 932 "maxi-liberation.xrc"
#: ввод:24 ввод:108 ввод:194 ввод:238 стандартный
msgid "&Edit"
msgstr "&Редактировать"

#. line 202 "maxi-liberation.xrc"
#: ввод:26 стандартный
msgid "Remove the selected tags"
msgstr "Удалить выделенные таги"

#. line 203 "maxi-liberation.xrc"
#. line 522 "maxi-liberation.xrc"
#: ввод:28 ввод:112 стандартный
msgid "&Remove"
msgstr "&Удалить"

#. line 221 "maxi-liberation.xrc"
#: ввод:30 стандартный
msgid "Save tags to the filesystem"
msgstr "Сохранить таги на диск"

#. line 222 "maxi-liberation.xrc"
#. line 318 "maxi-liberation.xrc"
#. line 414 "maxi-liberation.xrc"
#. line 541 "maxi-liberation.xrc"
#: ввод:32 ввод:64 ввод:96 ввод:116 стандартный
msgid "&Save"
msgstr "&Сохранить"

#. line 234 "maxi-liberation.xrc"
#: ввод:34 стандартный
msgid "Tags From Filepath"
msgstr "Таги из путей файлов"

#. line 251 "maxi-liberation.xrc"
#. line 347 "maxi-liberation.xrc"
#: ввод:36 ввод:68 стандартный
msgid "Pattern:"
msgstr "Шаблон:"

#. line 261 "maxi-liberation.xrc"
#. line 357 "maxi-liberation.xrc"
#: ввод:38 ввод:70 стандартный
msgid ""
"Enter absolute or relative filepath with inserted tags in angle brackets "
"into it, e. g. ~/Books/<Author>/<Title>"
msgstr ""
"Введи абсолютный или относительный путь файла и вставь в него таги в угловых "
"скобках, напр., ~/Книги/<Автор>/<Заголовок>"

#. line 264 "maxi-liberation.xrc"
#. line 360 "maxi-liberation.xrc"
#. line 606 "maxi-liberation.xrc"
#. line 693 "maxi-liberation.xrc"
#: ввод:40 ввод:72 ввод:128 ввод:166 стандартный
msgid "<Title>"
msgstr "<Заголовок>"

#. line 265 "maxi-liberation.xrc"
#. line 361 "maxi-liberation.xrc"
#: ввод:42 ввод:74 стандартный
msgid "<Author> - <Title>"
msgstr "<Автор> - <Заголовок>"

#. line 266 "maxi-liberation.xrc"
#. line 362 "maxi-liberation.xrc"
#: ввод:44 ввод:76 стандартный
msgid "(<Date>) <Title>"
msgstr "(<Дата>) Заголовок"

#. line 267 "maxi-liberation.xrc"
#. line 363 "maxi-liberation.xrc"
#: ввод:46 ввод:78 стандартный
msgid "(<Date>) <Author> - <Title>"
msgstr "(<Дата>) <Автор> - <Заголовок>"

#. line 268 "maxi-liberation.xrc"
#. line 364 "maxi-liberation.xrc"
#: ввод:48 ввод:80 стандартный
msgid "(<Date>) <Title> [<Publisher> - <ISBN>]"
msgstr "(<Дата>) <Заголовок> [<Издатель> - <ISBN>]"

#. line 269 "maxi-liberation.xrc"
#. line 365 "maxi-liberation.xrc"
#: ввод:50 ввод:82 стандартный
msgid "(<Date>) <Author> - <Title> [<Publisher> - <ISBN>]"
msgstr "(<Дата>) <Автор> - <Заголовок> [<Издатель> - <ISBN>]"

#. line 270 "maxi-liberation.xrc"
#. line 366 "maxi-liberation.xrc"
#: ввод:52 ввод:84 стандартный
msgid "<Author>/<Title>"
msgstr "<Автор>/<Заголовок>"

#. line 271 "maxi-liberation.xrc"
#. line 367 "maxi-liberation.xrc"
#: ввод:54 ввод:86 стандартный
msgid "<Author>/(<Date>) <Title>"
msgstr "<Автор>/(<Дата>) <Заголовок>"

#. line 272 "maxi-liberation.xrc"
#. line 368 "maxi-liberation.xrc"
#: ввод:56 ввод:88 стандартный
msgid "<Author>/(<Date>) <Title> [<Publisher> - <ISBN>]"
msgstr "<Автор>/(<Дата>) <Заголовок> [<Издатель> - <ISBN>]"

#. line 304 "maxi-liberation.xrc"
#: ввод:58 стандартный
msgid "Show new tags according to the pattern with filepath"
msgstr "Показать новые таги в соответствии с шаблоном пути файла"

#. line 305 "maxi-liberation.xrc"
#. line 401 "maxi-liberation.xrc"
#: ввод:60 ввод:92 стандартный
msgid "&Preview"
msgstr "&Предпросмотр"

#. line 317 "maxi-liberation.xrc"
#: ввод:62 стандартный
msgid ""
"Save new tags to the filesystem. The tags will be added to existing ones."
msgstr "Сохранить новые таги на диск. Таги будут добавлены к существующим."

#. line 330 "maxi-liberation.xrc"
#: ввод:66 стандартный
msgid "Filepath From Tags"
msgstr "Пути файлов из тагов"

#. line 400 "maxi-liberation.xrc"
#: ввод:90 стандартный
msgid "Show new filepaths according to the pattern with tags"
msgstr "Показать новые пути файлов в соответствии с шаблоном тагов"

#. line 413 "maxi-liberation.xrc"
#: ввод:94 стандартный
msgid "Move the files to their new paths, creating directories if necessary"
msgstr ""
"Переместить файлы согласно их новым путям, создавать директории при "
"необходимости"

#. line 426 "maxi-liberation.xrc"
#. line 847 "maxi-liberation.xrc"
#: ввод:98 ввод:218 стандартный
msgid "Edit Cover"
msgstr "Редактировать обложку"

#. line 479 "maxi-liberation.xrc"
#: ввод:100 стандартный
msgid ""
"Note: changing the name or removing a cover may affect the book content!"
msgstr ""
"Примечание: изменение названия или удаление обложки может повлиять на "
"содержимое книги!"

#. line 495 "maxi-liberation.xrc"
#. line 836 "maxi-liberation.xrc"
#: ввод:102 ввод:214 стандартный
msgid "Add a new cover"
msgstr "Добавить новую обложку"

#. line 508 "maxi-liberation.xrc"
#. line 931 "maxi-liberation.xrc"
#: ввод:106 ввод:236 стандартный
msgid "Edit the selected cover"
msgstr "Редактировать выделенную обложку"

#. line 521 "maxi-liberation.xrc"
#: ввод:110 стандартный
msgid "Remove the selected cover"
msgstr "Удалить выделенную обложку"

#. line 540 "maxi-liberation.xrc"
#: ввод:114 стандартный
msgid "Save covers to the filesystem"
msgstr "Сохранить обложки на диск"

#. line 560 "maxi-liberation.xrc"
#: ввод:118 стандартный
msgid "Plugins"
msgstr "Плагины"

#. line 578 "maxi-liberation.xrc"
#: ввод:120 стандартный
msgid "Add Tag"
msgstr "Добавить таг"

#. line 592 "maxi-liberation.xrc"
#. line 679 "maxi-liberation.xrc"
#: ввод:122 ввод:160 стандартный
msgid "Tag:"
msgstr "Таг:"

#. line 602 "maxi-liberation.xrc"
#: ввод:124 стандартный
msgid "Enter or choose the name of the tag to be added"
msgstr "Введи или выбери имя добавляемого тага"

#. line 605 "maxi-liberation.xrc"
#. line 692 "maxi-liberation.xrc"
#: ввод:126 ввод:164 стандартный
msgid "<Author>"
msgstr "<Автор>"

#. line 607 "maxi-liberation.xrc"
#. line 694 "maxi-liberation.xrc"
#: ввод:130 ввод:168 стандартный
msgid "<Publisher>"
msgstr "<Издатель>"

#. line 608 "maxi-liberation.xrc"
#. line 695 "maxi-liberation.xrc"
#: ввод:132 ввод:170 стандартный
msgid "<ISBN>"
msgstr ""

#. line 609 "maxi-liberation.xrc"
#. line 696 "maxi-liberation.xrc"
#: ввод:134 ввод:172 стандартный
msgid "<Date>"
msgstr "<Дата>"

#. line 610 "maxi-liberation.xrc"
#. line 697 "maxi-liberation.xrc"
#: ввод:136 ввод:174 стандартный
msgid "<Genre>"
msgstr "<Жанр>"

#. line 611 "maxi-liberation.xrc"
#. line 698 "maxi-liberation.xrc"
#: ввод:138 ввод:176 стандартный
msgid "<Language>"
msgstr "<Язык>"

#. line 612 "maxi-liberation.xrc"
#. line 699 "maxi-liberation.xrc"
#: ввод:140 ввод:178 стандартный
msgid "<Contributor>"
msgstr "<Участник>"

#. line 613 "maxi-liberation.xrc"
#. line 700 "maxi-liberation.xrc"
#: ввод:142 ввод:180 стандартный
msgid "<Rights>"
msgstr "<Права>"

#. line 614 "maxi-liberation.xrc"
#. line 701 "maxi-liberation.xrc"
#: ввод:144 ввод:182 стандартный
msgid "<Description>"
msgstr "<Описание>"

#. line 623 "maxi-liberation.xrc"
#. line 710 "maxi-liberation.xrc"
#: ввод:146 ввод:184 стандартный
msgid "Value:"
msgstr "Значение:"

#. line 633 "maxi-liberation.xrc"
#: ввод:148 стандартный
msgid "Enter the value for the tag to be added"
msgstr "Введи значение добавляемого тага"

#. line 642 "maxi-liberation.xrc"
#. line 729 "maxi-liberation.xrc"
#. line 824 "maxi-liberation.xrc"
#. line 919 "maxi-liberation.xrc"
#: ввод:150 ввод:188 ввод:210 ввод:232 стандартный
msgid "Return to the main window"
msgstr "Вернуться в главное окно"

#. line 643 "maxi-liberation.xrc"
#. line 730 "maxi-liberation.xrc"
#. line 825 "maxi-liberation.xrc"
#. line 920 "maxi-liberation.xrc"
#: ввод:152 ввод:190 ввод:212 ввод:234 стандартный
msgid "&Cancel"
msgstr "&Отмена"

#. line 665 "maxi-liberation.xrc"
#: ввод:158 стандартный
msgid "Edit Tag"
msgstr "Редактировать таг"

#. line 689 "maxi-liberation.xrc"
#: ввод:162 стандартный
msgid "Enter or choose the new name of the tag to be edited"
msgstr "Введи или выбери новое имя редактируемого тага"

#. line 720 "maxi-liberation.xrc"
#: ввод:186 стандартный
msgid "Enter the new value for the tag to be edited"
msgstr "Введи новое значение редактируемого тага"

#. line 741 "maxi-liberation.xrc"
#: ввод:192 стандартный
msgid "Edit the selected tag"
msgstr "Редактировать выделенный таг"

#. line 752 "maxi-liberation.xrc"
#: ввод:196 стандартный
msgid "Add Cover"
msgstr "Добавить обложку"

#. line 766 "maxi-liberation.xrc"
#. line 861 "maxi-liberation.xrc"
#: ввод:198 ввод:220 стандартный
msgid "Name:"
msgstr "Название:"

#. line 776 "maxi-liberation.xrc"
#: ввод:200 стандартный
msgid "Enter the name of the cover to be added"
msgstr "Введи название добавляемой обложки"

#. line 785 "maxi-liberation.xrc"
#. line 880 "maxi-liberation.xrc"
#: ввод:202 ввод:224 стандартный
msgid "Filepath:"
msgstr "Путь файла:"

#. line 801 "maxi-liberation.xrc"
#: ввод:204 стандартный
msgid "Enter the filepath to the cover to be added"
msgstr "Введи путь к файлу добавляемой обложки"

#. line 810 "maxi-liberation.xrc"
#: ввод:206 стандартный
msgid "Browse the filepath to the cover to be added"
msgstr "Выбери путь к файлу добавляемой обложки"

#. line 811 "maxi-liberation.xrc"
#. line 906 "maxi-liberation.xrc"
#: ввод:208 ввод:230 стандартный
msgid "&Browse..."
msgstr "О&бзор..."

#. line 871 "maxi-liberation.xrc"
#: ввод:222 стандартный
msgid "Enter the new name of the cover to be edited"
msgstr "Введи новое название редактируемой обложки"

#. line 896 "maxi-liberation.xrc"
#: ввод:226 стандартный
msgid "Enter the new filepath to the cover to be edited"
msgstr "Введи новый путь к файлу редактируемой обложки"

#. line 905 "maxi-liberation.xrc"
#: ввод:228 стандартный
msgid "Browse the new filepath to the cover to be edited"
msgstr "Выбрать новый путь к файлу редактируемой обложки"

#. line 943 "maxi-liberation.xrc"
#: ввод:240 стандартный
msgid "&Plugins"
msgstr "&Плагины"

#. line 948 "maxi-liberation.xrc"
#: ввод:242 стандартный
msgid "A&bout"
msgstr "&О программе"
