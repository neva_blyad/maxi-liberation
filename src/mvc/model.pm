=pod

=encoding UTF-8

=head1 NAME

Maxi Liberation
model.pm

=head1 VERSION

0.27

=head1 DESCRIPTION

EPUB/FB2/FB3 book meta tag editor.

This is the Model module.
This is where the data and logic is.
Nothing GUI oriented here.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This file is part of Maxi Liberation.

Maxi Liberation is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maxi Liberation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare Model package
package model;

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# Use experimental switch-case construct
use v5.14;
use feature qw(switch);
no warnings qw(experimental);

# Some I/O functions should fuck up everything
use autodie qw(chdir chmod open close);

# CPAN modules
use Archive::Zip;
use Cwd;
use Data::Dumper;
use Digest::SHA;
use File::Basename;
use File::Copy;
use File::Path;
use File::Spec::Functions;
use File::Temp;
use MIME::Base64;
use MIME::Types;
use Path::Tiny;
use Scalar::Util;
use Try::Tiny;
use XML::LibXML;

# Path to binary
my $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib $path;
use lib File::Spec::Functions::catfile($path, '..', 'external');

# Our own modules
#use view; # View. Model should not interact with View.
use ctrl; # Controller

# Constants
use constant TITLE_SAVE_TAGS_ONLY       => 0;
use constant TITLE_SAVE_COVERS_ONLY     => 1;
use constant TITLE_SAVE_TAGS_AND_COVERS => 2;

use constant RES_OK             => 0;
use constant RES_EMPTY          => 1;
use constant RES_EXISTS_ALREADY => 2;
use constant RES_IO_ERR         => 3;

use constant FORMAT_EPUB => 0;
use constant FORMAT_FB2  => 2;
use constant FORMAT_FB3  => 3;

use constant SHA_ALG => 256; # Use SHA256 for cover checksums

# Global Variables
our $dir;
my  $tmp;
our %titles;
our @tags_all;
our @covers_all;

################################################################################

sub preinit
{
}

################################################################################

sub init
{
    my $working_dir = $ctrl::use_prefix ?
        File::Spec::Functions::catfile($ctrl::prefix, 'share', 'maxi-liberation') :
        File::Spec::Functions::catfile($path, '..', '..');

    # Change directory to the base path
    try   { chdir $working_dir;       }
    catch { $ctrl::logger->error($_); };

    # Do not print any errors with unzipping
    sub err_show {};
    Archive::Zip::setErrorHandler(\&err_show);

    # Create temporary directory
    $tmp = File::Temp->newdir('TEMPLATE' => 'maxi-liberationXXXX',
                              'TMPDIR'   => 1,
                              'CLEANUP'  => 1,
                              'UNLINK'   => 1);
}

################################################################################

sub title_load
{
    my $filepath = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });

    sub tmp_create
    {
        my ($filename_no_ext, undef, undef);

        my $filepath = shift;

        # Parse filepath.
        #
        # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
        ($filename_no_ext, undef, undef) = File::Basename::fileparse($filepath, qr/\.[^.]*/);
        $ctrl::logger->debug('$filename_no_ext = ', sub { Data::Dumper::Dumper($filename_no_ext) });

        # Create temporary directory
        $titles{$filepath}{'tmp'} = File::Temp->newdir('TEMPLATE' => "${filename_no_ext}XXXX",
                                                       'DIR'      => $tmp,
                                                       'CLEANUP'  => 1,
                                                       'UNLINK'   => 1);
        $ctrl::logger->debug(q($titles{$filepath}{'tmp'} = ), sub { Data::Dumper::Dumper($titles{$filepath}{'tmp'}) });
    }

    # EPUB format load handling subroutine
    sub epub_load
    {
        my $filepath_abs;
        my  @tags;
        my  @covers;
        my ($container, $container_abs);
        my ($attr_path);

        my $filepath = shift;

        # Create temporary directory
        tmp_create($filepath);

        # Extract ZIP archive
        $filepath_abs = File::Spec::Functions::catfile($dir, $filepath);
        $ctrl::logger->debug('$filepath_abs = ', sub { Data::Dumper::Dumper($filepath_abs) });

        $titles{$filepath}{'zip'} = Archive::Zip->new();
        $titles{$filepath}{'zip'}->read($filepath_abs) == Archive::Zip->AZ_OK or
            return;

        # Extract the top-level XML file into the temporary directory
        $container     = File::Spec::Functions::catfile('META-INF', 'container.xml');
        $container_abs = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $container);
        $ctrl::logger->debug('$container = ',    sub { Data::Dumper::Dumper($container)     });
        $ctrl::logger->debug('$container_abs =', sub { Data::Dumper::Dumper($container_abs) });

        $titles{$filepath}{'zip'}->memberNamed($container) or
            return;
        $titles{$filepath}{'zip'}->extractMemberWithoutPaths($container, $container_abs) == Archive::Zip->AZ_OK or
            die _T('ZIP extract error');

        # Find file with meta tags and covers in top-level XML
        chmod 0644, $container_abs;
        $titles{$filepath}{'opf'}{'dom'} = XML::LibXML->load_xml('location'     => $container_abs,
                                                                 'load_ext_dtd' => 0);
        $ctrl::logger->debug($titles{$filepath}{'opf'}{'dom'}->toString(0));
        $titles{$filepath}{'opf'}{'xpc'} = XML::LibXML::XPathContext->new($titles{$filepath}{'opf'}{'dom'});
        $titles{$filepath}{'opf'}{'xpc'}->registerNs('main' => 'urn:oasis:names:tc:opendocument:xmlns:container');

        ($attr_path) = $titles{$filepath}{'opf'}{'xpc'}->findnodes(q(/*[local-name() = 'container' and position() = 1]/
                                                                      *[local-name() = 'rootfiles' and position() = 1]/
                                                                      *[local-name() = 'rootfile'  and position() = 1]/@full-path));
        defined $attr_path or
            die _T("Can't find file with meta tags and covers");
        $titles{$filepath}{'opf'}{'filepath'} = $attr_path->textContent();
        $titles{$filepath}{'opf'}{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $titles{$filepath}{'opf'}{'filepath'});
        $ctrl::logger->debug(q($titles{$filepath}{'opf'}{'filepath'} = ),     sub { Data::Dumper::Dumper($titles{$filepath}{'opf'}{'filepath'    }) });
        $ctrl::logger->debug(q($titles{$filepath}{'opf'}{'filepath_abs'} = ), sub { Data::Dumper::Dumper($titles{$filepath}{'opf'}{'filepath_abs'}) });

        # Extract the file with meta tags and covers into the temporary directory
        $titles{$filepath}{'zip'}->extractMemberWithoutPaths($titles{$filepath}{'opf'}{'filepath'    },
                                                             $titles{$filepath}{'opf'}{'filepath_abs'}) == Archive::Zip->AZ_OK or
            die _T('ZIP extract error');

        # Parse meta tags in this file
        chmod 0644, $titles{$filepath}{'opf'}{'filepath_abs'};
        $titles{$filepath}{'opf'}{'dom'} = XML::LibXML->load_xml('location'     => $titles{$filepath}{'opf'}{'filepath_abs'},
                                                                 'load_ext_dtd' => 0);
        $ctrl::logger->debug($titles{$filepath}{'opf'}{'dom'}->toString(0));
        $titles{$filepath}{'opf'}{'xpc'} = XML::LibXML::XPathContext->new($titles{$filepath}{'opf'}{'dom'});
        $titles{$filepath}{'opf'}{'xpc'}->registerNs('main' => 'http://www.idpf.org/2007/opf');

        # Find out meta tags from XML
        foreach my $tag ($titles{$filepath}{'opf'}{'xpc'}->findnodes("/*[local-name() = 'package'  and position() = 1]/
                                                                       *[local-name() = 'metadata' and position() = 1]/
                                                                       *[name() != 'meta']"))
        {
            my %tag;

            my $name = $tag->nodeName();
            my $val  = $tag->textContent();

            given ($name)
            {
                $name = _T('Author'     ) when 'dc:creator';
                $name = _T('Title'      ) when 'dc:title';
                $name = _T('Publisher'  ) when 'dc:publisher';
                $name = _T('ISBN'       ) when 'dc:identifier';
                $name = _T('Date'       ) when 'dc:date';
                $name = _T('Genre'      ) when 'dc:subject';
                $name = _T('Language'   ) when 'dc:language';
                $name = _T('Contributor') when 'dc:contributor';
                $name = _T('Rights'     ) when 'dc:rights';
                $name = _T('Description') when 'dc:description';
            }

            $tag{'name'} = $name;
            $tag{'val' } = $val;

            push @tags, \%tag;
        }

        $ctrl::logger->debug(q(\@tags = ), sub { Data::Dumper::Dumper(\@tags) });

        # Find out the covers from XML
        foreach my $meta ($titles{$filepath}{'opf'}{'xpc'}->findnodes(q(/*[local-name() = 'package'  and position() = 1]/
                                                                         *[local-name() = 'metadata' and position() = 1]/
                                                                         *[name() = 'meta' and
                                                                           @name  = 'cover'])))
        {
            my %cover;
            my $attr_href;
            my $sha;

            $cover{'name'} = $meta->getAttribute('content');
            defined $cover{'name'} or
                die _T('Cover read error');
            $ctrl::logger->debug(q($cover{'name'} = ), sub { Data::Dumper::Dumper($cover{'name'}) });

            ($attr_href) = $titles{$filepath}{'opf'}{'xpc'}->findnodes("/*[local-name() = 'package' and position() = 1]/
                                                                         *[local-name() = 'manifest']/
                                                                         *[local-name() = 'item' and \@id = '$cover{'name'}']/
                                                                         \@href");
            defined $attr_href or
                die _T('Cover read error');
            $attr_href = $attr_href->textContent();
            $ctrl::logger->debug(q($attr_href = ), sub { Data::Dumper::Dumper($attr_href) });

            # Extract the cover into the temporary directory
            $cover{'filepath'    } = Path::Tiny::path(File::Spec::Functions::catfile(File::Basename::dirname($titles{$filepath}{'opf'}{'filepath'}), $attr_href))->canonpath();
            $cover{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $cover{'filepath'});
            $ctrl::logger->debug(q($cover{'filepath'} = ),     sub { Data::Dumper::Dumper($cover{'filepath'    }) });
            $ctrl::logger->debug(q($cover{'filepath_abs'} = ), sub { Data::Dumper::Dumper($cover{'filepath_abs'}) });

            $titles{$filepath}{'zip'}->extractMemberWithoutPaths($cover{'filepath'}, $cover{'filepath_abs'}) == Archive::Zip->AZ_OK or
                die _T('ZIP extract error');

            # Calculate its SHA checksum
            $sha = Digest::SHA->new(SHA_ALG);
            $sha->addfile($cover{'filepath_abs'});
            $cover{'checksum'} = $sha->hexdigest();
            $ctrl::logger->debug(q($cover{'checksum'} = ), sub { Data::Dumper::Dumper($cover{'checksum'}) });

            push @covers, \%cover;
        }

        $ctrl::logger->debug(q(\@covers = ), sub { Data::Dumper::Dumper(\@covers) });

        # Remember tags and covers for the future
        $titles{$filepath}{'format'} = FORMAT_EPUB;
        $titles{$filepath}{'tags'  } = \@tags;
        $titles{$filepath}{'covers'} = \@covers;
    }

    # FB2 format load handling subroutine
    sub fb2_load
    {
        my $filepath_abs;
        my $res;
        my $fb;
        my @tags;
        my @covers;

        my $filepath = shift;

        # Extract ZIP archive (if book is zipped)
        $filepath_abs = File::Spec::Functions::catfile($dir, $filepath);
        $ctrl::logger->debug('$filepath_abs = ', sub { Data::Dumper::Dumper($filepath_abs) });

        $titles{$filepath}{'zip'} = Archive::Zip->new();

        if ($titles{$filepath}{'zip'}->read($filepath_abs) == Archive::Zip->AZ_OK)
        {
            my ($filename_no_ext, undef, undef);
            my ($book, $book_abs);

            # Create temporary directory
            tmp_create($filepath);

            # Parse filepath.
            #
            # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
            ($filename_no_ext, undef, undef) = File::Basename::fileparse($filepath_abs, qr/\.[^.]*/);
            $ctrl::logger->debug('$filename_no_ext = ', sub { Data::Dumper::Dumper($filename_no_ext) });

            # Extract the top-level file with book into the temporary directory
            $titles{$filepath}{'book'}{'filepath'    } = $filename_no_ext;
            $titles{$filepath}{'book'}{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $titles{$filepath}{'book'}{'filepath'});
            $ctrl::logger->debug(q($titles{$filepath}{'book'}{'filepath'    } = ), sub { Data::Dumper::Dumper($titles{$filepath}{'book'}{'filepath'    }) });
            $ctrl::logger->debug(q($titles{$filepath}{'book'}{'filepath_abs'} = ), sub { Data::Dumper::Dumper($titles{$filepath}{'book'}{'filepath_abs'}) });

            $titles{$filepath}{'zip'}->memberNamed($titles{$filepath}{'book'}{'filepath'}) or
                return;
            $titles{$filepath}{'zip'}->extractMemberWithoutPaths($titles{$filepath}{'book'}{'filepath'}, $titles{$filepath}{'book'}{'filepath_abs'}) == Archive::Zip->AZ_OK or
                die _T('ZIP extract error');
        }
        else
        {
            delete $titles{$filepath}{'zip'};
            $titles{$filepath}{'book'}{'filepath_abs'} = $filepath_abs;
        }

        chmod 0644, $titles{$filepath}{'book'}{'filepath_abs'};

        # Parse meta tags
        try
        {
            $titles{$filepath}{'book'}{'dom'} = XML::LibXML->load_xml('location'     => $titles{$filepath}{'book'}{'filepath_abs'},
                                                                      'load_ext_dtd' => 0);
        }
        catch
        {
            $res = 1;
        };

        return if $res;

        $ctrl::logger->debug($titles{$filepath}{'book'}{'dom'}->toString(0));
        $titles{$filepath}{'book'}{'xpc'} = XML::LibXML::XPathContext->new($titles{$filepath}{'book'}{'dom'});
        $titles{$filepath}{'book'}{'xpc'}->registerNs('main' => 'http://www.gribuser.ru/xml/fictionbook/2.0');

        ($fb) = $titles{$filepath}{'book'}{'xpc'}->findnodes("/*[local-name() = 'FictionBook' and position() = 1]");
        defined $fb or
            die _T('Cover read error');

        # Find out meta tags from XML
        foreach my $node ($fb->findnodes("*[local-name() = 'description']/
                                          *"))
        {
            my %tag;
            my $name = $node->nodeName();

            given ($name)
            {
                when ('title-info')
                {
                    foreach my $node_ ($node->findnodes('*'))
                    {
                        my %tag_;
                        my $name_ = $node_->nodeName();

                        next if $name_ eq 'coverpage';

                        given ($name_)
                        {
                            $name_ = _T('Author'     ) when 'author';
                            $name_ = _T('Title'      ) when 'book-title';
                            $name_ = _T('Date'       ) when 'date';
                            $name_ = _T('Genre'      ) when 'genre';
                            $name_ = _T('Language'   ) when 'lang';
                            $name_ = _T('Description') when 'annotation';
                        }

                        $tag_{'name'} = $name_;
                        $tag_{'val' } = $node_->textContent();

                        push @tags, \%tag_;
                    }

                    next;
                }

                when ('document-info')
                {
                    foreach my $node_ ($node->findnodes('*'))
                    {
                        my %tag_;
                        my $name_ = $node_->nodeName();

                        given ($name_)
                        {
                            $name_ = _T('Author'   ) when 'author';
                            $name_ = _T('Publisher') when 'publisher';
                            $name_ = _T('Date'     ) when 'date';
                        }

                        $tag_{'name'} = $name_;
                        $tag_{'val' } = $node_->textContent();

                        push @tags, \%tag_;
                    }

                    next;
                }

                when ('publish-info')
                {
                    foreach my $node_ ($node->findnodes('*'))
                    {
                        my %tag_;
                        my $name_ = $node_->nodeName();

                        given ($name_)
                        {
                            $name_ = _T('Publisher') when 'publisher';
                            $name_ = _T('ISBN'     ) when 'isbn';
                        }

                        $tag_{'name'} = $name_;
                        $tag_{'val' } = $node_->textContent();

                        push @tags, \%tag_;
                    }

                    next;
                }
            }

            $tag{'name'} = $name;
            $tag{'val' } = $node->textContent();

            push @tags, \%tag;
        }

        # Find out the covers from XML
        foreach my $node ($fb->findnodes(q(*[local-name() = 'description']/
                                           *[local-name() = 'title-info']/
                                           *[local-name() = 'coverpage']/
                                           *[local-name() = 'image']/
                                           @l:href)))
        {
            my $bin;
            my %cover;
            my $sha;

            $cover{'name'} = substr $node->textContent(), 1;
            $ctrl::logger->debug(q($cover{'name'} = ), sub { Data::Dumper::Dumper($cover{'name'}) });

            ($bin) = $fb->findnodes("*[local-name() = 'binary' and \@id = '$cover{'name'}']");
            defined $bin or
                die _T('Cover read error');

            $cover{'type'} = $bin->getAttribute('content-type');
            defined $cover{'type'} or
                die _T('Cover read error');
            $ctrl::logger->debug(q($cover{'type'} = ), sub { Data::Dumper::Dumper($cover{'type'}) });

            # Calculate its SHA checksum
            $sha = Digest::SHA->new(SHA_ALG);
            $sha->add($cover{'bin'});
            $cover{'checksum'} = $sha->hexdigest();
            $ctrl::logger->debug(q($cover{'checksum'} = ), sub { Data::Dumper::Dumper($cover{'checksum'}) });

            push @covers, \%cover;
        }

        # Remember tags and covers for the future
        $titles{$filepath}{'format'} = FORMAT_FB2;
        $titles{$filepath}{'tags'  } = \@tags;
        $titles{$filepath}{'covers'} = \@covers;
    }

    # FB3 format load handling subroutine
    sub fb3_load
    {
        my  $filepath_abs;
        my  $attr_target;
        my  @tags;
        my  @covers;
        my ($filename_no_ext, $dir_rel, $ext);

        my $filepath = shift;

        # Create temporary directory
        tmp_create($filepath);

        # Extract ZIP archive
        $filepath_abs = File::Spec::Functions::catfile($dir, $filepath);
        $ctrl::logger->debug('$filepath_abs = ', sub { Data::Dumper::Dumper($filepath_abs) });

        $titles{$filepath}{'zip'} = Archive::Zip->new();
        $titles{$filepath}{'zip'}->read($filepath_abs) == Archive::Zip->AZ_OK or
            return;

        # Extract the top-level XML file into the temporary directory
        $titles{$filepath}{'rels'}{'filepath'    } = File::Spec::Functions::catfile('_rels', '.rels');
        $titles{$filepath}{'rels'}{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $titles{$filepath}{'rels'}{'filepath'});
        $ctrl::logger->debug("$titles{$filepath}{'rels'}{'filepath'} = ",     sub { Data::Dumper::Dumper($titles{$filepath}{'rels'}{'filepath'    }) });
        $ctrl::logger->debug("$titles{$filepath}{'rels'}{'filepath_abs'} = ", sub { Data::Dumper::Dumper($titles{$filepath}{'rels'}{'filepath_abs'}) });

        $titles{$filepath}{'zip'}->memberNamed($titles{$filepath}{'rels'}{'filepath'}) or
            return;
        $titles{$filepath}{'zip'}->extractMemberWithoutPaths($titles{$filepath}{'rels'}{'filepath'}, $titles{$filepath}{'rels'}{'filepath_abs'}) == Archive::Zip->AZ_OK or
            die _T('ZIP extract error');

        # Find file with meta tags in top-level XML
        chmod 0644, $titles{$filepath}{'rels'}{'filepath_abs'};
        $titles{$filepath}{'rels'}{'dom'} = XML::LibXML->load_xml('location'     => $titles{$filepath}{'rels'}{'filepath_abs'},
                                                                  'load_ext_dtd' => 0);
        $ctrl::logger->debug($titles{$filepath}{'rels'}{'dom'}->toString(0));
        $titles{$filepath}{'rels'}{'xpc'} = XML::LibXML::XPathContext->new($titles{$filepath}{'rels'}{'dom'});
        $titles{$filepath}{'rels'}{'xpc'}->registerNs('main' => 'http://schemas.openxmlformats.org/package/2006/relationships');

        ($attr_target) = $titles{$filepath}{'rels'}{'xpc'}->findnodes(q(/*[local-name() = 'Relationships' and position() = 1]/
                                                                         *[local-name() = 'Relationship'  and @Type = 'http://www.fictionbook.org/FictionBook3/relationships/Book']/@Target));
        defined $attr_target or
            die _T("Can't find description file");
        $titles{$filepath}{'desc'}{'filepath'    } = $attr_target->textContent();
        $titles{$filepath}{'desc'}{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $titles{$filepath}{'desc'}{'filepath'});
        $ctrl::logger->debug(q($titles{$filepath}{'desc'}{'filepath'} = ),     sub { Data::Dumper::Dumper($titles{$filepath}{'desc'}{'filepath'    }) });
        $ctrl::logger->debug(q($titles{$filepath}{'desc'}{'filepath_abs'} = ), sub { Data::Dumper::Dumper($titles{$filepath}{'desc'}{'filepath_abs'}) });

        # Extract the file with meta tags into the temporary directory
        $titles{$filepath}{'zip'}->extractMemberWithoutPaths($titles{$filepath}{'desc'}{'filepath'}, $titles{$filepath}{'desc'}{'filepath_abs'}) == Archive::Zip->AZ_OK or
            die _T('ZIP extract error');

        # Parse meta tags in this file
        chmod 0644, $titles{$filepath}{'desc'}{'filepath_abs'};
        $titles{$filepath}{'desc'}{'dom'} = XML::LibXML->load_xml('location'     => $titles{$filepath}{'desc'}{'filepath_abs'},
                                                                  'load_ext_dtd' => 0);
        $ctrl::logger->debug($titles{$filepath}{'desc'}{'dom'}->toString(0));
        $titles{$filepath}{'desc'}{'xpc'} = XML::LibXML::XPathContext->new($titles{$filepath}{'desc'}{'dom'});
        $titles{$filepath}{'desc'}{'xpc'}->registerNs('main' => 'http://www.fictionbook.org/FictionBook3/description');

        # Find out meta tags from XML
        foreach my $node ($titles{$filepath}{'desc'}{'xpc'}->findnodes("/*[local-name() = 'fb3-description' and position() = 1]/*"))
        {
            my %tag;
            my $name = $node->nodeName();

            given ($name)
            {
                when ('fb3-relations')
                {
                    foreach my $node_ ($node->findnodes('*'))
                    {
                        my %tag_;
                        my $attr_link = $node_->getAttribute('link');
                        my $name_     = $node_->nodeName();

                        given ($attr_link)
                        {
                            when ('author')
                            {
                                foreach my $node__ ($node_->findnodes('*'))
                                {
                                    my %tag__;
                                    my $name__ = $node__->nodeName();

                                    given ($name__)
                                    {
                                        $name__ = _T('Author') when 'title';
                                    }

                                    $tag__{'name'} = $name__;
                                    $tag__{'val' } = $node__->textContent();

                                    push @tags, \%tag__;
                                }

                                next;
                            }

                            when ('publisher')
                            {
                                foreach my $node__ ($node_->findnodes('*'))
                                {
                                    my %tag__;
                                    my $name__ = $node__->nodeName();

                                    given ($name__)
                                    {
                                        $name__ = _T('Publisher') when 'title';
                                    }

                                    $tag__{'name'} = $name__;
                                    $tag__{'val' } = $node__->textContent();

                                    push @tags, \%tag__;
                                }

                                next;
                            }
                        }

                        $tag_{'name'} = $name_;
                        $tag_{'val' } = $node_->textContent();

                        push @tags, \%tag_;
                    }

                    next;
                }

                when ('document-info')
                {
                    foreach my $node_ ($node->findnodes('*'))
                    {
                        my %tag_;
                        my $name_ = $node_->nodeName();

                        given ($name_)
                        {
                            $name_ = _T('ISBN') when 'isbn';
                        }

                        $tag_{'name'} = $name_;
                        $tag_{'val' } = $node_->textContent();

                        push @tags, \%tag_;
                    }

                    next;
                }

                when ('written')
                {
                    foreach my $node_ ($node->findnodes('*'))
                    {
                        my %tag_;
                        my $name_ = $node_->nodeName();

                        given ($name_)
                        {
                            $name_ = _T('Date'    ) when 'date';
                            $name_ = _T('Language') when 'lang';
                        }

                        $tag_{'name'} = $name_;
                        $tag_{'val' } = $node_->textContent();

                        push @tags, \%tag_;
                    }

                    next;
                }

                when ('fb3-classification')
                {
                    foreach my $node_ ($node->findnodes('*'))
                    {
                        my %tag_;
                        my $name_ = $node_->nodeName();

                        given ($name_)
                        {
                            $name_ = _T('Genre') when 'subject';
                        }

                        $tag_{'name'} = $name_;
                        $tag_{'val' } = $node_->textContent();

                        push @tags, \%tag_;
                    }

                    next;
                }
            }

            given ($name)
            {
                $name = _T('Title'      ) when 'title';
                $name = _T('Language'   ) when 'lang';
                $name = _T('Rights'     ) when 'copyrights';
                $name = _T('Description') when 'annotation';
            }

            $tag{'name'} = $name;
            $tag{'val' } = $node->textContent();

            push @tags, \%tag;
        }

        # Parse filepath.
        #
        # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
        ($filename_no_ext, $dir_rel, $ext) = File::Basename::fileparse($titles{$filepath}{'desc'}{'filepath'}, qr/\.[^.]*/);
        $ctrl::logger->debug('$filename_no_ext = ', sub { Data::Dumper::Dumper($filename_no_ext) });
        $ctrl::logger->debug('$dir_rel = ',         sub { Data::Dumper::Dumper($dir_rel        ) });
        $ctrl::logger->debug('$ext = ',             sub { Data::Dumper::Dumper($ext            ) });

        # Determine the file with cover links (1)
        $titles{$filepath}{'desc_rels'}{'filepath'    } = Path::Tiny::path(File::Spec::Functions::catfile($dir_rel, '_rels', "$filename_no_ext$ext.rels"))->canonpath();
        $titles{$filepath}{'desc_rels'}{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $titles{$filepath}{'desc_rels'}{'filepath'});
        $ctrl::logger->debug(q($titles{$filepath}{'desc_rels'}{'filepath'} = ),     sub { Data::Dumper::Dumper($titles{$filepath}{'desc_rels'}{'filepath'    }) });
        $ctrl::logger->debug(q($titles{$filepath}{'desc_rels'}{'filepath_abs'} = ), sub { Data::Dumper::Dumper($titles{$filepath}{'desc_rels'}{'filepath_abs'}) });

        # Determine the file with cover links (2)
        $titles{$filepath}{'body_rels'}{'filepath'    } = Path::Tiny::path(File::Spec::Functions::catfile($dir_rel, '_rels', "body.xml.rels"))->canonpath();
        $titles{$filepath}{'body_rels'}{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $titles{$filepath}{'body_rels'}{'filepath'});
        $ctrl::logger->debug(q($titles{$filepath}{'body_rels'}{'filepath'} = ),     sub { Data::Dumper::Dumper($titles{$filepath}{'body_rels'}{'filepath'    }) });
        $ctrl::logger->debug(q($titles{$filepath}{'body_rels'}{'filepath_abs'} = ), sub { Data::Dumper::Dumper($titles{$filepath}{'body_rels'}{'filepath_abs'}) });

        # Extract the files with cover links into the temporary directory
        $titles{$filepath}{'zip'}->extractMemberWithoutPaths($titles{$filepath}{'desc_rels'}{'filepath'    },
                                                             $titles{$filepath}{'desc_rels'}{'filepath_abs'}) == Archive::Zip->AZ_OK or
            die _T('ZIP extract error');
        $titles{$filepath}{'zip'}->extractMemberWithoutPaths($titles{$filepath}{'body_rels'}{'filepath'    },
                                                             $titles{$filepath}{'body_rels'}{'filepath_abs'}) == Archive::Zip->AZ_OK or
            die _T('ZIP extract error');

        # Parse the file with cover links (1)
        chmod 0644, $titles{$filepath}{'desc_rels'}{'filepath_abs'};
        $titles{$filepath}{'desc_rels'}{'dom'} = XML::LibXML->load_xml('location'     => $titles{$filepath}{'desc_rels'}{'filepath_abs'},
                                                                       'load_ext_dtd' => 0);
        $ctrl::logger->debug($titles{$filepath}{'desc_rels'}{'dom'}->toString(0));
        $titles{$filepath}{'desc_rels'}{'xpc'} = XML::LibXML::XPathContext->new($titles{$filepath}{'desc_rels'}{'dom'});
        $titles{$filepath}{'desc_rels'}{'xpc'}->registerNs('main' => 'http://schemas.openxmlformats.org/package/2006/relationships');

        # Parse the file with cover links (2)
        chmod 0644, $titles{$filepath}{'body_rels'}{'filepath_abs'};
        $titles{$filepath}{'body_rels'}{'dom'} = XML::LibXML->load_xml('location'     => $titles{$filepath}{'body_rels'}{'filepath_abs'},
                                                                       'load_ext_dtd' => 0);
        $ctrl::logger->debug($titles{$filepath}{'body_rels'}{'dom'}->toString(0));
        $titles{$filepath}{'body_rels'}{'xpc'} = XML::LibXML::XPathContext->new($titles{$filepath}{'body_rels'}{'dom'});
        $titles{$filepath}{'body_rels'}{'xpc'}->registerNs('main' => 'http://schemas.openxmlformats.org/package/2006/relationships');

        $ctrl::logger->debug(q(\@tags = ), sub { Data::Dumper::Dumper(\@tags) });

        # Find out the title covers from XML
        foreach my $rel ($titles{$filepath}{'rels'}{'xpc'}->findnodes(q(/*[local-name() = 'Relationships' and position() = 1]/
                                                                         *[local-name() = 'Relationship'  and @Type = 'http://schemas.openxmlformats.org/package/2006/relationships/metadata/thumbnail'])))
        {
            my %cover;
            my $sha;

            $cover{'name'} = $rel->getAttribute('Id');
            defined $cover{'name'} or
                die _T('Cover read error');
            $ctrl::logger->debug(q($cover{'name'} = ), sub { Data::Dumper::Dumper($cover{'name'}) });

            # Extract the cover into the temporary directory
            $cover{'filepath'} = $rel->getAttribute('Target');
            defined $cover{'filepath'} or
                die _T('Cover read error');
            $cover{'filepath_abs'} = File::Spec::Functions::catfile($titles{$filepath}{'tmp'}, $cover{'filepath'});
            $ctrl::logger->debug(q($cover{'filepath'} = ),     sub { Data::Dumper::Dumper($cover{'filepath'    }) });
            $ctrl::logger->debug(q($cover{'filepath_abs'} = ), sub { Data::Dumper::Dumper($cover{'filepath_abs'}) });

            $titles{$filepath}{'zip'}->extractMemberWithoutPaths($cover{'filepath'}, $cover{'filepath_abs'}) == Archive::Zip->AZ_OK or
                die _T('ZIP extract error');

            # Calculate its SHA checksum
            $sha = Digest::SHA->new(SHA_ALG);
            $sha->addfile($cover{'filepath_abs'});
            $cover{'checksum'} = $sha->hexdigest();
            $ctrl::logger->debug(q($cover{'checksum'} = ), sub { Data::Dumper::Dumper($cover{'checksum'}) });

            push @covers, \%cover;
        }

        $ctrl::logger->debug(q(\@covers = ), sub { Data::Dumper::Dumper(\@covers) });

        # Remember tags and covers for the future
        $titles{$filepath}{'format'} = FORMAT_FB3;
        $titles{$filepath}{'tags'  } = \@tags;
        $titles{$filepath}{'covers'} = \@covers;
    }

    try
    {
        # We don't know the file format yet.
        # Try different handlers.
        epub_load($filepath);
        fb2_load ($filepath)     unless exists $titles{$filepath}{'format'};
        fb3_load ($filepath)     unless exists $titles{$filepath}{'format'};

        die _T('Unknown format') unless exists $titles{$filepath}{'format'};

        $ctrl::logger->debug(q(\$titles{$filepath} = ), sub { Data::Dumper::Dumper(\$titles{$filepath}) });
    }
    catch
    {
        delete $titles{$filepath};

        $ctrl::logger->warn($_);
    };
}

################################################################################

sub title_unload
{
    my $filepath = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });

    # Drop title
    delete $titles{$filepath};
}

################################################################################

sub title_save
{
    my $filepath = shift;
    my $behavior = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });
    $ctrl::logger->debug('$behavior = ', sub { Data::Dumper::Dumper($behavior) });

    # EPUB format save handling subroutine
    sub epub_save
    {
        my ($pkg, $metadata, $manifest);

        my $filepath = shift;
        my $behavior = shift;

        # Add opf, dc namespaces if missing
        ($pkg) = $titles{$filepath}{'opf'}{'xpc'}->findnodes("/*[local-name() = 'package' and position() = 1]");
        defined $pkg or
            die _T("Can't find root XML node package");
        $pkg->setNamespace('http://www.idpf.org/2007/opf', 'opf', 0);
        $pkg->setNamespace('http://purl.org/dc/elements/1.1/', 'dc', 0);

        ($metadata) = $pkg->findnodes("*[local-name() = 'metadata' and position() = 1]");
        defined $metadata or
            die _T("Can't find root XML node metadata");
        $metadata->setNamespaceDeclURI('opf', undef);
        $metadata->setNamespaceDeclURI('dc', undef);

        # Save tags
        if ($behavior == TITLE_SAVE_TAGS_ONLY ||
            $behavior == TITLE_SAVE_TAGS_AND_COVERS)
        {
            # Remove old meta tags
            foreach my $tag ($metadata->findnodes(q(*[not (name() = 'meta' and
                                                           @name  = 'cover')])))
            {
                $metadata->removeChild($tag);
            }

            # Add the new ones
            foreach my $tag (@{$titles{$filepath}{'tags'}})
            {
                my $name = $tag->{'name'};
                my $val  = $tag->{'val'};

                next if $name =~ /[<>&'" ]/;

                given ($name)
                {
                    $name = 'dc:creator'     when $_ eq _T('Author'     );
                    $name = 'dc:title'       when $_ eq _T('Title'      );
                    $name = 'dc:publisher'   when $_ eq _T('Publisher'  );
                    $name = 'dc:identifier'  when $_ eq _T('ISBN'       );
                    $name = 'dc:date'        when $_ eq _T('Date'       );
                    $name = 'dc:subject'     when $_ eq _T('Genre'      );
                    $name = 'dc:language'    when $_ eq _T('Language'   );
                    $name = 'dc:contributor' when $_ eq _T('Contributor');
                    $name = 'dc:rights'      when $_ eq _T('Rights'     );
                    $name = 'dc:description' when $_ eq _T('Description');
                }

                $metadata->appendTextChild($name, $val);
            }
        }

        # Save covers
        if ($behavior == TITLE_SAVE_COVERS_ONLY ||
            $behavior == TITLE_SAVE_TAGS_AND_COVERS)
        {
            # Remove old covers
            ($manifest) = $pkg->findnodes("*[local-name() = 'manifest']");
            defined $manifest or
                die _T("Can't find root XML node manifest");
            $manifest->setNamespaceDeclURI('opf', undef);
            $manifest->setNamespaceDeclURI('dc', undef);

            foreach my $meta ($metadata->findnodes(q(*[name() = 'meta' and
                                                       @name  = 'cover'])))
            {
                my  $attr_content;
                my ($item);
                my  $attr_href;
                my  $cover;

                # Remove cover from XML (1)
                $metadata->removeChild($meta);

                $attr_content = $meta->getAttribute('content');
                defined $attr_content or
                    $ctrl::logger->debug('Cover write error') and
                    next;
                $ctrl::logger->debug(q(\$attr_content = ), sub { Data::Dumper::Dumper($attr_content) });

                ($item) = $manifest->findnodes("*[local-name() = 'item' and \@id = '$attr_content']");
                defined $item or
                    $ctrl::logger->debug('Cover write error') and
                    next;

                $attr_href = $item->getAttribute('href');
                defined $attr_href or
                    $ctrl::logger->debug('Cover write error') and
                    next;
                $ctrl::logger->debug(q(\$attr_href = ), sub { Data::Dumper::Dumper($attr_href) });

                # Remove cover from XML (2)
                $manifest->removeChild($item);

                # Remove cover from ZIP
                $cover = Path::Tiny::path(File::Spec::Functions::catfile(File::Basename::dirname($titles{$filepath}{'opf'}{'filepath'}), $attr_href))->canonpath();
                $titles{$filepath}{'zip'}->removeMember($cover);
            }

            # Add the new ones
            foreach my $cover (@{$titles{$filepath}{'covers'}})
            {
                my $opf_dir   = File::Basename::dirname($titles{$filepath}{'opf'}{'filepath'});
                my $meta      = XML::LibXML::Element->new('meta');
                my $item      = XML::LibXML::Element->new('item');
                my $href_attr = $cover->{'filepath'};
                my $mime      = MIME::Types->new();
                my $type_attr = $mime->mimeTypeOf($cover->{'filepath_abs'});

                $href_attr =~ s|^\Q$opf_dir\E/||i;
                $href_attr =~ s|^\Q$opf_dir\E\\||i;

                $meta->setAttribute('name',    'cover');
                $meta->setAttribute('content', $cover->{'name'});

                $item->setAttribute('id',         $cover->{'name'});
                $item->setAttribute('properties', $cover->{'name'});
                $item->setAttribute('href',       $href_attr);
                $item->setAttribute('media-type', $type_attr);

                # Add cover to XML
                $metadata->addChild($meta);
                $manifest->addChild($item);

                # Archive cover from the temporary directory
                $titles{$filepath}{'zip'}->addFile($cover->{'filepath_abs'}, $cover->{'filepath'});
            }
        }

        # Save file with meta tags and covers on disk
        $titles{$filepath}{'opf'}{'dom'}->toFile($titles{$filepath}{'opf'}{'filepath_abs'}, 0);
        $ctrl::logger->debug($titles{$filepath}{'opf'}{'dom'}->toString(0));

        # Archive file with meta tags and covers from the temporary directory.
        #
        # (Cannot use updateMember() instead of removeMember() and
        # addFile() methods, don't know why.)
        #$titles{$filepath}{'zip'}->updateMember($titles{$filepath}{'opf'}{'filepath'}, $titles{$filepath}{'opf'}{'filepath_abs'});
        $titles{$filepath}{'zip'}->removeMember($titles{$filepath}{'opf'}{'filepath'});
        $titles{$filepath}{'zip'}->addFile($titles{$filepath}{'opf'}{'filepath_abs'}, $titles{$filepath}{'opf'}{'filepath'});

        # Repack ZIP archive
        $titles{$filepath}{'zip'}->overwrite() == Archive::Zip->AZ_OK or
            die _T('ZIP write error');
    }

    # F3B format save handling subroutine
    sub fb2_save
    {
        my ($fb, $desc, $title_info);

        my $filepath = shift;
        my $behavior = shift;

        ($fb) = $titles{$filepath}{'book'}{'xpc'}->findnodes("/*[local-name() = 'FictionBook' and position() = 1]");
        defined $fb or
            die _T('Cover write error');
        ($desc) = $fb->findnodes("/*[local-name() = 'FictionBook' and position() = 1]/
                                   *[local-name() = 'description']");
        defined $desc or
            die _T('Cover write error');

        ($title_info) = $desc->findnodes("*[local-name() = 'title-info']");

        # Save tags
        if ($behavior == TITLE_SAVE_TAGS_ONLY ||
            $behavior == TITLE_SAVE_TAGS_AND_COVERS)
        {
            my ($title_info, $doc_info, $publish_info);

            # Remove old meta tags
            foreach my $node ($desc->findnodes("*"))
            {
                if ($node->nodeName() eq 'title-info')
                {
                    $title_info = $node;

                    foreach my $node_ ($node->findnodes("*[local-name() != 'coverpage']"))
                    {
                        $node->removeChild($node_);
                    }
                }
                else
                {
                    $desc->removeChild($node);
                }
            }

            # Add the new ones
            foreach my $tag (@{$titles{$filepath}{'tags'}})
            {
                my $name = $tag->{'name'};
                my $val  = $tag->{'val'};

                next if $name =~ /[<>&'" ]/;

                if ($name eq _T('Author'     ) ||
                    $name eq _T('Title'      ) ||
                    $name eq _T('Genre'      ) ||
                    $name eq _T('Language'   ) ||
                    $name eq _T('Description'))
                {
                    given ($name)
                    {
                        $name = 'author'     when $_ eq _T('Author'     );
                        $name = 'book-title' when $_ eq _T('Title'      );
                        $name = 'genre'      when $_ eq _T('Genre'      );
                        $name = 'lang'       when $_ eq _T('Language'   );
                        #$name = 'annotation' when $_ eq _T('Description');

                        default { $name = 'annotation'; }
                    }

                    unless (defined $title_info)
                    {
                        $title_info = XML::LibXML::Element->new('title-info');
                        $desc->addChild($title_info);
                    }

                    $title_info->appendTextChild($name, $val);

                    next;
                }

                if ($name eq _T('Date'))
                {
                    my $date = XML::LibXML::Element->new('date');

                    $date->setAttribute('value', $val);
                    $date->appendText($val);

                    unless (defined $title_info)
                    {
                        $title_info = XML::LibXML::Element->new('title-info');
                        $desc->addChild($title_info);
                    }

                    $title_info->addChild($date);

                    next;
                }

                if ($name eq _T('Publisher'))
                {
                    $name = 'publisher';

                    unless (defined $doc_info)
                    {
                        $doc_info = XML::LibXML::Element->new('document-info');
                        $desc->addChild($doc_info);
                    }

                    $doc_info->appendTextChild($name, $val);

                    next;
                }

                if ($name eq _T('ISBN'))
                {
                    $name = 'isbn';

                    unless (defined $publish_info)
                    {
                        $publish_info = XML::LibXML::Element->new('publish-info');
                        $desc->addChild($publish_info);
                    }

                    $publish_info->appendTextChild($name, $val);

                    next;
                }

                unless (defined $doc_info)
                {
                    $doc_info = XML::LibXML::Element->new('document-info');
                    $desc->addChild($doc_info);
                }

                $doc_info->appendTextChild($name, $val);
            }
        }

        # Save covers
        if ($behavior == TITLE_SAVE_COVERS_ONLY ||
            $behavior == TITLE_SAVE_TAGS_AND_COVERS)
        {
            # Remove old covers
            if (defined $title_info)
            {
                foreach my $cover ($title_info->findnodes("*[local-name() = 'coverpage']"))

                {
                    foreach my $img ($cover->findnodes(q(*[local-name() = 'image']/
                                                         @l:href)))
                    {
                        my $name = substr $img->textContent(), 1;

                        foreach my $bin ($fb->findnodes("*[local-name() = 'binary' and \@id = '$name']"))
                        {
                            $fb->removeChild($bin);
                        }
                    }

                    $title_info->removeChild($cover);
                }
            }
            else
            {
                $title_info = XML::LibXML::Element->new('title-info');
                $desc->addChild($title_info);
            }

            # Add the new ones
            foreach my $cover (@{$titles{$filepath}{'covers'}})
            {
                my $err;
                my $img;
                my $cover_;

                my $bin = XML::LibXML::Element->new('binary');

                if (exists $cover->{'filepath_abs'})
                {
                    my $mime;
                    my $type_attr;

                    try
                    {
                        local $/;

                        no warnings 'once'; # Don't know why this error appears
                        open FILE, '< :encoding(UTF-8)', $cover->{'filepath_abs'};
                        $bin->appendText(MIME::Base64::encode_base64(<FILE>));
                        close FILE;
                    }
                    catch
                    {
                        $ctrl::logger->warn($_);
                        $err = 1;
                    };

                    next if $err;

                    $mime      = MIME::Types->new();
                    $type_attr = $mime->mimeTypeOf($cover->{'filepath_abs'});

                    $bin->setAttribute('content-type', $type_attr);
                }
                else
                {
                    $bin->setAttribute('content-type', $cover->{'type'});
                    $bin->appendText(MIME::Base64::encode_base64($cover->{'bin'}));
                }

                $bin->setAttribute('id', $cover->{'name'});
                $fb->addChild($bin);

                $img    = XML::LibXML::Element->new('image');
                $cover_ = XML::LibXML::Element->new('coverpage');

                $img->setAttribute('l:href', "#$cover->{'name'}");
                $cover_->addChild($img);
                $title_info->addChild($cover_);
            }
        }

        # Save file with meta tags on disk
        $titles{$filepath}{'book'}{'dom'}->toFile($titles{$filepath}{'book'}{'filepath_abs'}, 0);
        $ctrl::logger->debug($titles{$filepath}{'book'}{'dom'}->toString(0));

        # Pack ZIP archive (if book is zipped)
        if (exists $titles{$filepath}{'zip'})
        {
            # Archive the file with meta tags from the temporary directory.
            #
            # (Cannot use updateMember() instead of removeMember() and
            # addFile() methods, don't know why.)
            #$titles{$filepath}{'zip'}->updateMember($titles{$filepath}{'book'}{'filepath'}, $titles{$filepath}{'book'}{'filepath_abs'});
            $titles{$filepath}{'zip'}->removeMember($titles{$filepath}{'book'}{'filepath'});
            $titles{$filepath}{'zip'}->addFile($titles{$filepath}{'book'}{'filepath_abs'}, $titles{$filepath}{'book'}{'filepath'});

            # Repack ZIP archive
            $titles{$filepath}{'zip'}->overwrite() == Archive::Zip->AZ_OK or
                die _T('ZIP write error');
        }
    }

    # F3B format save handling subroutine
    sub fb3_save
    {
        my $filepath = shift;
        my $behavior = shift;

        # Save tags
        if ($behavior == TITLE_SAVE_TAGS_ONLY ||
            $behavior == TITLE_SAVE_TAGS_AND_COVERS)
        {
            my ($desc, $rels, $info, $written_node, $class);

            # Remove old meta tags
            ($desc) = $titles{$filepath}{'desc'}{'xpc'}->findnodes("/*[local-name() = 'fb3-description' and position() = 1]");
            defined $desc or
                die _T("Can't find root XML node description");
            $desc->removeChildNodes();

            # Add the new ones
            foreach my $tag (@{$titles{$filepath}{'tags'}})
            {
                my $name = $tag->{'name'};
                my $val  = $tag->{'val'};

                next if $name =~ /[<>&'" ]/;

                if ($name eq _T('Author') ||
                    $name eq _T('Publisher'))
                {
                    my $title;
                    my $attr_link;
                    my $subj;

                    $title = XML::LibXML::Element->new('title');
                    $title->appendTextChild('main', $val);

                    $attr_link = ($name eq _T('Author')) ?
                        'author' :
                        'publisher';

                    $subj = XML::LibXML::Element->new('subject');
                    $subj->setAttribute('link', $attr_link);
                    $subj->addChild($title);

                    unless (defined $rels)
                    {
                        $rels = XML::LibXML::Element->new('fb3-relations');
                        $desc->addChild($rels);
                    }

                    $rels->addChild($subj);

                    next;
                }

                if ($name eq _T('Title'))
                {
                    my $title = XML::LibXML::Element->new('title');

                    $title->appendTextChild('main', $val);
                    $desc->addChild($title);

                    next;
                }

                if ($name eq _T('ISBN'))
                {
                    unless (defined $info)
                    {
                        $info = XML::LibXML::Element->new('document-info');
                        $desc->addChild($info);
                    }

                    $info->appendTextChild('isbn', $val);

                    next;
                }

                if ($name eq _T('Date'    ) ||
                    $name eq _T('Language'))
                {
                    $name = ($name eq _T('Date')) ?
                        'date' :
                        'lang';

                    unless (defined $written_node)
                    {
                        $written_node = XML::LibXML::Element->new('written');
                        $desc->addChild($written_node);
                    }

                    $written_node->appendTextChild($name, $val);

                    next;
                }

                if ($name eq _T('Genre'))
                {
                    unless (defined $class)
                    {
                        $class = XML::LibXML::Element->new('fb3-classification');
                        $desc->addChild($class);
                    }

                    $class->appendTextChild('subject', $val);

                    next;
                }

                given ($name)
                {
                    $name = 'lang'       when $_ eq _T('Language'   );
                    $name = 'copyrights' when $_ eq _T('Rights'     );
                    $name = 'annotation' when $_ eq _T('Description');
                }

                $desc->appendTextChild($name, $val);
            }

            # Save file with meta tags on disk
            $titles{$filepath}{'desc'}{'dom'}->toFile($titles{$filepath}{'desc'}{'filepath_abs'}, 0);
            $ctrl::logger->debug($titles{$filepath}{'desc'}{'dom'}->toString(0));

            # Archive the file with meta tags from the temporary directory.
            #
            # (Cannot use updateMember() instead of removeMember() and
            # addFile() methods, don't know why.)
            #$titles{$filepath}{'zip'}->updateMember($titles{$filepath}{'desc'}{'filepath'}, $titles{$filepath}{'desc'}{'filepath_abs'});
            $titles{$filepath}{'zip'}->removeMember($titles{$filepath}{'desc'}{'filepath'});
            $titles{$filepath}{'zip'}->addFile($titles{$filepath}{'desc'}{'filepath_abs'}, $titles{$filepath}{'desc'}{'filepath'});

            # Repack ZIP archive
            $titles{$filepath}{'zip'}->overwrite() == Archive::Zip->AZ_OK or
                die _T('ZIP write error');
        }

        # Save covers
        if ($behavior == TITLE_SAVE_COVERS_ONLY ||
            $behavior == TITLE_SAVE_TAGS_AND_COVERS)
        {
            my  $dir_rel;
            my ($have_changed_rels, $have_changed_rels_, $have_changed_rels__);
            my ($rels, $rels_, $rels__);

            # Parse filepath.
            #
            # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
            (undef, $dir_rel, undef) = File::Basename::fileparse($titles{$filepath}{'desc'}{'filepath'}, qr/\.[^.]*/);

            # Remove old covers
            ($rels) = $titles{$filepath}{'rels'}{'xpc'}->findnodes("/*[local-name() = 'Relationships' and position() = 1]");
            defined $rels or
                die _T('Cover write error');
            ($rels_) = $titles{$filepath}{'desc_rels'}{'xpc'}->findnodes("/*[local-name() = 'Relationships' and position() = 1]");
            defined $rels_ or
                die _T('Cover write error');
            ($rels__) = $titles{$filepath}{'body_rels'}{'xpc'}->findnodes("/*[local-name() = 'Relationships' and position() = 1]");
            defined $rels__ or
                die _T('Cover write error');

            foreach my $rel ($rels->findnodes(q(*[local-name() = 'Relationship' and @Type = 'http://schemas.openxmlformats.org/package/2006/relationships/metadata/thumbnail'])))
            {
                my $attr_id;
                my $attr_target;

                # Remove cover from XML (1)
                $rels->removeChild($rel);

                # Remove cover from XML (2)
                $attr_id = $rel->getAttribute('Id');
                $ctrl::logger->debug(q($attr_id = ), sub { Data::Dumper::Dumper($attr_id) });

                if (defined $attr_id)
                {
                    foreach my $rel_ ($rels_->findnodes("*[local-name() = 'Relationship' and \@Id = '$attr_id']"))
                    {
                        $rels_->removeChild($rel_);
                        $have_changed_rels_ = 1;
                    }
                }

                # Remove cover from ZIP
                $attr_target = $rel->getAttribute('Target');
                defined $attr_target or
                    $ctrl::logger->debug('Cover write error') and
                    next;

                $titles{$filepath}{'zip'}->removeMember($attr_target);

                # Remove cover from XML (3)
                $attr_target =~ s|^\Q$dir_rel\E||i;

                foreach my $rel__ ($rels__->findnodes("*[local-name() = 'Relationship' and \@Target = '$attr_target']"))
                {
                    $rels__->removeChild($rel__);
                    $have_changed_rels__ = 1;
                }

                $have_changed_rels = 1;
            }

            # Add the new ones
            foreach my $cover (@{$titles{$filepath}{'covers'}})
            {
                my $attr_target;

                my $rel   = XML::LibXML::Element->new('Relationship');
                my $rel_  = XML::LibXML::Element->new('Relationship');
                my $rel__ = XML::LibXML::Element->new('Relationship');

                # Add cover to XML (1)
                $rel->setAttribute('Id',     $cover->{'name'});
                $rel->setAttribute('Type',   'http://schemas.openxmlformats.org/package/2006/relationships/metadata/thumbnail');
                $rel->setAttribute('Target', $cover->{'filepath'});

                $rels->addChild($rel);

                # Add cover to XML (2)
                $rel_->setAttribute('Id',     $cover->{'name'});
                $rel_->setAttribute('Type',   'http://www.fictionbook.org/FictionBook3/relationships/body');
                $rel_->setAttribute('Target', 'body.xml');

                $rels_->addChild($rel_);

                # Add cover to XML (3)
                $attr_target =  $cover->{'filepath'};
                $attr_target =~ s|^\Q$dir_rel\E||i;

                $rel__->setAttribute('Id',     $cover->{'name'});
                $rel__->setAttribute('Type',   'http://www.fictionbook.org/FictionBook3/relationships/image');
                $rel__->setAttribute('Target', $attr_target);

                $rels__->addChild($rel__);

                # Archive cover from the temporary directory
                $titles{$filepath}{'zip'}->addFile($cover->{'filepath_abs'}, $cover->{'filepath'});

                $have_changed_rels   = 1;
                $have_changed_rels_  = 1;
                $have_changed_rels__ = 1;
            }

            # Save the top-level XML file on disk.
            # Save the files with cover links on disk.
            $titles{$filepath}{'rels'     }{'dom'}->toFile($titles{$filepath}{'rels'     }{'filepath_abs'}, 0) if $have_changed_rels;
            $titles{$filepath}{'desc_rels'}{'dom'}->toFile($titles{$filepath}{'desc_rels'}{'filepath_abs'}, 0) if $have_changed_rels_;
            $titles{$filepath}{'body_rels'}{'dom'}->toFile($titles{$filepath}{'body_rels'}{'filepath_abs'}, 0) if $have_changed_rels__;
            $ctrl::logger->debug($titles{$filepath}{'rels'     }{'dom'}->toString(0));
            $ctrl::logger->debug($titles{$filepath}{'desc_rels'}{'dom'}->toString(0));
            $ctrl::logger->debug($titles{$filepath}{'body_rels'}{'dom'}->toString(0));

            # Archive the top-level XML file from the temporary directory.
            # Archive the files with cover links from the temporary directory.
            #
            # (Cannot use updateMember() instead of removeMember() and
            # addFile() methods, don't know why.)
            #$titles{$filepath}{'zip'}->updateMember($titles{$filepath}{'rels'     }{'filepath'}, $titles{$filepath}{'rels'     }{'filepath_abs'}) if $have_changed_rels;
            #$titles{$filepath}{'zip'}->updateMember($titles{$filepath}{'rels_desc'}{'filepath'}, $titles{$filepath}{'rels_desc'}{'filepath_abs'}) if $have_changed_rels_;
            $titles{$filepath}{'zip'}->removeMember($titles{$filepath}{'rels'     }{'filepath'}) if $have_changed_rels;
            $titles{$filepath}{'zip'}->removeMember($titles{$filepath}{'desc_rels'}{'filepath'}) if $have_changed_rels_;
            $titles{$filepath}{'zip'}->removeMember($titles{$filepath}{'body_rels'}{'filepath'}) if $have_changed_rels__;
            $titles{$filepath}{'zip'}->addFile($titles{$filepath}{'rels'     }{'filepath_abs'}, $titles{$filepath}{'rels'     }{'filepath'}) if $have_changed_rels;
            $titles{$filepath}{'zip'}->addFile($titles{$filepath}{'desc_rels'}{'filepath_abs'}, $titles{$filepath}{'desc_rels'}{'filepath'}) if $have_changed_rels_;
            $titles{$filepath}{'zip'}->addFile($titles{$filepath}{'body_rels'}{'filepath_abs'}, $titles{$filepath}{'body_rels'}{'filepath'}) if $have_changed_rels__;

            # Repack ZIP archive
            if ($have_changed_rels || $have_changed_rels_ || $have_changed_rels__)
            {
                $titles{$filepath}{'zip'}->overwrite() == Archive::Zip->AZ_OK or
                    die _T('ZIP write error');
            }
        }
    }

    try
    {
        # Call save handler according to the file format
        given ($titles{$filepath}{'format'})
        {
            epub_save($filepath, $behavior) when $_ == FORMAT_EPUB;
            fb2_save ($filepath, $behavior) when $_ == FORMAT_FB2;
            fb3_save ($filepath, $behavior) when $_ == FORMAT_FB3;
        }
    }
    catch
    {
        $ctrl::logger->error($_);
    };
}

################################################################################

sub filepath_rel_get
{
    my $filepath_rel;

    my $name_with_ext = shift;
    my $filepath      = shift;

    $ctrl::logger->debug('$name      = ', sub { Data::Dumper::Dumper($name_with_ext) });
    $ctrl::logger->debug('$filepath  = ', sub { Data::Dumper::Dumper($filepath     ) });

    given ($titles{$filepath}{'format'})
    {
        when ($_ == FORMAT_EPUB)
        {
            $filepath_rel = Path::Tiny::path(File::Spec::Functions::catfile(File::Basename::dirname($titles{$filepath}{'opf'}{'filepath'}),
                $name_with_ext))->canonpath();
        }

        when ($_ == FORMAT_FB2)
        {
            $filepath_rel = '';
        }

        #when ($_ == FORMAT_FB3)
        default
        {
            $filepath_rel = Path::Tiny::path(File::Spec::Functions::catfile(File::Basename::dirname($titles{$filepath}{'desc'}{'filepath'}),
                $name_with_ext))->canonpath();
        }
    }

    return $filepath_rel;
}

################################################################################

sub edit_tags_merge
{
    my @tags;

    my $filepath = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath)});

    # Merge tags with the same names and the same values
    foreach my $tag_all (@tags_all)
    {
        foreach my $idx (0 .. $#{$titles{$filepath}{'tags'}})
        {
            my $tag = ${$titles{$filepath}{'tags'}}[$idx];

            if ($tag->{'name'} eq $tag_all->{'name'} &&
                exists $tag_all->{'val'}             &&
                $tag->{'val'} eq $tag_all->{'val'}   &&
                !exists $tag->{'found'})
            {
                $tag_all->{'cnt'}++;
                $tag_all->{'found'} = 1;
                $tag_all->{'links'}{$filepath} = $tag;
                $tag->{'found'} = 1;

                last;
            }
        }
    }

    # Merge tags with the same names and different values
    foreach my $tag_all (@tags_all)
    {
        if (!exists $tag_all->{'found'})
        {
            foreach my $idx (0 .. $#{$titles{$filepath}{'tags'}})
            {
                my $tag = ${$titles{$filepath}{'tags'}}[$idx];

                if ($tag->{'name'} eq $tag_all->{'name'} &&
                    !exists $tag->{'found'})
                {
                    $tag_all->{'cnt'}++;
                    $tag_all->{'found'} = 1;
                    $tag_all->{'links'}{$filepath} = $tag;
                    delete $tag_all->{'val'};
                    $tag->{'found'} = 1;

                    last;
                }
            }
        }
    }

    # Add missing tags
    foreach my $idx (0 .. $#{$titles{$filepath}{'tags'}})
    {
        my $tag = ${$titles{$filepath}{'tags'}}[$idx];

        if (!exists $tag->{'found'})
        {
            my %tag_all;

            $tag_all{'name'} = $tag->{'name'};
            $tag_all{'val' } = $tag->{'val'};
            $tag_all{'cnt' } = 1;
            $tag_all{'links'}{$filepath} = $tag;

            push @tags_all, \%tag_all;
        }
    }

    # Cleanup merged tags
    foreach my $tag_all (@tags_all)
    {
        delete $tag_all->{'found'};
    }

    # Cleanup source tags
    foreach my $tag (@{$titles{$filepath}{'tags'}})
    {
        delete $tag->{'found'};
    }
}

################################################################################

sub edit_tags_add
{
    my %tag_all;

    my $tag = shift;
    my $val = shift;

    $ctrl::logger->debug('$tag = ', sub { Data::Dumper::Dumper($tag) });
    $ctrl::logger->debug('$val = ', sub { Data::Dumper::Dumper($val) });

    return RES_EMPTY if $tag eq '';

    # Add tag
    $tag_all{'name'} = $tag;
    $tag_all{'val' } = $val;
    $tag_all{'cnt' } = %titles;

    foreach my $filepath (keys %titles)
    {
        my %tag;

        $tag{'name'} = $tag;
        $tag{'val' } = $val;

        push @{$titles{$filepath}{'tags'}}, \%tag;

        $tag_all{'links'}{$filepath} = \%tag;
    }

    push @tags_all, \%tag_all;

    return RES_OK;
}

################################################################################

sub edit_tags_edit
{
    my $idx = shift;
    my $tag = shift;
    my $val = shift;
    my $all = shift;

    $ctrl::logger->debug('$idx = ', sub { Data::Dumper::Dumper($idx) });
    $ctrl::logger->debug('$tag = ', sub { Data::Dumper::Dumper($tag) });
    $ctrl::logger->debug('$val = ', sub { Data::Dumper::Dumper($val) });
    $ctrl::logger->debug('$all = ', sub { Data::Dumper::Dumper($all) });

    return RES_EMPTY if $tag eq '';

    # Edit tag
    $tags_all[$idx]->{'name'} = $tag;
    $tags_all[$idx]->{'val' } = $val;

    foreach my $filepath (keys %{$tags_all[$idx]{'links'}})
    {
        my $tag_ = $tags_all[$idx]{'links'}{$filepath};

        $tag_->{'name'} = $tag;
        $tag_->{'val' } = $val;
    }

    if ($all)
    {
        my @new = grep { not exists $tags_all[$idx]{'links'}{$_} } keys %titles;

        $tags_all[$idx]{'cnt'} = %titles;

        foreach my $filepath (@new)
        {
            my %tag_;

            $tag_{'name'} = $tag;
            $tag_{'val' } = $val;

            push @{$titles{$filepath}{'tags'}}, \%tag_;

            $tags_all[$idx]{'links'}{$filepath} = \%tag_;
        }
    }

    return RES_OK;
}

################################################################################

sub edit_tags_remove
{
    my $idx = shift;

    $ctrl::logger->debug('$idx = ', sub { Data::Dumper::Dumper($idx) });

    # Remove tag
    foreach my $filepath (keys %{$tags_all[$idx]{'links'}})
    {
        my $tag = $tags_all[$idx]{'links'}{$filepath};

        foreach my $idx_ (0 .. $#{$titles{$filepath}{'tags'}})
        {
            my $tag_ = $titles{$filepath}{'tags'}[$idx_];

            if (Scalar::Util::refaddr($tag) == Scalar::Util::refaddr($tag_))
            {
                splice @{$titles{$filepath}{'tags'}}, $idx_, 1;
                last;
            }
        }
    }

    splice @tags_all, $idx, 1;
}

################################################################################

sub edit_tags_save
{
    my $filepath = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });

    # Save result on disk
    title_save($filepath, TITLE_SAVE_TAGS_ONLY);
}

################################################################################

sub tags_from_filepath_parse
{
    my ($filename_no_ext, $dir_rel, undef);
    my  $filepath_no_ext;

    my @tag_names;
    my @tags;

    my $filepath = shift;
    my $pattern  = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });
    $ctrl::logger->debug('$pattern = ',  sub { Data::Dumper::Dumper($pattern)  });

    # Parse filepath.
    #
    # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
    ($filename_no_ext, $dir_rel, undef) = File::Basename::fileparse($filepath, qr/\.[^.]*/);
    $ctrl::logger->debug('$filename_no_ext = ', sub { Data::Dumper::Dumper($filename_no_ext) });
    $ctrl::logger->debug('$dir_rel = ',         sub { Data::Dumper::Dumper($dir_rel        ) });

    $filepath_no_ext = File::Spec::Functions::catfile($pattern eq '' || Path::Tiny::path($pattern)->is_relative() ?
                                                          $dir_rel : ($dir, $dir_rel),
                                                      $filename_no_ext);
    $filepath_no_ext = Path::Tiny::path($filepath_no_ext)->canonpath();
    $ctrl::logger->debug('filepath_no_ext = ', sub { Data::Dumper::Dumper($filepath_no_ext) });

    # We will use Perl regex to capture meta tags from filepath.
    # First, capture tag names into array.
    @tag_names = $pattern =~ /<(.+?)>/g;

    # Fix pattern to capture values
    $pattern =  quotemeta $pattern;
    $pattern =~ s/\\<.+?\\>/(.+)/g;
    $ctrl::logger->debug('$pattern = ', sub { Data::Dumper::Dumper($pattern) });

    # Capture tag values into array
    if ($filepath_no_ext =~ /$pattern/)
    {
        foreach my $idx (0 .. $#{^CAPTURE})
        {
            my %tag;

            $tag{'name'} = $tag_names[$idx];
            $tag{'val' } = ${^CAPTURE}[$idx];

            push @tags, \%tag;
        }
    }

    $ctrl::logger->debug('\@tags = ', sub { Data::Dumper::Dumper(\@tags) });

    return \@tags;
}

################################################################################

sub tags_from_filepath_save
{
    my $filepath = shift;
    my $tags     = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });
    $ctrl::logger->debug('\$tags = ',    sub { Data::Dumper::Dumper(\$tags)    });

    # Add new tags to memory
    push @{$titles{$filepath}{'tags'}}, @$tags;

    # Save result on disk
    title_save($filepath, TITLE_SAVE_TAGS_ONLY);
}

################################################################################

sub filepath_from_tags_parse
{
    my $new;
    my $ext;

    my $filepath = shift;
    my $pattern  = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });
    $ctrl::logger->debug('$pattern = ',  sub { Data::Dumper::Dumper($pattern)  });

    # We will use Perl regex to substitute tags in pattern.
    # It produces a new filename.
    $new = $pattern;

    if (defined $titles{$filepath}{'tags'})
    {
        foreach my $tag (@{$titles{$filepath}{'tags'}})
        {
            $new =~ s/\Q<$tag->{'name'}\E>/$tag->{'val'}/g;
        }
    }

    # Parse filepath.
    #
    # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
    (undef, undef, $ext) = File::Basename::fileparse($filepath, qr/\.[^.]*/);
    $ctrl::logger->debug('$ext = ', sub { Data::Dumper::Dumper($ext) });

    # Add book extension to new filename
    $new .= $ext;

    $ctrl::logger->debug('$new = ', sub { Data::Dumper::Dumper($new) });

    return $new;
}

################################################################################

sub filepath_from_tags_save
{
    my $res;

    my $filepath = shift;
    my $new      = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });
    $ctrl::logger->debug('$new = ',      sub { Data::Dumper::Dumper($new)      });

    try
    {
        my $filepath_abs;
        my $new_abs;
        my $dir_new;

        # Get absolute target filepaths
        $new ne '' or
            die "Filepath is empty, can't create it!";

        $filepath_abs = File::Spec::Functions::catfile($dir, $filepath);
        $ctrl::logger->debug('$filepath_abs = ', sub { Data::Dumper::Dumper($filepath_abs) });

        $new_abs = Path::Tiny::path($new)->is_relative() ?
            File::Spec::Functions::catfile($dir, $new) :
            $new;
        $ctrl::logger->debug('$new_abs = ', sub { Data::Dumper::Dumper($new_abs) });

        # Create directory tree
        $dir_new = File::Basename::dirname($new_abs);
        File::Path::make_path($dir_new, {'error' => \my $err});

        if (defined $err && @$err > 0)
        {
            foreach my $diag (@$err)
            {
                my ($filepath_, $msg) = %$diag;

                if ($filepath_ eq '') { die _T('General error: ') . $msg;                                 }
                else                  { die _T('Problem creating directory ') . $filepath_ . ': ' . $msg; }
            }
        }

        # Rename the file
        File::Copy::move($filepath_abs, $new_abs) or
            die _T('Move failed: ') . $!;

        # Remember the new filepath
        #
        # Do not do it now because titles will be cleared anyway soon.
        #$titles{$new} = $titles{$filepath};
        #delete $titles{$filepath};

        $res = 1;
    }
    catch
    {
        $ctrl::logger->error($_);
    };
}

################################################################################

sub edit_covers_merge
{
    my @covers;

    my $filepath = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath)});

    # Merge covers with the same names and the same checksums
    foreach my $cover_all (@covers_all)
    {
        foreach my $idx (0 .. $#{$titles{$filepath}{'covers'}})
        {
            my $cover = ${$titles{$filepath}{'covers'}}[$idx];

            if ($cover->{'name'} eq $cover_all->{'name'}                            &&
                (exists $cover_all->{'filepath_abs'} || exists $cover_all->{'bin'}) &&
                $cover->{'checksum'} eq $cover_all->{'checksum'}                    &&
                !exists $cover->{'found'})
            {
                $cover_all->{'filepath_abs'} = $cover->{'filepath_abs'} if exists $cover->{'filepath_abs'};
                $cover_all->{'bin'} = \$cover->{'bin'} if exists $cover->{'bin'};
                $cover_all->{'type'} = $cover->{'type'} if exists $cover->{'type'};
                $cover_all->{'cnt'}++;
                $cover_all->{'found'} = 1;
                $cover_all->{'links'}{$filepath} = $cover;
                $cover->{'found'} = 1;

                last;
            }
        }
    }

    # Merge covers with the same names and different checksums
    foreach my $cover_all (@covers_all)
    {
        if (!exists $cover_all->{'found'})
        {
            foreach my $idx (0 .. $#{$titles{$filepath}{'covers'}})
            {
                my $cover = ${$titles{$filepath}{'covers'}}[$idx];

                if ($cover->{'name'} eq $cover_all->{'name'} &&
                    !exists $cover->{'found'})
                {
                    $cover_all->{'cnt'}++;
                    $cover_all->{'found'} = 1;
                    $cover_all->{'links'}{$filepath} = $cover;
                    delete $cover_all->{'filepath_abs'};
                    delete $cover_all->{'bin'};
                    delete $cover_all->{'type'};
                    delete $cover_all->{'checksum'};
                    $cover->{'found'} = 1;

                    last;
                }
            }
        }
    }

    # Add missing covers
    foreach my $idx (0 .. $#{$titles{$filepath}{'covers'}})
    {
        my $cover = ${$titles{$filepath}{'covers'}}[$idx];

        if (!exists $cover->{'found'})
        {
            my %cover_all;

            $cover_all{'name'        } =  $cover->{'name'};
            $cover_all{'filepath_abs'} =  $cover->{'filepath_abs'} if exists $cover->{'filepath_abs'};
            $cover_all{'bin'         } = \$cover->{'bin'}          if exists $cover->{'bin'};
            $cover_all{'type'        } =  $cover->{'type'}         if exists $cover->{'type'};
            $cover_all{'checksum'    } =  $cover->{'checksum'};
            $cover_all{'cnt'         } =  1;
            $cover_all{'links'}{$filepath} = $cover;

            push @covers_all, \%cover_all;
        }
    }

    # Cleanup merged covers
    foreach my $cover_all (@covers_all)
    {
        delete $cover_all->{'found'};
    }

    # Cleanup source covers
    foreach my $cover (@{$titles{$filepath}{'covers'}})
    {
        delete $cover->{'found'};
    }
}

################################################################################

sub edit_covers_add
{
    my $checksum;
    my %cover_all;
    my $ext;
    my $res;
    my $name_with_ext;

    my $name     = shift;
    my $filepath = shift;

    $ctrl::logger->debug('$name     = ', sub { Data::Dumper::Dumper($name    ) });
    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });

    # Ensure there are no covers with the same name
    foreach my $cover (@covers_all)
    {
        if ($cover->{'name'} eq $name)
        {
            return RES_EXISTS_ALREADY;
        }
    }

    # Add cover
    try
    {
        my $sha = Digest::SHA->new(SHA_ALG);

        $sha->addfile($filepath);
        $checksum = $sha->hexdigest();
        $cover_all{'checksum'} = $checksum;
    }
    catch
    {
        $res = RES_IO_ERR;
    };

    return $res if defined $res;

    $cover_all{'name'        } = $name;
    $cover_all{'filepath_abs'} = $filepath;
    $cover_all{'cnt'         } = %titles;

    # Parse filepath.
    #
    # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
    (undef, undef, $ext) = File::Basename::fileparse($filepath, qr/\.[^.]*/);
    $name_with_ext = $name . $ext;

    foreach my $filepath_ (keys %titles)
    {
        my %cover;

        $cover{'name'        } = $name;
        $cover{'filepath'    } = filepath_rel_get($name_with_ext, $filepath_);
        $cover{'filepath_abs'} = $filepath;
        $cover{'checksum'    } = $checksum;

        push @{$titles{$filepath_}{'covers'}}, \%cover;

        $cover_all{'links'}{$filepath_} = \%cover;
    }

    push @covers_all, \%cover_all;

    return RES_OK;
}

################################################################################

sub edit_covers_edit
{
    my $checksum;
    my $res;
    my $ext;
    my $name_with_ext;

    my $idx      = shift;
    my $name     = shift;
    my $filepath = shift;
    my $all      = shift;

    $ctrl::logger->debug('$idx = ',      sub { Data::Dumper::Dumper($idx     ) });
    $ctrl::logger->debug('$name = ',     sub { Data::Dumper::Dumper($name    ) });
    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });
    $ctrl::logger->debug('$all = ',      sub { Data::Dumper::Dumper($all     ) });

    # Ensure there are no covers with the same name
    foreach my $idx_ (0 .. $#covers_all)
    {
        next if $idx_ == $idx;

        if ($covers_all[$idx_]{'name'} eq $name)
        {
            return RES_EXISTS_ALREADY;
        }
    }

    # Edit cover
    try
    {
        my $sha = Digest::SHA->new(SHA_ALG);

        $sha->addfile($filepath);
        $checksum = $sha->hexdigest();
        $covers_all[$idx]{'checksum'} = $checksum;
    }
    catch
    {
        $res = RES_IO_ERR;
    };

    return $res if defined $res;

    $covers_all[$idx]->{'name'        } = $name;
    $covers_all[$idx]->{'filepath_abs'} = $filepath;

    delete $covers_all[$idx]->{'bin'};
    delete $covers_all[$idx]->{'type'};

    # Parse filepath.
    #
    # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
    (undef, undef, $ext) = File::Basename::fileparse($filepath, qr/\.[^.]*/);
    $name_with_ext = $name . $ext;

    foreach my $filepath_ (keys %{$covers_all[$idx]{'links'}})
    {
        my $cover_ = $covers_all[$idx]{'links'}{$filepath_};

        $cover_->{'name'        } = $name;
        $cover_->{'filepath'    } = filepath_rel_get($name_with_ext, $filepath_);
        $cover_->{'filepath_abs'} = $filepath;
        $cover_->{'checksum'    } = $checksum;

        delete $cover_->{'bin' };
        delete $cover_->{'type'};
    }

    if ($all)
    {
        my @new = grep { not exists $covers_all[$idx]{'links'}{$_} } keys %titles;

        $covers_all[$idx]{'cnt'} = %titles;

        foreach my $filepath_ (@new)
        {
            my %cover_;

            $cover_{'name'        } = $name;
            $cover_{'filepath'    } = filepath_rel_get($name_with_ext, $filepath_);
            $cover_{'filepath_abs'} = $filepath;
            $cover_{'checksum'    } = $checksum;

            delete $cover_{'bin' };
            delete $cover_{'type'};

            push @{$titles{$filepath_}{'covers'}}, \%cover_;

            $covers_all[$idx]{'links'}{$filepath_} = \%cover_;
        }
    }

    return RES_OK;
}

################################################################################

sub edit_covers_remove
{
    my $idx = shift;

    $ctrl::logger->debug('$idx = ', sub { Data::Dumper::Dumper($idx) });

    # Remove cover
    foreach my $filepath (keys %{$covers_all[$idx]{'links'}})
    {
        my $cover = $covers_all[$idx]{'links'}{$filepath};

        foreach my $idx_ (0 .. $#{$titles{$filepath}{'covers'}})
        {
            my $cover_ = $titles{$filepath}{'covers'}[$idx_];

            if (Scalar::Util::refaddr($cover) == Scalar::Util::refaddr($cover_))
            {
                splice @{$titles{$filepath}{'covers'}}, $idx_, 1;
                last;
            }
        }
    }

    splice @covers_all, $idx, 1;
}

################################################################################

sub edit_covers_save
{
    my $filepath = shift;

    $ctrl::logger->debug('$filepath = ', sub { Data::Dumper::Dumper($filepath) });

    # Save result on disk
    title_save($filepath, TITLE_SAVE_COVERS_ONLY);
}

################################################################################

return 1;
