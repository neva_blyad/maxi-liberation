=pod

=encoding UTF-8

=head1 NAME

Maxi Liberation
view.pm

=head1 VERSION

0.27

=head1 DESCRIPTION

EPUB/FB2/FB3 book meta tag editor.

This is the View module.
All GUI should go here.
Now it is implemented by the wxWidgets/wxPerl widget toolkit.
Qt interface is planned.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This file is part of Maxi Liberation.

Maxi Liberation is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maxi Liberation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare View package
package view;

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# Some I/O functions should fuck up everything
use autodie qw(open close);

# CPAN modules
use Cwd;
use File::Basename;
use File::Spec::Functions;
use IO::Scalar;
use Locale::gettext;
use Scalar::Util;
use Try::Tiny;
use Wx;
use Wx::Event;
use Wx::XRC;

# Path to binary
my $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib $path;

# Our own modules
#use model;          # Model. View should not interact with the Model.
use ctrl;           # Controller
use view::btn;      # wxButton
use view::cmb;      # wxComboBox
use view::lbl;      # wxStaticText
use view::lst_box;  # wxListBox
use view::lst_ctrl; # wxListCtrl
use view::txt;      # wxTextCtrl

# Constants
use constant XRC     => 'maxi-liberation.xrc'; # Name of our XRC GUI resource file
use constant COPYING => $ctrl::use_prefix ?
                            File::Spec::Functions::catfile('..', 'doc', 'maxi-liberation', 'COPYING') :
                            'COPYING'; # Copying file

use constant ICON    => File::Spec::Functions::catfile('img', 'luna-libre.ico'); # Window icon
use constant LOGO    => File::Spec::Functions::catfile('img', 'luna-libre.png'); # Window logo

use constant GIF  => Wx->wxBITMAP_TYPE_GIF;
use constant JPEG => Wx->wxBITMAP_TYPE_JPEG;
use constant PNG  => Wx->wxBITMAP_TYPE_PNG;

# Global Variables
our $app;
our $locale;

our $frm;
    our $splitter_vertical;
        our $panel_left;
            our $splitter_horizontal;
                our $panel_top;
                    our $dir;
                our $panel_bottom;
                    our $lst_box_filepath;
                    our $lbl_dir;
                    our $btn_menu;
                    our $btn_scan;
                    our $txt_depth;
                    our $lbl_lvl;
                    our $lbl_cnt;
        our $panel_right;
            our $notebook;
                our $panel_tab1;
                    our $lst_ctrl_tab1;
                    our $btn_add_tab1;
                    our $btn_edit_tab1;
                    our $btn_remove_tab1;
                    our $btn_save_tab1;
                our $panel_tab2;
                    our $lbl_pattern_tab2;
                    our $cmb_pattern_tab2;
                    our $lst_ctrl_tab2;
                    our $btn_preview_tab2;
                    our $btn_save_tab2;
                our $panel_tab3;
                    our $lbl_pattern_tab3;
                    our $cmb_pattern_tab3;
                    our $lst_ctrl_tab3;
                    our $btn_preview_tab3;
                    our $btn_save_tab3;
                our $panel_tab4;
                    our $splitter_vertical_tab4;
                        our $panel_left_tab4;
                            our $lst_box_tab4;
                        our $panel_right_tab4;
                            our $img_tab4;
                    our $lbl_tab4;
                    our $btn_add_tab4;
                    our $btn_edit_tab4;
                    our $btn_remove_tab4;
                    our $btn_save_tab4;
our $menu;
    our $menu_plugins;
    our $menu_about;

our $dlg_plugins;
    our $check_lst_plugins;

our $dlg_add_tag;
    our $lbl_add_tag;
    our $cmb_add_tag;
    our $lbl_add_val;
    our $txt_add_val;
    our $btn_add_tag_cancel;
    our $btn_add_tag;

our $dlg_edit_tag;
    our $lbl_edit_tag;
    our $cmb_edit_tag;
    our $lbl_edit_val;
    our $txt_edit_val;
    our $btn_edit_tag_cancel;
    our $btn_edit_tag;

our $dlg_add_cover;
    our $lbl_add_name;
    our $txt_add_name;
    our $lbl_add_filepath;
    our $txt_add_filepath;
    our $btn_add_filepath;
    our $btn_add_cover_cancel;
    our $btn_add_cover;

our $dlg_edit_cover;
    our $lbl_edit_name;
    our $txt_edit_name;
    our $lbl_edit_filepath;
    our $txt_edit_filepath;
    our $btn_edit_filepath;
    our $btn_edit_cover_cancel;
    our $btn_edit_cover;

my $img_tab4_;
my ($img_tab4_width, $img_tab4_height);

################################################################################

sub preinit
{
    # Initializes all available image handlers
    Wx::InitAllImageHandlers();
}

################################################################################

sub init
{
       $app    = Wx::SimpleApp->new();
    my $xrc    = Wx::XmlResource->new();
       $locale = Wx::Locale->new(Locale::gettext::setlocale(Locale::gettext->LC_MESSAGES));

    # The following code will set the appropriate variables so that
    # the import of XRC code can be translated
    $locale->AddCatalogLookupPathPrefix(ctrl->LOCALE);
    $locale->AddCatalog(ctrl->DOMAIN);

    # Load the XRC file
    $xrc->InitAllHandlers();
    $xrc->Load(XRC);

    # Find/load the widgets
    $frm = $xrc->LoadFrame(undef, 'frm');
        $splitter_vertical = $frm->FindWindow('splitter_vertical');
            $panel_left = $splitter_vertical->FindWindow('panel_left');
                $splitter_horizontal = $panel_left->FindWindow('splitter_horizontal');
                    $panel_top = $splitter_horizontal->FindWindow('panel_top');
                        $dir = $panel_top->FindWindow('dir');
                    $panel_bottom = $splitter_horizontal->FindWindow('panel_bottom');
                        $lst_box_filepath = $panel_bottom->FindWindow('lst_box_filepath');
                        $lbl_dir          = $panel_bottom->FindWindow('lbl_dir');
                        $btn_menu         = $panel_bottom->FindWindow('btn_menu');
                        $btn_scan         = $panel_bottom->FindWindow('btn_scan');
                        $txt_depth        = $panel_bottom->FindWindow('txt_depth');
                        $lbl_lvl          = $panel_bottom->FindWindow('lbl_lvl');
                        $lbl_cnt          = $panel_bottom->FindWindow('lbl_cnt');
            $panel_right = $splitter_vertical->FindWindow('panel_right');
                $notebook = $panel_right->FindWindow('panel_right');
                    $panel_tab1 = $notebook->FindWindow('panel_tab1');
                        $lst_ctrl_tab1    = $panel_tab1->FindWindow('lst_ctrl_tab1');
                        $btn_add_tab1     = $panel_tab1->FindWindow('btn_add_tab1');
                        $btn_edit_tab1    = $panel_tab1->FindWindow('btn_edit_tab1');
                        $btn_remove_tab1  = $panel_tab1->FindWindow('btn_remove_tab1');
                        $btn_save_tab1    = $panel_tab1->FindWindow('btn_save_tab1');
                    $panel_tab2 = $notebook->FindWindow('panel_tab2');
                        $lbl_pattern_tab2 = $panel_tab2->FindWindow('lbl_pattern_tab2');
                        $cmb_pattern_tab2 = $panel_tab2->FindWindow('cmb_pattern_tab2');
                        $lst_ctrl_tab2    = $panel_tab2->FindWindow('lst_ctrl_tab2');
                        $btn_preview_tab2 = $panel_tab2->FindWindow('btn_preview_tab2');
                        $btn_save_tab2    = $panel_tab2->FindWindow('btn_save_tab2');
                    $panel_tab3 = $notebook->FindWindow('panel_tab3');
                        $lbl_pattern_tab3 = $panel_tab3->FindWindow('lbl_pattern_tab3');
                        $cmb_pattern_tab3 = $panel_tab3->FindWindow('cmb_pattern_tab3');
                        $lst_ctrl_tab3    = $panel_tab3->FindWindow('lst_ctrl_tab3');
                        $btn_preview_tab3 = $panel_tab3->FindWindow('btn_preview_tab3');
                        $btn_save_tab3    = $panel_tab3->FindWindow('btn_save_tab3');
                    $panel_tab4 = $notebook->FindWindow('panel_tab4');
                        $splitter_vertical_tab4 = $panel_tab4->FindWindow('splitter_vertical_tab4');
                            $panel_left_tab4 = $splitter_vertical_tab4->FindWindow('panel_left_tab4');
                                $lst_box_tab4 = $panel_left_tab4->FindWindow('lst_box_tab4');
                            $panel_right_tab4 = $splitter_vertical_tab4->FindWindow('panel_right_tab4');
                                $img_tab4 = $panel_right_tab4->FindWindow('img_tab4');
                        $lbl_tab4        = $panel_tab4->FindWindow('lbl_tab4');
                        $btn_add_tab4    = $panel_tab4->FindWindow('btn_add_tab4');
                        $btn_edit_tab4   = $panel_tab4->FindWindow('btn_edit_tab4');
                        $btn_remove_tab4 = $panel_tab4->FindWindow('btn_remove_tab4');
                        $btn_save_tab4   = $panel_tab4->FindWindow('btn_save_tab4');
    $menu = $xrc->LoadMenu('menu');
        $menu_plugins = $frm->FindWindow('menu_plugins');
        $menu_about   = $frm->FindWindow('menu_about');

    $dlg_plugins = $xrc->LoadDialog(undef, 'dlg_plugins');
        $check_lst_plugins     = $dlg_plugins->FindWindow ('check_lst_plugins');

    $dlg_add_tag = $xrc->LoadDialog(undef, 'dlg_add_tag');
        $lbl_add_tag           = $dlg_add_tag->FindWindow('lbl_add_tag');
        $cmb_add_tag           = $dlg_add_tag->FindWindow('cmb_add_tag');
        $lbl_add_val           = $dlg_add_tag->FindWindow('lbl_add_val');
        $txt_add_val           = $dlg_add_tag->FindWindow('txt_add_val');
        $btn_add_tag_cancel    = $dlg_add_tag->FindWindow('btn_add_tag_cancel');
        $btn_add_tag           = $dlg_add_tag->FindWindow('btn_add_tag');

    $dlg_edit_tag = $xrc->LoadDialog(undef, 'dlg_edit_tag');
        $lbl_edit_tag          = $dlg_edit_tag->FindWindow('lbl_edit_tag');
        $cmb_edit_tag          = $dlg_edit_tag->FindWindow('cmb_edit_tag');
        $lbl_edit_val          = $dlg_edit_tag->FindWindow('lbl_edit_val');
        $txt_edit_val          = $dlg_edit_tag->FindWindow('txt_edit_val');
        $btn_edit_tag_cancel   = $dlg_edit_tag->FindWindow('btn_edit_tag_cancel');
        $btn_edit_tag          = $dlg_edit_tag->FindWindow('btn_edit_tag');

    $dlg_add_cover = $xrc->LoadDialog(undef, 'dlg_add_cover');
        $lbl_add_name          = $dlg_add_cover->FindWindow('lbl_add_name');
        $txt_add_name          = $dlg_add_cover->FindWindow('txt_add_name');
        $lbl_add_filepath      = $dlg_add_cover->FindWindow('lbl_add_filepath');
        $txt_add_filepath      = $dlg_add_cover->FindWindow('txt_add_filepath');
        $btn_add_filepath      = $dlg_add_cover->FindWindow('btn_add_filepath');
        $btn_add_cover_cancel  = $dlg_add_cover->FindWindow('btn_add_cover_cancel');
        $btn_add_cover         = $dlg_add_cover->FindWindow('btn_add_cover');

    $dlg_edit_cover = $xrc->LoadDialog(undef, 'dlg_edit_cover');
        $lbl_edit_name         = $dlg_edit_cover->FindWindow('lbl_edit_name');
        $txt_edit_name         = $dlg_edit_cover->FindWindow('txt_edit_name');
        $lbl_edit_filepath     = $dlg_edit_cover->FindWindow('lbl_edit_filepath');
        $txt_edit_filepath     = $dlg_edit_cover->FindWindow('txt_edit_filepath');
        $btn_edit_filepath     = $dlg_edit_cover->FindWindow('btn_edit_filepath');
        $btn_edit_cover_cancel = $dlg_edit_cover->FindWindow('btn_edit_cover_cancel');
        $btn_edit_cover        = $dlg_edit_cover->FindWindow('btn_edit_cover');

    # Setup events for the widgets
    Wx::Event::EVT_CLOSE               ($frm,          \&ctrl::closed);
    Wx::Event::EVT_LISTBOX             ($panel_bottom, Wx::XmlResource::GetXRCID('lst_box_filepath'), \&ctrl::lst_box_filepath_selected);
    Wx::Event::EVT_BUTTON              ($panel_bottom, Wx::XmlResource::GetXRCID('btn_menu'        ), \&ctrl::btn_menu_clicked);
    Wx::Event::EVT_BUTTON              ($panel_bottom, Wx::XmlResource::GetXRCID('btn_scan'        ), \&ctrl::btn_scan_clicked);
    Wx::Event::EVT_LIST_ITEM_SELECTED  ($panel_tab1,   Wx::XmlResource::GetXRCID('lst_ctrl_tab1'   ), \&ctrl::lst_ctrl_tab1_selected);
    Wx::Event::EVT_LIST_ITEM_DESELECTED($panel_tab1,   Wx::XmlResource::GetXRCID('lst_ctrl_tab1'   ), \&ctrl::lst_ctrl_tab1_deselected);
    Wx::Event::EVT_BUTTON              ($panel_tab1,   Wx::XmlResource::GetXRCID('btn_add_tab1'    ), \&ctrl::btn_add_tab1_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab1,   Wx::XmlResource::GetXRCID('btn_edit_tab1'   ), \&ctrl::btn_edit_tab1_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab1,   Wx::XmlResource::GetXRCID('btn_remove_tab1' ), \&ctrl::btn_remove_tab1_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab1,   Wx::XmlResource::GetXRCID('btn_save_tab1'   ), \&ctrl::btn_save_tab1_clicked);
    Wx::Event::EVT_TEXT                ($panel_tab2,   Wx::XmlResource::GetXRCID('cmb_pattern_tab2'), \&ctrl::cmb_pattern_tab2_changed);
    Wx::Event::EVT_COMBOBOX            ($panel_tab2,   Wx::XmlResource::GetXRCID('cmb_pattern_tab2'), \&ctrl::cmb_pattern_tab2_changed) if $^O eq 'darwin'; # EVT_TEXT event macros only is not enough for macOS
    Wx::Event::EVT_BUTTON              ($panel_tab2,   Wx::XmlResource::GetXRCID('btn_preview_tab2'), \&ctrl::btn_preview_tab2_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab2,   Wx::XmlResource::GetXRCID('btn_save_tab2'   ), \&ctrl::btn_save_tab2_clicked);
    Wx::Event::EVT_TEXT                ($panel_tab3,   Wx::XmlResource::GetXRCID('cmb_pattern_tab3'), \&ctrl::cmb_pattern_tab3_changed);
    Wx::Event::EVT_COMBOBOX            ($panel_tab3,   Wx::XmlResource::GetXRCID('cmb_pattern_tab3'), \&ctrl::cmb_pattern_tab3_changed) if $^O eq 'darwin'; # EVT_TEXT event macros only is not enough for macOS
    Wx::Event::EVT_BUTTON              ($panel_tab3,   Wx::XmlResource::GetXRCID('btn_preview_tab3'), \&ctrl::btn_preview_tab3_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab3,   Wx::XmlResource::GetXRCID('btn_save_tab3'   ), \&ctrl::btn_save_tab3_clicked);
    Wx::Event::EVT_LISTBOX             ($panel_tab4,   Wx::XmlResource::GetXRCID('lst_box_tab4'    ), \&ctrl::lst_box_tab4_selected);
    Wx::Event::EVT_SIZE                ($img_tab4,     \&ctrl::img_tab4_resized);
    Wx::Event::EVT_BUTTON              ($panel_tab4,   Wx::XmlResource::GetXRCID('btn_add_tab4'    ), \&ctrl::btn_add_tab4_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab4,   Wx::XmlResource::GetXRCID('btn_edit_tab4'   ), \&ctrl::btn_edit_tab4_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab4,   Wx::XmlResource::GetXRCID('btn_remove_tab4' ), \&ctrl::btn_remove_tab4_clicked);
    Wx::Event::EVT_BUTTON              ($panel_tab4,   Wx::XmlResource::GetXRCID('btn_save_tab4'   ), \&ctrl::btn_save_tab4_clicked);
    Wx::Event::EVT_MENU                ($menu,         Wx::XmlResource::GetXRCID('menu_plugins'    ), \&ctrl::menu_plugins_clicked);
    Wx::Event::EVT_MENU                ($menu,         Wx::XmlResource::GetXRCID('menu_about'      ), \&ctrl::menu_about_clicked);

    Wx::Event::EVT_CHECKLISTBOX($dlg_plugins,  Wx::XmlResource::GetXRCID('check_lst_plugins'), \&ctrl::check_lst_plugins_checked);

    Wx::Event::EVT_COMBOBOX($dlg_add_tag,    Wx::XmlResource::GetXRCID('cmb_add_tag'          ), \&ctrl::cmb_add_tag_selected);
    Wx::Event::EVT_BUTTON  ($dlg_add_tag,    Wx::XmlResource::GetXRCID('btn_add_tag_cancel'   ), \&ctrl::btn_add_tag_cancel_clicked);
    Wx::Event::EVT_BUTTON  ($dlg_add_tag,    Wx::XmlResource::GetXRCID('btn_add_tag'          ), \&ctrl::btn_add_tag_clicked);

    Wx::Event::EVT_COMBOBOX($dlg_edit_tag,   Wx::XmlResource::GetXRCID('cmb_edit_tag'         ), \&ctrl::cmb_edit_tag_selected);
    Wx::Event::EVT_BUTTON  ($dlg_edit_tag,   Wx::XmlResource::GetXRCID('btn_edit_tag_cancel'  ), \&ctrl::btn_edit_tag_cancel_clicked);
    Wx::Event::EVT_BUTTON  ($dlg_edit_tag,   Wx::XmlResource::GetXRCID('btn_edit_tag'         ), \&ctrl::btn_edit_tag_clicked);

    Wx::Event::EVT_BUTTON  ($dlg_add_cover,  Wx::XmlResource::GetXRCID('btn_add_filepath'     ), \&ctrl::btn_add_filepath_clicked);
    Wx::Event::EVT_BUTTON  ($dlg_add_cover,  Wx::XmlResource::GetXRCID('btn_add_cover_cancel' ), \&ctrl::btn_add_cover_cancel_clicked);
    Wx::Event::EVT_BUTTON  ($dlg_add_cover,  Wx::XmlResource::GetXRCID('btn_add_cover'        ), \&ctrl::btn_add_cover_clicked);

    Wx::Event::EVT_BUTTON  ($dlg_edit_cover, Wx::XmlResource::GetXRCID('btn_edit_filepath'    ), \&ctrl::btn_edit_filepath_clicked);
    Wx::Event::EVT_BUTTON  ($dlg_edit_cover, Wx::XmlResource::GetXRCID('btn_edit_cover_cancel'), \&ctrl::btn_edit_cover_cancel_clicked);
    Wx::Event::EVT_BUTTON  ($dlg_edit_cover, Wx::XmlResource::GetXRCID('btn_edit_cover'       ), \&ctrl::btn_edit_cover_clicked);

    # Setup the widgets
    $dir->SetPath($model::dir);

    $lst_ctrl_tab1->InsertColumn(0, _T('Tag'));
    $lst_ctrl_tab1->InsertColumn(1, _T('Value'));
    $lst_ctrl_tab2->InsertColumn(0, _T('Filepath'));
    $lst_ctrl_tab3->InsertColumn(0, _T('Filepath'));
    $lst_ctrl_tab3->InsertColumn(1, _T('New Filepath'));

    $lst_ctrl_tab1->SetColumnWidth(0, 120);
    $lst_ctrl_tab1->SetColumnWidth(1, 840);
    $lst_ctrl_tab2->SetColumnWidth(0, 480);
    $lst_ctrl_tab3->SetColumnWidth(0, 480);
    $lst_ctrl_tab3->SetColumnWidth(1, 480);

    if ($^O eq 'MSWin32')
    {
        foreach my $idx (0 .. cmb_pattern_tab2_cnt_get() - 1)
        {
            my $pattern;

            $pattern =  cmb_pattern_tab2_get($idx);
            $pattern =~ s|/|\\|g;

            cmb_pattern_tab2_set($idx, $pattern);
        }

        foreach my $idx (0 .. cmb_pattern_tab3_cnt_get() - 1)
        {
            my $pattern;

            $pattern =  cmb_pattern_tab3_get($idx);
            $pattern =~ s|/|\\|g;

            cmb_pattern_tab3_set($idx, $pattern);
        }
    }

    if ($^O eq 'darwin')
    {
        ctrl::cmb_add_tag_selected();
        ctrl::cmb_edit_tag_selected();
    }

    # Set the main window icon.
    # Show the main window.
    $frm->SetIcon(Wx::Icon->new(ICON, Wx->wxBITMAP_TYPE_ICO)) if $^O ne 'darwin';
    $frm->Show();
}

################################################################################

sub loop
{
    $app->MainLoop();
}

################################################################################

sub frm_destroy
{
    $frm->Destroy();
}

################################################################################

sub frm_title_set
{
    my $frm_title = shift;

    $frm->SetTitle($frm_title);
}

################################################################################

sub dlg_err_show
{
    my $txt = shift;

    Wx::MessageBox($txt, _T('Error'));
}

################################################################################

sub dlg_filepath_show
{
    my $filepath;

    my $filename = shift;
    my $dir      = shift;

    my $filter = join '|', (_T('GIF images (*.gif)|*.gif'),
                            _T('JPEG images (*.jpeg; *.jpg)|*.jpeg;*.jpg'),
                            _T('PNG images (*.png)|*.png'));

    my $dlg = Wx::FileDialog->new($frm,
                                  _T('Select an image'),
                                  $dir,
                                  $filename,
                                  $filter,
                                  Wx->wxFD_OPEN | Wx->wxFD_PREVIEW);
       $dlg->SetFilterIndex(1);

    if ($dlg->ShowModal() != Wx->wxID_CANCEL)
    {
        $filepath = $dlg->GetPath();
    }

    $dlg->Destroy();

    return $filepath;
}

################################################################################

sub dir_get
{
    return $dir->GetPath();
}

################################################################################

sub dir_set
{
    my $dir_ = shift;

    $dir->SetPath($dir_);
}

################################################################################

sub dir_upd
{
    $dir->ReCreateTree();
}

################################################################################

sub lst_box_filepath_add     {        view::lst_box::add     ($lst_box_filepath, @_); }
sub lst_box_filepath_cnt_get { return view::lst_box::cnt_get ($lst_box_filepath, @_); }
sub lst_box_filepath_get     { return view::lst_box::get     ($lst_box_filepath, @_); }
sub lst_box_filepath_set     {        view::lst_box::set     ($lst_box_filepath, @_); }
sub lst_box_filepath_meta_get{ return view::lst_box::meta_get($lst_box_filepath, @_); }
sub lst_box_filepath_meta_set{        view::lst_box::meta_set($lst_box_filepath, @_); }
sub lst_box_filepath_clr     {        view::lst_box::clr     ($lst_box_filepath, @_); }
sub lst_box_filepath_sel_get { return view::lst_box::sel_get ($lst_box_filepath, @_); }

################################################################################

sub lbl_dir_set{ view::lbl::set($lbl_dir, @_); }

################################################################################

sub btn_menu_popup_show
{
    my ($width, $height) = $btn_menu->GetSizeWH();

    $btn_menu->PopupMenu($menu, 0, $height);
}

################################################################################

sub check_lst_plugins_add
{
    my $idx      = shift;
    my $filename = shift;

    if (defined $idx)
    {
        $check_lst_plugins->Insert('', $idx);
    }
    else
    {
        $check_lst_plugins->Append('');
    }
}

################################################################################

sub check_lst_plugins_set
{
    my $idx      = shift;
    my $filename = shift;

    $check_lst_plugins->SetString($idx, $filename);
}

################################################################################

sub check_lst_plugins_check
{
    my $idx  = shift;
    my $flag = shift;

    $check_lst_plugins->Check($idx, $flag);
}

################################################################################

sub check_lst_plugins_meta_get
{
    my $idx = shift;

    return $check_lst_plugins->GetClientData($idx);
}

################################################################################

sub check_lst_plugins_meta_set
{
    my $idx  = shift;
    my $meta = shift;

    $check_lst_plugins->SetClientData($idx, $meta);
}

################################################################################

sub check_lst_plugins_clr
{
    $check_lst_plugins->Clear();
}

################################################################################

sub check_lst_plugins_sel_get
{
    my $param_type;
    my $res;

    my $param = shift;

    try
    {
        # Check parameter type passed to the subroutine
        $param_type = Scalar::Util::blessed($param);

        if (defined $param_type && $param_type eq 'Wx::CommandEvent')
        {
            # Event was passed.
            # Get index of checked/unchecked element.
            my $event = $param;

            $res = $event->GetInt();
        }
        elsif (defined $param && !defined $param_type)
        {
            # Index was passed.
            # Get whether according element is checked or not.
            my $idx = $param;

            $res = $check_lst_plugins->IsChecked($idx);
        }
        else
        {
            # Something different was passed.
            # Error occured.
            die _T('Wrong parameter');
        }
    }
    catch
    {
        $ctrl::logger->error($_);
    };

    return $res;
}

################################################################################

sub dlg_plugins_show
{
    $dlg_plugins->ShowModal();
}

################################################################################

sub dlg_plugins_destroy
{
    $dlg_plugins->Destroy();
}

################################################################################

sub dlg_about_show
{
    try
    {
        my $copying;
        my $dlg;

        open FILE, '< :encoding(UTF-8)', COPYING;
        $copying = <FILE>;
        $copying = <FILE>;
        $copying = <FILE>;
        $copying = <FILE>;
        local $/;
        $copying = <FILE>;
        close FILE;

        $dlg = Wx::AboutDialogInfo->new();

        $dlg->SetIcon(Wx::Icon->new(LOGO, Wx->wxBITMAP_TYPE_PNG));
        $dlg->SetName(_T('Maxi Liberation'));
        $dlg->SetVersion(ctrl->VER);
        $dlg->SetDescription(_T("EPUB/FB2/FB3 book meta tag editor\n\nMaxi means maximum expression of digital text form.\nLiberation is library."));
        $dlg->SetWebSite('https://www.lovecry.pt/maxi-liberation/', _T('The program web page'));
        $dlg->SetCopyright(_T("Copyright © 2021 НЕВСКИЙ БЛЯДИНА\n<neva_blyad\@lovecry.pt>\n<neva_blyad\@lovecri.es>"));
        $dlg->AddDeveloper(_T('НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>'));
        $dlg->AddDeveloper(_T('Invisible Light'));
        $dlg->AddDeveloper(_T('I wish there was somebody else...'));
        $dlg->SetArtists([_T('Unluckily'),
                          _T('none'),
                          _T('so'),
                          _T('the'),
                          _T('graphic'),
                          _T('is'),
                          _T('bad')]);
        $dlg->SetLicense($copying);

        Wx::AboutBox($dlg);
    }
    catch
    {
        $ctrl::logger->error($_);
    };
}

################################################################################

sub txt_depth_get { return view::txt::get($txt_depth, @_); }
sub txt_depth_set { return view::txt::set($txt_depth, @_); }

################################################################################

sub lbl_cnt_set{ view::lbl::set($lbl_cnt, @_); }

################################################################################

sub lst_ctrl_tab1_row_add    {        view::lst_ctrl::row_add    ($lst_ctrl_tab1, @_); }
sub lst_ctrl_tab1_row_remove {        view::lst_ctrl::row_remove ($lst_ctrl_tab1, @_); }
sub lst_ctrl_tab1_row_cnt_get{ return view::lst_ctrl::row_cnt_get($lst_ctrl_tab1, @_); }
sub lst_ctrl_tab1_get        {        view::lst_ctrl::get        ($lst_ctrl_tab1, @_); }
sub lst_ctrl_tab1_set        {        view::lst_ctrl::set        ($lst_ctrl_tab1, @_); }
sub lst_ctrl_tab1_clr        {        view::lst_ctrl::clr        ($lst_ctrl_tab1, @_); }
sub lst_ctrl_tab1_sel_get    { return view::lst_ctrl::sel_get    ($lst_ctrl_tab1, @_); }
sub lst_ctrl_tab1_sel_set    {        view::lst_ctrl::sel_set    ($lst_ctrl_tab1, @_); }

################################################################################

sub btn_add_tab1_en_set   { view::btn::en_set($btn_add_tab1,    @_); }
sub btn_edit_tab1_en_set  { view::btn::en_set($btn_edit_tab1,   @_); }
sub btn_remove_tab1_en_set{ view::btn::en_set($btn_remove_tab1, @_); }
sub btn_save_tab1_en_set  { view::btn::en_set($btn_save_tab1,   @_); }

################################################################################

sub cmb_pattern_tab2_cnt_get{ return view::cmb::cnt_get($cmb_pattern_tab2, @_); }
sub cmb_pattern_tab2_get    { return view::cmb::get    ($cmb_pattern_tab2, @_); }
sub cmb_pattern_tab2_set    {        view::cmb::set    ($cmb_pattern_tab2, @_); }
sub cmb_pattern_tab2_en_set {        view::cmb::en_set ($cmb_pattern_tab2, @_); }

################################################################################

sub lst_ctrl_tab2_row_add    {        view::lst_ctrl::row_add    ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_row_remove {        view::lst_ctrl::row_remove ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_row_cnt_get{ return view::lst_ctrl::row_cnt_get($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_col_add    {        view::lst_ctrl::col_add    ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_col_remove {        view::lst_ctrl::col_remove ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_col_cnt_get{ return view::lst_ctrl::col_cnt_get($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_get        { return view::lst_ctrl::get        ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_set        {        view::lst_ctrl::set        ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_meta_set   {        view::lst_ctrl::meta_set   ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_clr        {        view::lst_ctrl::clr        ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_width_set  {        view::lst_ctrl::width_set  ($lst_ctrl_tab2, @_); }
sub lst_ctrl_tab2_sort       {        view::lst_ctrl::sort       ($lst_ctrl_tab2, @_); }

################################################################################

sub btn_preview_tab2_en_set{ view::btn::en_set($btn_preview_tab2, @_); }
sub btn_save_tab2_en_set   { view::btn::en_set($btn_save_tab2,    @_); }

################################################################################

sub cmb_pattern_tab3_cnt_get{ return view::cmb::cnt_get($cmb_pattern_tab3, @_); }
sub cmb_pattern_tab3_get    { return view::cmb::get    ($cmb_pattern_tab3, @_); }
sub cmb_pattern_tab3_set    {        view::cmb::set    ($cmb_pattern_tab3, @_); }
sub cmb_pattern_tab3_en_set {        view::cmb::en_set ($cmb_pattern_tab3, @_); }

################################################################################

sub lst_ctrl_tab3_row_add    {        view::lst_ctrl::row_add    ($lst_ctrl_tab3, @_); }
sub lst_ctrl_tab3_row_remove {        view::lst_ctrl::row_remove ($lst_ctrl_tab3, @_); }
sub lst_ctrl_tab3_row_cnt_get{ return view::lst_ctrl::row_cnt_get($lst_ctrl_tab3, @_); }
sub lst_ctrl_tab3_get        { return view::lst_ctrl::get        ($lst_ctrl_tab3, @_); }
sub lst_ctrl_tab3_set        {        view::lst_ctrl::set        ($lst_ctrl_tab3, @_); }
sub lst_ctrl_tab3_meta_set   {        view::lst_ctrl::meta_set   ($lst_ctrl_tab3, @_); }
sub lst_ctrl_tab3_clr        {        view::lst_ctrl::clr        ($lst_ctrl_tab3, @_); }
sub lst_ctrl_tab3_sort       {        view::lst_ctrl::sort       ($lst_ctrl_tab3, @_); }

################################################################################

sub btn_preview_tab3_en_set{ view::btn::en_set($btn_preview_tab3, @_); }
sub btn_save_tab3_en_set   { view::btn::en_set($btn_save_tab3,    @_); }

################################################################################

sub lst_box_tab4_add     {        view::lst_box::add     ($lst_box_tab4, @_); }
sub lst_box_tab4_remove  {        view::lst_box::remove  ($lst_box_tab4, @_); }
sub lst_box_tab4_cnt_get { return view::lst_box::cnt_get ($lst_box_tab4, @_); }
sub lst_box_tab4_get     { return view::lst_box::get     ($lst_box_tab4, @_); }
sub lst_box_tab4_set     {        view::lst_box::set     ($lst_box_tab4, @_); }
#sub lst_box_tab4_meta_get{ return view::lst_box::meta_get($lst_box_tab4, @_); }
#sub lst_box_tab4_meta_set{        view::lst_box::meta_set($lst_box_tab4, @_); }
sub lst_box_tab4_clr     {        view::lst_box::clr     ($lst_box_tab4, @_); }
sub lst_box_tab4_sel_get { return view::lst_box::sel_get ($lst_box_tab4, @_); }

################################################################################

sub img_tab4_set
{
    my $param = shift;
    my $type  = shift;

    try
    {
        my $filepath;
        my $bin;

        if (ref $param) { $bin      = $param; }
        else            { $filepath = $param; }

        if (defined $filepath)
        {
            # Load image from a file
            my $bmp = Wx::Bitmap->new($filepath, $type);
               $bmp->IsOk() or
                    die _T('Image load error');

            $img_tab4_ = $bmp->ConvertToImage();
        }
        else
        {
            # Load image from RAM
            my $bin_io    = IO::Scalar->new($bin);
            my $handler   = Wx::Image::FindHandlerType($type);
               $img_tab4_ = Wx::Image->new();

            $handler->LoadFile($img_tab4_, $bin_io);
        }

        ($img_tab4_width, $img_tab4_height) = ($img_tab4_->GetWidth(), $img_tab4_->GetHeight());
        $img_tab4->Show(1);
        img_tab4_upd();
    }
    catch
    {
        $ctrl::logger->warn($_);
    };
}

################################################################################

sub img_tab4_clr
{
    $img_tab4_ = undef;

    # It doesnt' work :(
    #$img_tab4->SetBitmap(Wx->wxNullBitmap);
    #$img_tab4->Refresh();
    
    # Used this hack instead
    $img_tab4->Show(0);
}

################################################################################

sub img_tab4_upd
{
    if (defined $img_tab4_)
    {
        my $coeff_x;
        my $coeff_y;

        my ($width,     $height    );
        my ($width_max, $height_max);

        my $bmp;

        ($width,     $height    ) = ($img_tab4_width, $img_tab4_height);
        ($width_max, $height_max) = $panel_right_tab4->GetSizeWH();

        return if $width_max  == 0 ||
                  $height_max == 0;

        $coeff_x = $width  / $width_max;
        $coeff_y = $height / $height_max;

        if ($coeff_x > 1 ||
            $coeff_y > 1)
        {
            if ($coeff_x > $coeff_y)
            {
                $width  /= $coeff_x;
                $height /= $coeff_x;
            }
            else
            {
                $width  /= $coeff_y;
                $height /= $coeff_y;
            }

            $bmp = Wx::Bitmap->new($img_tab4_->Scale($width, $height));
        }
        else
        {
            $bmp = Wx::Bitmap->new($img_tab4_);
        }

        $img_tab4->SetBitmap($bmp);
        $img_tab4->Refresh();
    }
}

################################################################################

sub btn_add_tab4_en_set   { view::btn::en_set($btn_add_tab4,    @_); }
sub btn_edit_tab4_en_set  { view::btn::en_set($btn_edit_tab4,   @_); }
sub btn_remove_tab4_en_set{ view::btn::en_set($btn_remove_tab4, @_); }
sub btn_save_tab4_en_set  { view::btn::en_set($btn_save_tab4,   @_); }

################################################################################

sub menu_destroy
{
    $menu->Destroy();
}

################################################################################

sub dlg_add_tag_show
{
    $dlg_add_tag->ShowModal();
}

################################################################################

sub dlg_add_tag_close
{
    $dlg_add_tag->Close();
}

################################################################################

sub dlg_add_tag_destroy
{
    $dlg_add_tag->Destroy();
}

################################################################################

sub cmb_add_tag_get{ return view::cmb::get($cmb_add_tag, @_); }
sub cmb_add_tag_set{        view::cmb::set($cmb_add_tag, @_); }

################################################################################

sub txt_add_val_get{ return view::txt::get($txt_add_val, @_); }

################################################################################

sub dlg_edit_tag_show
{
    $dlg_edit_tag->ShowModal();
}

################################################################################

sub dlg_edit_tag_close
{
    $dlg_edit_tag->Close();
}

################################################################################

sub dlg_edit_tag_destroy
{
    $dlg_edit_tag->Destroy();
}

################################################################################

sub cmb_edit_tag_get{ return view::cmb::get($cmb_edit_tag, @_); }
sub cmb_edit_tag_set{        view::cmb::set($cmb_edit_tag, @_); }

################################################################################

sub txt_edit_val_get{ return view::txt::get($txt_edit_val, @_); }
sub txt_edit_val_set{        view::txt::set($txt_edit_val, @_); }

################################################################################

sub dlg_add_cover_show
{
    $dlg_add_cover->ShowModal();
}

################################################################################

sub dlg_add_cover_close
{
    $dlg_add_cover->Close();
}

################################################################################

sub dlg_add_cover_destroy
{
    $dlg_add_cover->Destroy();
}

################################################################################

sub txt_add_name_get{ return view::txt::get($txt_add_name, @_); }

################################################################################

sub txt_add_filepath_get{ return view::txt::get($txt_add_filepath, @_); }
sub txt_add_filepath_set{        view::txt::set($txt_add_filepath, @_); }

################################################################################

sub dlg_edit_cover_show
{
    $dlg_edit_cover->ShowModal();
}

################################################################################

sub dlg_edit_cover_close
{
    $dlg_edit_cover->Close();
}

################################################################################

sub dlg_edit_cover_destroy
{
    $dlg_edit_cover->Destroy();
}

################################################################################

sub txt_edit_name_get{ return view::txt::get($txt_edit_name, @_); }
sub txt_edit_name_set{        view::txt::set($txt_edit_name, @_); }

################################################################################

sub txt_edit_filepath_get{ return view::txt::get($txt_edit_filepath, @_); }
sub txt_edit_filepath_set{        view::txt::set($txt_edit_filepath, @_); }

################################################################################

return 1;
