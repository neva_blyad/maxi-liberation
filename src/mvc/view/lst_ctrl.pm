=pod

=encoding UTF-8

=head1 NAME

Maxi Liberation
lst_ctrl.pm

=head1 VERSION

0.27

=head1 DESCRIPTION

EPUB/FB2/FB3 book meta tag editor.

This is the wxListCtrl widget of the View module.
View is GUI. Currently it implemented by wxWidgets/wxPerl.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This file is part of Maxi Liberation.

Maxi Liberation is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maxi Liberation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare wxListCtrl widget package of the View
package view::lst_ctrl;

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# CPAN modules
use Cwd;
use File::Basename;
use File::Spec::Functions;
use Try::Tiny;
use Wx;

# Path to binary
my $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib File::Spec::Functions::catfile($path, '..');

# Our own modules
#use model; # Model. View should not interact with the Model.
use view;  # View
use ctrl;  # Controller

################################################################################

sub row_add
{
    my $widget = shift;
    my $row    = shift;

    try
    {
        $row != 0 or
            die _T("Can't add header row. It's already present.");

        $row--;
        $widget->InsertStringItem($row, '');
    }
    catch
    {
        $ctrl::logger->error($_);
    }
}

################################################################################

sub row_remove
{
    my $widget = shift;
    my $row    = shift;

    try
    {
        $row != 0 or
            die _T("Can't delete header row");

        $row--;
        $widget->DeleteItem($row);
    }
    catch
    {
        $ctrl::logger->error($_);
    }
}

################################################################################

sub col_add
{
    my $widget = shift;
    my $col    = shift;

    $widget->InsertColumn($col, '');
}

################################################################################

sub col_remove
{
    my $widget = shift;
    my $col    = shift;

    $widget->DeleteColumn($col);
}

################################################################################

sub col_cnt_get
{
    my $widget = shift;

    return $widget->GetColumnCount();
}

################################################################################

sub row_cnt_get
{
    my $widget = shift;

    return $widget->GetItemCount() + 1;
}

################################################################################

sub get
{
    my $val;

    my $widget = shift;
    my $row    = shift;
    my $col    = shift;

    if ($row == 0)
    {
        $val = $widget->GetColumn($col)->GetText();
    }
    else
    {
        $row--;
        $val = $widget->GetItem($row, $col)->GetText();
    }

    return $val;
}

################################################################################

sub set
{
    my $widget = shift;
    my $row    = shift;
    my $col    = shift;
    my $val    = shift;

    if ($row == 0)
    {
        my $item = Wx::ListItem->new();
           $item->SetText($val);

        $widget->SetColumn($col, $item);
    }
    else
    {
        $row--;
        $widget->SetItem($row, $col, $val);
    }
}

################################################################################

sub meta_get
{
    my $meta;

    my $widget = shift;
    my $row    = shift;

    try
    {
        $row != 0 or
            die _T('No metadata can set for header row');

        $row--;
        $meta = $widget->GetItemData($row);
    }
    catch
    {
        $ctrl::logger->error($_);
    };

    return $meta;
}

################################################################################

sub meta_set
{
    my $widget = shift;
    my $row    = shift;
    my $meta   = shift;

    try
    {
        $row != 0 or
            die _T('No metadata should be set for header row');

        $row--;
        $widget->SetItemData($row, $meta);
    }
    catch
    {
        $ctrl::logger->error($_);
    };
}

################################################################################

sub clr
{
    my $widget = shift;

    $widget->DeleteAllItems();
}

################################################################################

sub width_set
{
    my $widget = shift;
    my $col    = shift;
    my $width  = shift;

    $widget->SetColumnWidth($col, $width);
}

################################################################################

sub sel_get
{
    my $sel;

    my $widget = shift;
    my $row    = shift;

    try
    {
        $row != 0 or
            die _T("Can't set selection to header row");

        $row--;
        $sel = $widget->GetItemState($row, Wx->wxLIST_STATE_SELECTED) == Wx->wxLIST_STATE_SELECTED ?
            1 :
            0;
    }
    catch
    {
        $ctrl::logger->error($_);
    };

    return $sel;
}

################################################################################

sub sel_set
{
    my $widget = shift;
    my $row    = shift;
    my $sel    = shift;

    try
    {
        $row != 0 or
            die _T("Can't get selection of header row");

        $row--;
        $widget->SetItemState($row,
                              $sel ? Wx->wxLIST_STATE_SELECTED : 1,
                              Wx->wxLIST_STATE_SELECTED);
    }
    catch
    {
        $ctrl::logger->error($_);
    };
}

################################################################################

sub sort
{
    my $widget = shift;

    $widget->SortItems(sub
                       {
                           my $a = shift;
                           my $b = shift;

                           return $a > $b;
                       });
}

################################################################################

return 1;
