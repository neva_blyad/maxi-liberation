=pod

=encoding UTF-8

=head1 NAME

Maxi Liberation
btn.pm

=head1 VERSION

0.27

=head1 DESCRIPTION

EPUB/FB2/FB3 book meta tag editor.

This is the wxButton widget of the View module.
View is GUI. Currently it implemented by wxWidgets/wxPerl.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This file is part of Maxi Liberation.

Maxi Liberation is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maxi Liberation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare wxButton widget package of the View
package view::btn;

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# CPAN modules
use Cwd;
use File::Basename;
use File::Spec::Functions;
use Wx;

# Path to binary
my $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib File::Spec::Functions::catfile($path, '..');

# Our own modules
#use model; # Model. View should not interact with the Model.
use view;  # View
use ctrl;  # Controller

################################################################################

sub en_set
{
    my $widget = shift;
    my $flag   = shift;

    if ($flag) { $widget->Enable();  }
    else       { $widget->Disable(); }
}

################################################################################

return 1;
