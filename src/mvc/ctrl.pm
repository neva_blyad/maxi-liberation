=pod

=encoding UTF-8

=head1 NAME

Maxi Liberation
ctrl.pm

=head1 VERSION

0.27

=head1 DESCRIPTION

EPUB/FB2/FB3 book meta tag editor.

This is the Controller module.
It is glue between View and Model.
Nothing GUI oriented here.
No business logic, just connecting things, i. e. UI callbacks.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This file is part of Maxi Liberation.

Maxi Liberation is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maxi Liberation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare Controller package
package ctrl;

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# Use experimental switch-case construct
use v5.14;
use feature qw(switch);
no warnings qw(experimental);

# Some I/O functions should fuck up everything
use autodie qw(open close);

# CPAN modules
use Config::Tiny;
use Cwd;
use Exporter qw(import);
use Encode;
use File::Basename;
use File::Find::Rule;
use File::HomeDir;
use File::Spec::Functions;
use Getopt::Long;
use Locale::gettext;
use Log::Log4perl;
use MIME::Types;
use Path::Tiny;
use Pod::Usage;
use Tie::IxHash;
use Try::Tiny;

# i18n & l10n.
# Make Alias _T() => Locale::gettext::gettext().
BEGIN
{
    #*_T = \&Locale::gettext::gettext;

    sub _T
    {
        my $msg = shift;

        return Encode::decode_utf8(Locale::gettext::gettext($msg));
    }

    our @EXPORT = qw(_T);
}

# Path to binary
my  $path;
our $prefix;
our $use_prefix;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    my $pattern;

    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);

    $pattern    = File::Spec::Functions::catfile('share', 'perl5', 'maxi-liberation', 'src', 'mvc');
    $prefix     = $path;
    $use_prefix = 1 if $prefix =~ s/\Q$pattern\E\z//i;
}

# Module path
use lib $path;
use lib File::Spec::Functions::catfile($path, '..', 'external');
use lib File::Spec::Functions::catfile($path, '..', '..');

# Our own modules
use model; # Model
use view;  # View

# External modules
require 'path_normalize_by_string_manipulation.pl'; # Normalized directory paths

# Constants
use constant NAME    => 'Maxi Liberation';
use constant VER     => 0.27;

use constant PLUGINS => $ctrl::use_prefix ?
                            File::Spec::Functions::catfile('..', 'perl5', 'maxi-liberation', 'plugins', '*.pm') :
                            File::Spec::Functions::catfile('plugins', '*.pm'); # Plugins

use constant LOCALE  => $ctrl::use_prefix ?
                            File::Spec::Functions::catfile('..', 'locale') :
                            'locale'; # Directory with translations
use constant DOMAIN  => $ctrl::use_prefix ? 'maxi-liberation' : 'messages'; # Tranlslation domain
use constant CHARSET => 'UTF-8';                                            # Character set of the translation domain

# Global Variables
my  $diff;
my  $miss;
my  $miss_;
our $logger;
my  $cfg;
my  $cfg_path;
my  $pattern_tab2;
my  $pattern_tab3;

################################################################################

sub preinit
{
    my $debug_uc;
    my $home = File::HomeDir->my_home();

    sub locale_init
    {
        my $locale;

        # Setup standard handler modes
        binmode STDIN,  ':encoding(UTF-8)';
        binmode STDOUT, ':encoding(UTF-8)';
        binmode STDERR, ':encoding(UTF-8)';

        # Setup locale
        $locale = Locale::gettext::setlocale(Locale::gettext->LC_MESSAGES);

        $ENV{'LANG'       } = $locale;
        $ENV{'LC_MESSAGES'} = $locale;

        if ($locale eq 'C')
        {
            Locale::gettext::textdomain(Locale::gettext->NULL);
        }
        else
        {
            Locale::gettext::textdomain(DOMAIN);
            Locale::gettext::bindtextdomain(DOMAIN, LOCALE);
            Locale::gettext::bind_textdomain_codeset(DOMAIN, CHARSET);
        }

        # Prepare some translations
        $diff  = _T('\(different across \d+ titles'); # Regex for different titles
        $miss  = _T('missing from \d+ titles\)');     # Regex for missing titles
        $miss_ = ' \(' . $miss;                       # Regex for missing titles (only missing)
    }

    sub cli_init
    {
        # Variables for CLI arguments
        my $debug_uc = shift;
        my $home     = shift;

        my $debug = 'OFF';
        my $dir;
        my $help;
        my $version;

        my %opt = ('debug'   => \$debug,
                   'dir'     => \$dir,
                   'help'    => \$help,
                   'version' => sub { Getopt::Long::VersionMessage(VER . "\n"); });

        # Parse CLI arguments
        #Getopt::Long::Configure('bundling');
        Getopt::Long::GetOptions(\%opt, 'debug|d=s',
                                        'dir=s',
                                        'help|h',
                                        'version|v');

        if (defined $help)
        {
            Pod::Usage::pod2usage('-exitval' => 0);
        }

        $$debug_uc = uc $debug;

        unless ($$debug_uc eq 'OFF'   ||
                $$debug_uc eq 'FATAL' ||
                $$debug_uc eq 'ERROR' ||
                $$debug_uc eq 'WARN'  ||
                $$debug_uc eq 'INFO'  ||
                $$debug_uc eq 'DEBUG' ||
                $$debug_uc eq 'TRACE' ||
                $$debug_uc eq 'ALL')
        {
            Pod::Usage::pod2usage('-exitval' => 1);
        }

        # Setup start directory
        $model::dir = (defined $dir && $dir ne '') ?
            Path::Tiny::path($dir) :
            $home;
    }

    sub logger_init
    {
        my $debug_uc = shift;

        # Setup logger
        $Data::Dumper::Terse = 1;

        Log::Log4perl->easy_init({'level'  => Log::Log4perl::Level::to_priority($debug_uc),
                                  'utf8'   => 1,
                                  'layout' => '[%r] %p %M %L %m%n'});
        $logger = Log::Log4perl->get_logger();
    }

    sub cfg_init
    {
        my $home = shift;

        # Open the config or create the new one
        my $cfg_dir = File::Spec::Functions::catfile($home, '.config');

        if (-d $cfg_dir)
        {
            $cfg_dir  = File::Spec::Functions::catfile($cfg_dir, 'maxi-liberation'); 
            $cfg_path = File::Spec::Functions::catfile($cfg_dir, 'maxi-liberation.cfg');  

            mkdir $cfg_dir;
        }
        else
        {
            $cfg_path = File::Spec::Functions::catfile($home, '.maxi-liberation.cfg');
        }

        $cfg = Config::Tiny->read($cfg_path);
        $cfg = Config::Tiny->new() unless defined $cfg;
    }

    locale_init();               # Setup locale
    cli_init(\$debug_uc, $home); # Parse CLI arguments
    logger_init($debug_uc);      # Setup logger
    cfg_init($home);             # Open the config or create the new one
}

################################################################################

sub init
{
    my $changed_cfg;

    # Read files from initial directory
    dir_upd();

    # Handle all plugins one by one
    #while (my $filepath = <PLUGINS>) # Breaks xgettext if I don't comment it :(
    while (my $filepath = glob PLUGINS)
    {
        try
        {
            # Parse filepath.
            #
            # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
            my ($filename_no_ext, undef, undef) = File::Basename::fileparse($filepath, qr/\.[^.]*/);

            # Initialize plugins which are turned on.
            # Add missing configuration.
            if (exists $cfg->{'plugins'}->{$filename_no_ext})
            {
                if ($cfg->{'plugins'}->{$filename_no_ext} == 1)
                {
                    eval "use plugins::$filename_no_ext;";       die $@ if $@;
                    eval "plugins::${filename_no_ext}::init();"; die $@ if $@;
                }
            }
            else
            {
                $cfg->{'plugins'}->{$filename_no_ext} = 0;
                $changed_cfg = 1;
            }
        }
        catch
        {
            $logger->error($_);
        };
    }

    # Save changes to configuration file
    $cfg->write($cfg_path) if $changed_cfg;
}

################################################################################

sub execute
{
    # Execute GUI loop
    view::loop();
}

################################################################################

sub closed
{
    my $changed_cfg;

    # Close all windows and menus
    view::frm_destroy();
    view::menu_destroy();
    view::dlg_plugins_destroy();
    view::dlg_add_tag_destroy();
    view::dlg_edit_tag_destroy();
    view::dlg_add_cover_destroy();
    view::dlg_edit_cover_destroy();

    # Deinitialize plugins, close their windows and menus
    #while (my $filepath = <PLUGINS) # Breaks xgettext if I don't comment it :(
    while (my $filepath = glob PLUGINS)
    {
        try
        {
            # Parse filepath.
            #
            # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
            my ($filename_no_ext, undef, undef) = File::Basename::fileparse($filepath, qr/\.[^.]*/);

            # Deinitialize plugins which are turned on.
            # Add missing configuration.
            if (exists $cfg->{'plugins'}->{$filename_no_ext})
            {
                if ($cfg->{'plugins'}->{$filename_no_ext} == 1)
                {
                    eval "plugins::${filename_no_ext}::deinit();"; die $@ if $@;
                    eval "no use plugins::$filename_no_ext;";      die $@ if $@;
                }
            }
            else
            {
                $cfg->{'plugins'}->{$filename_no_ext} = 0;
                $changed_cfg = 1;
            }
        }
        catch
        {
            $logger->error($_);
        };
    }

    # Save changes to configuration file
    $cfg->write($cfg_path) if $changed_cfg;
}

################################################################################

sub frm_title_upd
{
    my $cnt       = view::lst_ctrl_tab2_row_cnt_get();
    my $frm_title = _T(NAME);

    if ($cnt > 1)
    {
        my $author;
        my $title;

        my $filepath = view::lst_ctrl_tab2_get(1, 0);

        foreach my $tag (@{$model::titles{$filepath}{'tags'}})
        {
            if ($tag->{'name'} eq _T('Author'))
            {
                $author = $tag->{'val'};
            }
            elsif ($tag->{'name'} eq _T('Title'))
            {
                $title = $tag->{'val'};
            }

            last if $author && $title;
        }

        if ($cnt > 2) { $frm_title = _T(' and ') . ($cnt - 2) . _T(' more † ') . $frm_title; }
        else          { $frm_title = " † $frm_title"; }

        if ($author || $title)
        {
            $frm_title = "$title$frm_title"  if $title;
            $frm_title = " - $frm_title"     if $title && $author;
            $frm_title = "$author$frm_title" if $author;
        }
        else
        {
            $frm_title = "$filepath$frm_title";
        }
    }

    view::frm_title_set($frm_title);
}

################################################################################

sub dir_upd
{
    my $dir;
    my $max_depth;

    $model::dir = view::dir_get();
                  view::lst_box_filepath_clr();
    $max_depth  = view::txt_depth_get();

    if ($^O eq 'MSWin32') { $dir = "$model::dir\\";                                   }
    else                  { $dir = $model::dir eq '/' ? $model::dir : "$model::dir/"; }

    view::lbl_dir_set($dir);

    if ($max_depth < 1)
    {
        $max_depth = 1;
    }

    view::txt_depth_set($max_depth);

    foreach my $filepath (sort File::Find::Rule->or(File::Find::Rule->file->name(qr/^.*(?:\.epub|\.fb2|\.fb2.zip|\.fb3)\z/i))
                                               ->maxdepth($max_depth)
                                               ->relative()
                                               ->in($model::dir))
    {
        my $cnt;

        $filepath = Encode::decode_utf8($filepath);
        $cnt      = view::lst_box_filepath_cnt_get();
                    view::lst_box_filepath_add();
                    view::lst_box_filepath_set($cnt, $filepath);
    }

    view::dir_upd();
    view::dir_set($model::dir);

    %model::titles = ();

    lbl_cnt_upd();

    view::lst_ctrl_tab1_clr();
    view::btn_add_tab1_en_set(0);
    view::btn_edit_tab1_en_set(0);
    view::btn_remove_tab1_en_set(0);
    view::btn_save_tab1_en_set(0);
    view::cmb_pattern_tab2_en_set(0);

    foreach my $col (reverse 1 .. view::lst_ctrl_tab2_col_cnt_get() - 1)
    {
        view::lst_ctrl_tab2_col_remove($col);
    }

    view::lst_ctrl_tab2_clr();
    view::btn_preview_tab2_en_set(0);
    view::btn_save_tab2_en_set(0);
    view::cmb_pattern_tab3_en_set(0);
    view::lst_ctrl_tab3_clr();
    view::btn_preview_tab3_en_set(0);
    view::btn_save_tab3_en_set(0);
    view::lst_box_tab4_clr();
    view::img_tab4_clr();
    view::btn_add_tab4_en_set(0);
    view::btn_edit_tab4_en_set(0);
    view::btn_remove_tab4_en_set(0);
    view::btn_save_tab4_en_set(0);

    frm_title_upd();
}

################################################################################

sub lbl_cnt_upd
{
    my $cnt_sel;
    my $cnt;

    $cnt_sel = %model::titles; # Number of the currently handling titles
    $cnt     = view::lst_box_filepath_cnt_get();

    view::lbl_cnt_set("$cnt_sel/$cnt" . _T(' titles'));
}

################################################################################

sub tab1_upd
{
    my $err = 0;

    my $sel = shift;

    # Merge tags loaded from selected filepaths into one common
    @model::tags_all = ();

    foreach my $filepath (@$sel)
    {
        unless (exists $model::titles{$filepath})
        {
            $err++;
            next;
        }

        model::edit_tags_merge($filepath);
    }

    # Merged tag array is ready.
    # Now fill the table with data.
    view::lst_ctrl_tab1_clr();

    foreach my $idx (0 .. $#model::tags_all)
    {
        my $val_full;

        my $name        = $model::tags_all[$idx]{'name'};
        my $val         = $model::tags_all[$idx]{'val'};
        my $cnt         = $model::tags_all[$idx]{'cnt'};
        my $row         = $idx + 1;
        my $cnt_missing = @$sel - $err - $cnt;

        $val_full  = $model::tags_all[$idx]{'val'}                        if  defined $val;
        $val_full .= ' '                                                  if  defined $val && $cnt_missing > 0;
        $val_full .= '('                                                  if !defined $val || $cnt_missing > 0;
        $val_full .= _T('different across') . " $cnt " . _T('titles')     if !defined $val;
        $val_full .= ', '                                                 if !defined $val && $cnt_missing > 0;
        $val_full .= _T('missing from') . " $cnt_missing " . _T('titles') if                  $cnt_missing > 0;
        $val_full .= ')'                                                  if !defined $val || $cnt_missing > 0;

        view::lst_ctrl_tab1_row_add($row);
        view::lst_ctrl_tab1_set($row, 0, $name);
        view::lst_ctrl_tab1_set($row, 1, $val_full);

        $row++;
    }

    if ($err == @$sel)
    {
        view::btn_add_tab1_en_set(0);
        view::btn_save_tab1_en_set(0);
    }
    else
    {
        view::btn_add_tab1_en_set(1);
        view::btn_save_tab1_en_set(1);
    }
}

################################################################################

sub tab2_preview
{
    my $row_cnt;
    my $col_cnt;
    my $have_tags;

    $pattern_tab2 = view::cmb_pattern_tab2_get();
    $row_cnt      = view::lst_ctrl_tab2_row_cnt_get();
    $col_cnt      = view::lst_ctrl_tab2_col_cnt_get();

    # Remove old columns
    foreach my $col (reverse 1 .. $col_cnt - 1)
    {
        view::lst_ctrl_tab2_col_remove($col);

        $col_cnt--;
    }

    # Fill table control with tags parsed from each filepath
    foreach my $row (1 .. $row_cnt - 1)
    {
        my $filepath;
        my $pattern;
        my $tags;

        $filepath = view::lst_ctrl_tab2_get($row, 0);
        $pattern  = ($pattern_tab2 eq '') ?
            $pattern_tab2 :
            path_normalize_by_string_manipulation(Path::Tiny::path($pattern_tab2));
        $tags     = model::tags_from_filepath_parse($filepath, $pattern);

        # Fill table control with tags
        foreach my $tag (@$tags)
        {
            my $exists_col;

            # Fill existing column
            foreach my $col (1 .. $col_cnt - 1)
            {
                my $header = view::lst_ctrl_tab2_get(0,    $col);
                my $val    = view::lst_ctrl_tab2_get($row, $col);

                if ($tag->{'name'} eq $header &&
                    $val eq '')
                {
                    view::lst_ctrl_tab2_set($row, $col, $tag->{'val'});

                    $exists_col = 1;
                    last;
                }
            }

            # Add missing column if not exists
            unless ($exists_col)
            {
                view::lst_ctrl_tab2_col_add($col_cnt);
                view::lst_ctrl_tab2_width_set($col_cnt, 160); # Enought to place three columns with tags without scrolling
                view::lst_ctrl_tab2_set(0, $col_cnt, $tag->{'name'});
                view::lst_ctrl_tab2_set($row, $col_cnt, $tag->{'val'});

                $col_cnt++;
            }
        }

        $have_tags = 1 if exists $model::titles{$filepath};
    }

    view::btn_preview_tab2_en_set(0);
    view::btn_save_tab2_en_set($col_cnt > 1 && $have_tags ? 1 : 0);
}

################################################################################

sub tab3_preview
{
    my $en_save_tab3;

    $pattern_tab3 = view::cmb_pattern_tab3_get();

    foreach my $row (1 .. view::lst_ctrl_tab3_row_cnt_get() - 1)
    {
        my $filepath = view::lst_ctrl_tab3_get($row, 0);
        my $new      = model::filepath_from_tags_parse($filepath, $pattern_tab3);

        $new =  Path::Tiny::path($new);
        $new =~ s|^\Q$model::dir\E/||i;
        $new =~ s|^\Q$model::dir\E\\||i;

        $en_save_tab3 = 1 if $filepath ne $new;

        view::lst_ctrl_tab3_set($row, 1, $new);
    }

    view::btn_preview_tab3_en_set(0);
    view::btn_save_tab3_en_set($en_save_tab3);
}

################################################################################

sub tab4_upd
{
    my $err = 0;

    my $sel = shift;

    # Merge covers loaded from selected filepaths into one common
    @model::covers_all = ();

    foreach my $filepath (@$sel)
    {
        unless (exists $model::titles{$filepath})
        {
            $err++;
            next;
        }

        model::edit_covers_merge($filepath);
    }

    # Merged cover array is ready.
    # Now fill the table with data.
    view::lst_box_tab4_clr();
    view::img_tab4_clr();

    foreach my $idx (0 .. $#model::covers_all)
    {
        my $name_full;

        my $name        = $model::covers_all[$idx]{'name'};
        my $have_cover  = exists $model::covers_all[$idx]{'filepath_abs'} ||
                          exists $model::covers_all[$idx]{'bin'};
        my $cnt         = $model::covers_all[$idx]{'cnt'};
        my $cnt_missing = @$sel - $err - $cnt;

        $name_full  = $name;
        $name_full .= ' ('                                                 if !$have_cover || $cnt_missing > 0;
        $name_full .= _T('different across') . " $cnt " . _T('titles')     if !$have_cover;
        $name_full .= ', '                                                 if !$have_cover && $cnt_missing > 0;
        $name_full .= _T('missing from') . " $cnt_missing " . _T('titles') if                 $cnt_missing > 0;
        $name_full .= ')'                                                  if !$have_cover || $cnt_missing > 0;

        $cnt = view::lst_box_tab4_cnt_get();
               view::lst_box_tab4_add();
               view::lst_box_tab4_set($cnt, $name_full);
    }

    if ($err == @$sel)
    {
        view::btn_add_tab4_en_set(0);
        view::btn_save_tab4_en_set(0);
    }
    else
    {
        view::btn_add_tab4_en_set(1);
        view::btn_save_tab4_en_set(1);
    }
}

################################################################################

sub id_selected_deselected
{
    my $sel = shift;

    if (@$sel == 0)
    {
        view::img_tab4_clr();
        view::btn_edit_tab4_en_set(0);
        view::btn_remove_tab4_en_set(0);
    }
    else
    {
        my $idx = $$sel[0];

        if (exists $model::covers_all[$idx]{'checksum'})
        {
            my $filepath = $model::covers_all[$idx]{'filepath_abs'} if exists $model::covers_all[$idx]{'filepath_abs'};
            my $bin      = $model::covers_all[$idx]{'bin'         } if exists $model::covers_all[$idx]{'bin'         };
            my $mime     = MIME::Types->new();
            my $type     = exists $model::covers_all[$idx]{'filepath_abs'} ? $mime->mimeTypeOf($filepath) :
                                                                             $model::covers_all[$idx]{'type'};

            given ($type)
            {
                view::img_tab4_set(exists $model::covers_all[$idx]{'filepath_abs'} ? $filepath : $bin, view->GIF)  when $_ eq 'image/gif';
                view::img_tab4_set(exists $model::covers_all[$idx]{'filepath_abs'} ? $filepath : $bin, view->JPEG) when $_ eq 'image/jpeg';
                view::img_tab4_set(exists $model::covers_all[$idx]{'filepath_abs'} ? $filepath : $bin, view->PNG)  when $_ eq 'image/png';
            }
        }
        else
        {
            view::img_tab4_clr();
        }

        view::btn_edit_tab4_en_set(1);
        view::btn_remove_tab4_en_set(1);
    }
}

################################################################################

sub lst_box_filepath_selected
{
    tie my %sel, 'Tie::IxHash';

    my $cnt = view::lst_ctrl_tab2_row_cnt_get();

    my $sel = view::lst_box_filepath_sel_get();
       %sel = map { view::lst_box_filepath_get($_) => $_ } @$sel;
    my @sel = keys %sel;

    my @old = grep { not exists $sel{$_}           } keys %model::titles;
    my @new = grep { not exists $model::titles{$_} } keys %sel;

    # Handle deselected filepaths
    foreach my $filepath (@old)
    {
        my $row;

        foreach my $row_ (1 .. $cnt - 1)
        {
            if (view::lst_ctrl_tab2_get($row_, 0) eq $filepath)
            {
                $row = $row_;
                last;
            }
        }

        model::title_unload($filepath);
        view::lst_ctrl_tab2_row_remove($row);
        view::lst_ctrl_tab3_row_remove($row);

        $cnt--;
    }

    # Handle new selected filepaths
    foreach my $filepath (@new)
    {
        my $idx;
        my $row;

        model::title_load($filepath); # Parse the file meta data
        next unless exists $model::titles{$filepath};

        $idx = $sel{$filepath};
        $row = %model::titles;

        view::lst_ctrl_tab2_row_add($row);
        view::lst_ctrl_tab2_set($row, 0, $filepath);
        view::lst_ctrl_tab2_meta_set($row, $idx);
        view::lst_ctrl_tab2_sort();

        view::lst_ctrl_tab3_row_add($row);
        view::lst_ctrl_tab3_set($row, 0, $filepath);
        view::lst_ctrl_tab3_meta_set($row, $idx);
        view::lst_ctrl_tab3_sort();
    }

    lbl_cnt_upd();
    tab1_upd(\@sel);

    view::btn_edit_tab1_en_set(0);
    view::btn_remove_tab1_en_set(0);

    if (@$sel > 0)
    {
        view::cmb_pattern_tab2_en_set(1);
        view::cmb_pattern_tab3_en_set(1);
    }
    else
    {
        view::cmb_pattern_tab2_en_set(0);
        view::cmb_pattern_tab3_en_set(0);
    }

    tab2_preview();
    tab3_preview();
    tab4_upd(\@sel);

    view::btn_edit_tab4_en_set(0);
    view::btn_remove_tab4_en_set(0);

    # Set the main window title
    frm_title_upd();
}

################################################################################

sub btn_menu_clicked
{
    view::btn_menu_popup_show();
}

################################################################################

sub btn_scan_clicked
{
    dir_upd();
}

################################################################################

sub lst_ctrl_tab1_selected
{
    view::btn_edit_tab1_en_set(1);
    view::btn_remove_tab1_en_set(1);
}

################################################################################

sub lst_ctrl_tab1_deselected
{
    my $sel = 0;

    foreach my $row (1 .. view::lst_ctrl_tab1_row_cnt_get() - 1)
    {
        if (view::lst_ctrl_tab1_sel_get($row))
        {
            $sel = 1;
            last;
        }
    }

    unless ($sel)
    {
        view::btn_edit_tab1_en_set(0);
        view::btn_remove_tab1_en_set(0);
    }
}

################################################################################

sub btn_add_tab1_clicked
{
    view::dlg_add_tag_show();
}

################################################################################

sub btn_edit_tab1_clicked
{
    foreach my $row (1 .. view::lst_ctrl_tab1_row_cnt_get() - 1)
    {
        if (view::lst_ctrl_tab1_sel_get($row))
        {
            my $tag = view::lst_ctrl_tab1_get($row, 0);
            my $val = view::lst_ctrl_tab1_get($row, 1);

            view::cmb_edit_tag_set(undef, $tag);
            view::txt_edit_val_set($val);

            view::dlg_edit_tag_show();

            last;
        }
    }
}

################################################################################

sub btn_remove_tab1_clicked
{
    foreach my $row (reverse 1 .. view::lst_ctrl_tab1_row_cnt_get() - 1)
    {
        if (view::lst_ctrl_tab1_sel_get($row))
        {
            model::edit_tags_remove($row - 1);
            view::lst_ctrl_tab1_row_remove($row)
        }
    }

    frm_title_upd();
    view::btn_edit_tab1_en_set(0);
    view::btn_remove_tab1_en_set(0);
    tab3_preview();
}

################################################################################

sub btn_save_tab1_clicked
{
    foreach my $row (1 .. view::lst_ctrl_tab2_row_cnt_get() - 1)
    {
        my $filepath = view::lst_ctrl_tab2_get($row, 0);

        next unless exists $model::titles{$filepath};
        model::edit_tags_save($filepath);
    }
}

################################################################################

sub cmb_pattern_tab2_changed
{
    my $pattern = view::cmb_pattern_tab2_get();

    view::btn_preview_tab2_en_set($pattern eq $pattern_tab2 ? 0 : 1);
}

################################################################################

sub btn_preview_tab2_clicked
{
    tab2_preview();
}

################################################################################

sub btn_save_tab2_clicked
{
    my @sel;

    my $row_cnt = view::lst_ctrl_tab2_row_cnt_get();
    my $col_cnt = view::lst_ctrl_tab2_col_cnt_get();

    foreach my $row (1 .. $row_cnt - 1)
    {
        my @tags;
        my $filepath = view::lst_ctrl_tab3_get($row, 0);

        push @sel, $filepath;
        next unless exists $model::titles{$filepath};

        foreach my $col (1 .. $col_cnt - 1)
        {
            my %tag;

            $tag{'name'} = view::lst_ctrl_tab2_get(0,    $col);
            $tag{'val' } = view::lst_ctrl_tab2_get($row, $col);

            push @tags, \%tag;
        }

        model::tags_from_filepath_save($filepath, \@tags);
    }

    frm_title_upd();
    tab1_upd(\@sel);
    tab3_preview();
}

################################################################################

sub cmb_pattern_tab3_changed
{
    my $pattern = view::cmb_pattern_tab3_get();

    view::btn_preview_tab3_en_set($pattern_tab3 eq $pattern ? 0 : 1);
}

################################################################################

sub btn_preview_tab3_clicked
{
    tab3_preview();
}

################################################################################

sub btn_save_tab3_clicked
{
    my $res;

    foreach my $row (1 .. view::lst_ctrl_tab3_row_cnt_get() - 1)
    {
        my $filepath = view::lst_ctrl_tab3_get($row, 0);
        my $new      = view::lst_ctrl_tab3_get($row, 1);

        if ($filepath ne $new)
        {
            if (model::filepath_from_tags_save($filepath, $new))
            {
                $res = 1;
            }
        }
    }

    if ($res)
    {
        dir_upd();
    }
}

################################################################################

sub lst_box_tab4_selected
{
    my $sel = view::lst_box_tab4_sel_get();

    id_selected_deselected($sel);
}

################################################################################

sub img_tab4_resized
{
    view::img_tab4_upd();
}

################################################################################

sub btn_add_tab4_clicked
{
    view::dlg_add_cover_show();
}

################################################################################

sub btn_edit_tab4_clicked
{
    my $sel      = view::lst_box_tab4_sel_get();
    my $idx      = $$sel[0];
    my $name     = view::lst_box_tab4_get($idx);
    my $filepath = (exists $model::covers_all[$idx]{'filepath_abs'}) ?
                       $model::covers_all[$idx]{'filepath_abs'} : '';

    view::txt_edit_name_set($name);
    view::txt_edit_filepath_set($filepath);

    view::dlg_edit_cover_show();
}

################################################################################

sub btn_remove_tab4_clicked
{
    my $sel = view::lst_box_tab4_sel_get();
    my $idx = $$sel[0];

    model::edit_covers_remove($idx);
    view::lst_box_tab4_remove($idx);

    $sel = view::lst_box_tab4_sel_get();

    id_selected_deselected($sel);
}

################################################################################

sub btn_save_tab4_clicked
{
    foreach my $row (1 .. view::lst_ctrl_tab2_row_cnt_get() - 1)
    {
        my $filepath = view::lst_ctrl_tab2_get($row, 0);

        next unless exists $model::titles{$filepath};
        model::edit_covers_save($filepath);
    }
}

################################################################################

sub menu_plugins_clicked
{
    my $changed_cfg;

    # Show plugins
    view::check_lst_plugins_clr();

    # Handle all plugins one by one
    #while (my $filepath = <PLUGINS>) # Breaks xgettext if I don't comment it :(
    while (my $filepath = glob PLUGINS)
    {
        try
        {
            my  $pod;
            my  $filename;
            my ($filename_no_ext, undef, undef);

            state $idx = 0;

            $pod = '';

            # Read POD section of plugin
            open FILE, '> :encoding(UTF-8)', \$pod;
            $filename = File::Basename::basename($filepath);
            Pod::Usage::pod2usage('-exitval'  => 'NOEXIT',
                                  '-verbose'  => 99,
                                  '-sections' => 'DESCRIPTION',
                                  '-output'   => \*FILE,
                                  '-input'    => $filepath);
            $pod =  Encode::decode_utf8($pod);
            $pod =~ s/Description:\n//;
            $pod =~ s/\r|\n$//g;
            close FILE;

            # Parse filepath.
            #
            # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
            ($filename_no_ext, undef, undef) = File::Basename::fileparse($filepath, qr/\.[^.]*/);

            # Now we have plugin name and its description.
            # Add them to check list.
            view::check_lst_plugins_add();
            view::check_lst_plugins_set($idx, Encode::decode_utf8($filename_no_ext) . "\n$pod");
            view::check_lst_plugins_meta_set($idx, $filename);

            # Set the checkbox if needed.
            # Update plugin configuration.
            if (exists $cfg->{'plugins'}->{$filename_no_ext})
            {
                if ($cfg->{'plugins'}->{$filename_no_ext} == 1)
                {
                    view::check_lst_plugins_check($idx, 1);
                }
            }
            else
            {
                $cfg->{'plugins'}->{$filename_no_ext} = 0;
                $changed_cfg = 1;
            }

            $idx++;
        }
        catch
        {
            $logger->error($_);
        };
    }

    # Save changes to configuration file
    $cfg->write($cfg_path) if $changed_cfg;

    # Show plugin window
    view::dlg_plugins_show();
}

################################################################################

sub menu_about_clicked
{
    view::dlg_about_show();
}

################################################################################

sub check_lst_plugins_checked
{
    my $self  = shift;
    my $event = shift;

    # Disable/enable checked plugin.
    # Initialize those which are turned on.
    try
    {
        my ($filename_no_ext, undef, undef);

        my $idx      = view::check_lst_plugins_sel_get($event);
        my $filename = view::check_lst_plugins_meta_get($idx);
        my $filepath = File::Spec::Functions::catfile('src', 'plugins', $filename);

        # Parse filepath.
        #
        # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
        ($filename_no_ext, undef, undef) = File::Basename::fileparse($filepath, qr/\.[^.]*/);

        # Initialize/deinitilize plugin.
        # Update its configuration.
        if (view::check_lst_plugins_sel_get($idx))
        {
            $cfg->{'plugins'}->{$filename_no_ext} = 1;

            eval "use plugins::$filename_no_ext;";       die $@ if $@;
            eval "plugins::${filename_no_ext}::init();"; die $@ if $@;
        }
        else
        {
            $cfg->{'plugins'}->{$filename_no_ext} = 0;

            eval "plugins::${filename_no_ext}::deinit();"; die $@ if $@;
            eval "no use plugins::$filename_no_ext;";      die $@ if $@;
        }
    }
    catch
    {
        $logger->error($_);
    };

    # Save changes to configuration file
    $cfg->write($cfg_path);
}

################################################################################

sub cmb_add_tag_selected
{
    my $tag;

    $tag =  view::cmb_add_tag_get();
    $tag =~ s/^<//;
    $tag =~ s/>\z//;

    view::cmb_add_tag_set(undef, $tag);
}

################################################################################

sub btn_add_tag_cancel_clicked
{
    view::dlg_add_tag_close();
}

################################################################################

sub btn_add_tag_clicked
{
    my $val;
    my $tag;
    my $res;

    $val = view::txt_add_val_get();

    if ($val =~ /$diff/ ||
        $val =~ /$miss/)
    {
        return;
    }

    $tag = view::cmb_add_tag_get();
    $res = model::edit_tags_add($tag, $val);

    given ($res)
    {
        when ($_ == model->RES_OK)
        {
            my $cnt = view::lst_ctrl_tab1_row_cnt_get();

            frm_title_upd();
            view::lst_ctrl_tab1_row_add($cnt);
            view::lst_ctrl_tab1_set($cnt, 0, $tag);
            view::lst_ctrl_tab1_set($cnt, 1, $val);
            tab3_preview();
        }

        when ($_ == model->RES_EMPTY) { view::dlg_err_show(_T('The value for the tag to be added cannot be empty')); }
        default                       { $ctrl::logger->error(_T('API error')); }
    }
}

################################################################################

sub cmb_edit_tag_selected
{
    my $tag;

    $tag =  view::cmb_edit_tag_get();
    $tag =~ s/^<//;
    $tag =~ s/>\z//;

    view::cmb_edit_tag_set(undef, $tag);
}

################################################################################

sub btn_edit_tag_cancel_clicked
{
    view::dlg_edit_tag_close();
}

################################################################################

sub btn_edit_tag_clicked
{
    use constant
    {
        NOT_FOUND_TAG  => 0,
        NOT_EDITED_TAG => 1,
        EDITED_TAG     => 2,
        UPDATED_DLG    => 3,
    };

    my $state = NOT_FOUND_TAG;

    foreach my $row (1 .. view::lst_ctrl_tab1_row_cnt_get() - 1)
    {
        if (view::lst_ctrl_tab1_sel_get($row))
        {
            if ($state == NOT_FOUND_TAG)
            {
                my $val;
                my $tag;
                my $val_;
                my $res;

                $val   = view::txt_edit_val_get();
                $state = NOT_EDITED_TAG;

                if ($val =~ /$diff/)
                {
                    last;
                }

                $tag  = view::cmb_edit_tag_get();
                $val_ = $val;

                if ($val_ =~ s/$miss_//) { $res = model::edit_tags_edit($row - 1, $tag, $val_, 0); }
                else                     { $res = model::edit_tags_edit($row - 1, $tag, $val_, 1); }

                given ($res)
                {
                    when ($_ == model->RES_OK)
                    {
                        view::lst_ctrl_tab1_set($row, 0, $tag);
                        view::lst_ctrl_tab1_set($row, 1, $val);

                        frm_title_upd();
                        tab3_preview();

                        view::lst_ctrl_tab1_sel_set($row, 0);

                        $state = EDITED_TAG;
                    }

                    when ($_ == model->RES_EMPTY)
                    {
                        view::dlg_err_show(_T('The value for the tag to be edited cannot be empty'));
                        last;
                    }

                    default
                    {
                        $ctrl::logger->error(_T('API error'));
                        last;
                    }
                }
            }
            else
            {
                my $tag = view::lst_ctrl_tab1_get($row, 0);
                my $val = view::lst_ctrl_tab1_get($row, 1);

                view::cmb_edit_tag_set(undef, $tag);
                view::txt_edit_val_set($val);

                $state = UPDATED_DLG;

                last;
            }
        }
    }

    view::dlg_edit_tag_close() if $state == EDITED_TAG;
}

################################################################################

sub btn_add_filepath_clicked
{
    my  $old;
    my ($filename_no_ext, $dir, $ext);
    my  $filepath;

    $old = view::txt_add_filepath_get();

    # Parse filepath.
    #
    # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
    ($filename_no_ext, $dir, $ext) = File::Basename::fileparse($old, qr/\.[^.]*/);

    $filepath = view::dlg_filepath_show($filename_no_ext . $ext,
                                        $dir);

    view::txt_add_filepath_set($filepath) if defined $filepath;
}

################################################################################

sub btn_add_cover_cancel_clicked
{
    view::dlg_add_cover_close();
}

################################################################################

sub btn_add_cover_clicked
{
    my $name;
    my $filepath;
    my $res;

    $name = view::txt_add_name_get();

    if ($name =~ /$diff/ ||
        $name =~ /$miss/)
    {
        return;
    }

    $filepath = view::txt_add_filepath_get();
    $res      = model::edit_covers_add($name, $filepath);

    given ($res)
    {
        when ($_ == model->RES_OK)
        {
            my $cnt = view::lst_box_tab4_cnt_get();

            view::lst_box_tab4_add();
            view::lst_box_tab4_set($cnt, $name);
        }

        when ($_ == model->RES_EXISTS_ALREADY) { view::dlg_err_show(_T('The cover with name ') . $name . _T(' already exists')); }
        when ($_ == model->RES_IO_ERR)         { view::dlg_err_show(_T('The cover with filepath ') . $filepath . _T(' read error')); }
        default                                { $ctrl::logger->error(_T('API error')); }
    }
}

################################################################################

sub btn_edit_filepath_clicked
{
    my  $old;
    my ($filename_no_ext, $dir, $ext);
    my  $filepath;

    $old = view::txt_edit_filepath_get();

    # Parse filepath.
    #
    # $_[1] = '/usr/local/src/', $_[0] = 'perl-5.6.1.tar', $_[2] = '.gz'.
    ($filename_no_ext, $dir, $ext) = File::Basename::fileparse($old, qr/\.[^.]*/);

    $filepath = view::dlg_filepath_show($filename_no_ext . $ext,
                                        $dir);

    view::txt_edit_filepath_set($filepath) if defined $filepath;
}

################################################################################

sub btn_edit_cover_cancel_clicked
{
    view::dlg_edit_cover_close();
}

################################################################################

sub btn_edit_cover_clicked
{
    my $name;
    my $sel;
    my $idx;
    my $filepath;
    my $name_;
    my $res;

    $name = view::txt_edit_name_get();

    if ($name =~ /$diff/)
    {
        return;
    }

    $sel      = view::lst_box_tab4_sel_get();
    $idx      = $$sel[0];
    $filepath = view::txt_edit_filepath_get();
    $name_    = $name;

    if ($name_ =~ s/$miss_//) { $res = model::edit_covers_edit($idx, $name_, $filepath, 0); }
    else                      { $res = model::edit_covers_edit($idx, $name_, $filepath, 1); }

    given ($res)
    {
        when ($_ == model->RES_OK)
        {
            view::lst_box_tab4_set($idx, $name);
            id_selected_deselected($sel);
            view::dlg_edit_cover_close();
        }

        when ($_ == model->RES_EXISTS_ALREADY) { view::dlg_err_show(_T('The cover with name ') . $name . _T(' already exists')); }
        when ($_ == model->RES_IO_ERR)         { view::dlg_err_show(_T('The cover with filepath ') . $filepath . _T(' read error')); }
        default                                { $ctrl::logger->error(_T('API error')); }
    }
}

################################################################################

return 1;
