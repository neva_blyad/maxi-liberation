#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

Maxi Liberation
maxi-liberation.pl

=head1 VERSION

0.27

=head1 DESCRIPTION

EPUB/FB2/FB3 book meta tag editor.

Maxi means maximum expression of digital text form.
Liberation is library.

The program is Free/Libre and Open Source Software (FLOSS). There is only one
right software license in the world.

The program is cross-platform: GNU/Linux, UNIX, M$ Windows, Apple macOS are
all supported. It is based on wxWidgets library, so the GUI is native for each
OS, for example, it uses GTK+ for GNU, MFC for Windows, Cocoa for macOS. No
fucking control emulation like Qt or Java does.

The program has been inspired and influenced by Ex Falso, popular meta tag
editor for audio files. Many thanks to its authors! Used it regularly for my
music collection.

The program is written in Perl. All modules like ZIP or XML are just bindings
to C dynamic libraries, so it is fast as C anyway.

Now EPUB, FB2 and FB3 formats are supported. AZW/MOBI are planned.

=head1 USAGE

./maxi-liberation [OPTION]

     --dir=DIRECTORY   Give a directory to open at start
 -d, --debug=LOG_LEVEL Enable debug output: OFF, FATAL, ERROR, WARN, INFO,
                                            DEBUG, TRACE, ALL
 -h, --help            Display this help and exit
 -v, --version         Output version information and exit

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This file is part of Maxi Liberation.

Maxi Liberation is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maxi Liberation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# CPAN modules
use Cwd;
use File::Basename;
use File::Spec::Functions;

# Path to binary
my $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib File::Spec::Functions::catfile($path, 'mvc');

# Our own modules
use model; # Model
use view;  # View
use ctrl;  # Controller

# The main program.
# This application uses a Model-View-Controller architecture.
# Pre-initialize MVC modules.
model::preinit();
view::preinit();
ctrl::preinit();

# Initialize MVC modules
model::init();
view::init();
ctrl::init();

# Let Controller do all the application job
ctrl::execute();
