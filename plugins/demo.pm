=pod

=encoding UTF-8

=head1 NAME

Maxi Liberation
demo.pm

=head1 VERSION

0.27

=head1 DESCRIPTION

This is the Demo plugin.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This file is part of Maxi Liberation.

Maxi Liberation is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maxi Liberation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare Demo plugin package
package plugins::demo;

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# CPAN modules
use Cwd;
use Encode;
use File::Basename;
use File::Spec::Functions;
use Locale::gettext;
use Wx;

# Path to binary
my $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib File::Spec::Functions::catfile($path, '..', 'src', 'mvc');

# Our own modules
use model; # Model
use view;  # View
use ctrl;  # Controller

# Constants
use constant ID_BTN_DEMO => 1488;

use constant XRC => $ctrl::use_prefix ?
                        File::Spec::Functions::catfile('..', 'perl5', 'maxi-liberation', 'plugins', 'demo.xrc') :
                        File::Spec::Functions::catfile('plugins', 'demo.xrc'); # Name of our XRC GUI resource file

use constant LOCALE  => $ctrl::use_prefix ? File::Spec::Functions::catfile('..', 'locale') : 'locale'; # Directory with translations
use constant DOMAIN  => 'demo';                                                                        # Tranlaslation domain
use constant CHARSET => 'UTF-8';                                                                       # Character set of the translation domain

# Global Variables
my $btn_demo;
my $dlg_demo;
my $btn_demo_ok;

################################################################################

no warnings qw(redefine);
sub _T
{
    my $msg = shift;

    return Encode::decode_utf8(Locale::gettext::dgettext(DOMAIN, $msg));
}

################################################################################

sub init
{
    my $sizer;
    my $xrc = Wx::XmlResource->new();

    ############################################################################

    sub btn_demo_clicked
    {
        # Show the demo dialog
        $dlg_demo->ShowModal();
    }

    ############################################################################

    sub btn_demo_ok_clicked
    {
        # Close the demo dialog
        $dlg_demo->Close();
    }

    ############################################################################

    # Setup locale
    $view::locale->AddCatalog(DOMAIN);

    Locale::gettext::bindtextdomain(DOMAIN, LOCALE);
    Locale::gettext::bind_textdomain_codeset(DOMAIN, CHARSET);

    # Draw demo button
    $btn_demo = Wx::Button->new($view::panel_bottom, ID_BTN_DEMO, _T('&Demo'));
    $btn_demo->SetToolTip(_T('Show the demo message box'));

    $sizer = $view::btn_menu->GetContainingSizer();
    $sizer->Insert(1, $btn_demo, 0, Wx->wxALIGN_CENTER_VERTICAL | Wx->wxALL, 5);

    $view::panel_bottom->Layout();

    # Setup event for the demo button
    Wx::Event::EVT_BUTTON($view::panel_bottom, ID_BTN_DEMO, \&btn_demo_clicked);

    # Load the XRC file
    $xrc->InitAllHandlers();
    $xrc->Load(XRC);

    # Find/load the widgets
    $dlg_demo    = $xrc->LoadDialog(undef, 'dlg_demo');
    $btn_demo_ok = $dlg_demo->FindWindow('btn_demo_ok');

    # Setup event for the demo button
    Wx::Event::EVT_BUTTON($dlg_demo, Wx::XmlResource::GetXRCID('btn_demo_ok'), \&btn_demo_ok_clicked);

    # Demo message box
    Wx::MessageBox(_T('This is demo plugin'), _T('Hello'));
}

################################################################################

sub deinit
{
    $btn_demo->Destroy();
    $dlg_demo->Destroy();

    $view::panel_bottom->Layout();
}

################################################################################

return 1;
