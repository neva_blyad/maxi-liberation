#    maxi-liberation.rb
#    Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                       <neva_blyad@lovecri.es>
#
#    This file is part of Maxi Liberation.
#
#    Maxi Liberation is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Maxi Liberation is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

class MaxiLiberation < Formula
  desc "EPUB/FB3 book meta tag editor"
  homepage "http://www.lovecry.pt/maxi-liberation/"
  license "GPL-3.0-or-later"
  url "http://www.lovecry.pt/dl/maxi-liberation_0.27.tar.gz"
  #sha256 ""

  depends_on "perl"
  depends_on "wxmac"

  def install
      prefix.install "Maxi Liberation.app"

      prefix.install "bin"
      prefix.install "share"
  end
end
