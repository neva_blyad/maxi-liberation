#    Makefile
#    Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                       <neva_blyad@lovecri.es>
#
#    This file is part of Maxi Liberation.
#
#    Maxi Liberation is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Maxi Liberation is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Maxi Liberation.  If not, see <https://www.gnu.org/licenses/>.

.PHONY: all xrc pot po_init po_upd mo linux macos install deinstall clean

VER := 0.27
PREFIX ?= /usr/local/

CP := cp

PERL_FILES     = $(shell find src/     -type f -name '*.pl' -or -name '*.pm')
PERL_PLUGINS   = $(shell find plugins/ -type f -name '*.pm')
FBP_PLUGINS    = $(shell find plugins/ -type f -name '*.fbp')
PLUGINS        = $(shell find plugins/ -type f -not -name '*.fbp')
LOCALE_FILES   = $(shell find locale/  -type f -name 'messages.mo')
LOCALE_PLUGINS = $(shell find locale/  -type f -name '*.mo' -and -not -name 'messages.mo')

# Alias for the generating XRC and MO files
all: xrc mo

# Generate XRC resource files
xrc:
	# XRC
	wxformbuilder -g maxi-liberation.fbp
	
	# Plugin XRC
	for FILEPATH in $(FBP_PLUGINS);  \
	do                               \
		wxformbuilder -g $$FILEPATH; \
	done

# Generate POT files
pot: xrc
	# XRC
	wxrc --gettext maxi-liberation.xrc |    \
	                                        \
	xgettext --output=locale/messages.pot   \
	         --language=Perl                \
	         --from-code=UTF-8              \
	         --add-comments                 \
	         --keyword=_                    \
	         --sort-by-file                 \
	         --package-name=maxi-liberation \
	         --package-version=$(VER)       \
	                                        \
	         -
	
	# Perl sources
	xgettext --output=locale/messages.pot   \
	         --language=Perl                \
	         --from-code=UTF-8              \
	         --join-existing                \
	         --add-comments                 \
	         --keyword=_T                   \
	         --sort-by-file                 \
	         --package-name=maxi-liberation \
	         --package-version=$(VER)       \
	                                        \
	         $(PERL_FILES)
	
	# Plugin XRC
	for FILEPATH in $(FBP_PLUGINS);                    \
	do                                                 \
		FILENAME=`basename $$FILEPATH`;                \
		FILENAME_NO_EXT=$${FILENAME%.*};               \
		                                               \
		wxrc --gettext plugins/$$FILENAME_NO_EXT.xrc | \
		                                               \
		xgettext --output=locale/$$FILENAME_NO_EXT.pot \
		         --language=Perl                       \
		         --from-code=UTF-8                     \
		         --add-comments                        \
		         --keyword=_                           \
		         --sort-by-file                        \
		         --package-name=$$FILENAME_NO_EXT      \
		         --package-version=$(VER)              \
		                                               \
		         -;                                    \
	done
	
	# Perl plugin sources
	for FILEPATH in $(PERL_PLUGINS);                   \
	do                                                 \
		FILENAME=`basename $$FILEPATH`;                \
		FILENAME_NO_EXT=$${FILENAME%.*};               \
		                                               \
		xgettext --output=locale/$$FILENAME_NO_EXT.pot \
		         --language=Perl                       \
		         --from-code=UTF-8                     \
		         --join-existing                       \
		         --add-comments                        \
		         --keyword=_T                          \
		         --package-name=$$FILENAME_NO_EXT      \
		                                               \
		         $$FILEPATH;                           \
	done

# Generate PO files.
# The target silently overwrites existing files, which edited manually by 
# translators, that's why I commented out this code.
# Uncomment only for generating new language PO for the first time.
#po_init: pot
#	msginit --input=locale/messages.pot                     \
#	        --output-file=locale/ru/LC_MESSAGES/messages.po \
#	        --locale=ru_RU.UTF-8
#	
#	# Perl plugin sources
#	for FILEPATH in $(PERL_PLUGINS);                                     \
#	do                                                                   \
#		FILENAME=`basename $$FILEPATH`;                                  \
#		FILENAME_NO_EXT=$${FILENAME%.*};                                 \
#		                                                                 \
#		msginit --input=locale/$$FILENAME_NO_EXT.pot                     \
#		        --output-file=locale/ru/LC_MESSAGES/$$FILENAME_NO_EXT.po \
#		        --locale=ru_RU.UTF-8;                                    \
#	done

# Update PO files
po_upd: pot
	# Perl sources
	msgmerge --update                          \
	         --backup=off                      \
	         --sort-by-file                    \
	                                           \
	         locale/ru/LC_MESSAGES/messages.po \
	         locale/messages.pot
	
	# Perl plugin sources
	for FILEPATH in $(PERL_PLUGINS);                        \
	do                                                      \
		FILENAME=`basename $$FILEPATH`;                     \
		FILENAME_NO_EXT=$${FILENAME%.*};                    \
		                                                    \
		msgmerge --update                                   \
	             --backup=off                               \
	             --sort-by-file                             \
	                                                        \
	             locale/ru/LC_MESSAGES/$$FILENAME_NO_EXT.po \
	             locale/$$FILENAME_NO_EXT.pot;              \
	done

# Generate MO files
mo: po_upd
	# Perl sources
	msgfmt --output-file=locale/ru/LC_MESSAGES/messages.mo \
	                                                       \
	       locale/ru/LC_MESSAGES/messages.po
	
	# Perl plugin sources
	for FILEPATH in $(PERL_PLUGINS);                                    \
	do                                                                  \
		FILENAME=`basename $$FILEPATH`;                                 \
		FILENAME_NO_EXT=$${FILENAME%.*};                                \
		                                                                \
		msgfmt --output-file=locale/ru/LC_MESSAGES/$$FILENAME_NO_EXT.mo \
	                                                                    \
	             locale/ru/LC_MESSAGES/$$FILENAME_NO_EXT.po;            \
	done

# Install all necessary files to system
install:
	mkdir -p '$(PREFIX)/'
	mkdir -p '$(PREFIX)/bin/'
	mkdir -p '$(PREFIX)/share/applications/'
	mkdir -p '$(PREFIX)/share/doc/maxi-liberation/'
	mkdir -p '$(PREFIX)/share/maxi-liberation/'
	mkdir -p '$(PREFIX)/share/pixmaps/'
	mkdir -p '$(PREFIX)/share/perl5/maxi-liberation/'
	mkdir -p '$(PREFIX)/share/perl5/maxi-liberation/plugins/'
	
	ln -sf ../share/perl5/maxi-liberation/src/maxi-liberation.pl '$(PREFIX)/bin/Maxi Liberation'
	ln -sf ../share/perl5/maxi-liberation/src/maxi-liberation.pl '$(PREFIX)/bin/maxi-liberation'
	ln -sf ../maxi-liberation/img/luna-libre.png '$(PREFIX)/share/pixmaps/maxi-liberation.png'
	
	$(CP)    maxi-liberation.desktop '$(PREFIX)/share/applications/'
	$(CP)    AUTHORS                 '$(PREFIX)/share/doc/maxi-liberation/'
	$(CP)    COPYING                 '$(PREFIX)/share/doc/maxi-liberation/'
	$(CP) -R img                     '$(PREFIX)/share/maxi-liberation/'
	$(CP)    LICENSE                 '$(PREFIX)/share/doc/maxi-liberation/'
	$(CP)    maxi-liberation.xrc     '$(PREFIX)/share/maxi-liberation/'
	$(CP)    README.md               '$(PREFIX)/share/doc/maxi-liberation/'
	$(CP) -R src                     '$(PREFIX)/share/perl5/maxi-liberation/'
	
	# Plugins
	for FILEPATH in $(PLUGINS);                                          \
	do                                                                   \
		$(CP) $$FILEPATH $(PREFIX)/share/perl5/maxi-liberation/plugins/; \
	done
	
	# Locales
	for FILEPATH in $(LOCALE_FILES);                                   \
	do                                                                 \
		DIRNAME=`dirname $$FILEPATH`;                                  \
		                                                               \
		mkdir -p $(PREFIX)/share/$$DIRNAME;                            \
		$(CP) $$FILEPATH $(PREFIX)/share/$$DIRNAME/maxi-liberation.mo; \
	done
	
	# Plugin locales
	for FILEPATH in $(LOCALE_PLUGINS);               \
	do                                               \
		DIRNAME=`dirname $$FILEPATH`;                \
		                                             \
		mkdir -p $(PREFIX)/share/$$DIRNAME;          \
		$(CP) $$FILEPATH $(PREFIX)/share/$$DIRNAME/; \
	done

# Deinstall files from system
deinstall:
	$(RM)    '$(PREFIX)/bin/Maxi Liberation'
	$(RM)    '$(PREFIX)/bin/maxi-liberation'
	$(RM)    '$(PREFIX)/share/applications/maxi-liberation.desktop'
	$(RM) -r '$(PREFIX)/share/doc/maxi-liberation/'
	$(RM) -r '$(PREFIX)/share/maxi-liberation/'
	$(RM) -r '$(PREFIX)/share/perl5/maxi-liberation/'
	$(RM)    '$(PREFIX)/share/pixmaps/maxi-liberation.png'
	
	# Locales
	for FILEPATH in $(LOCALE_FILES);                        \
	do                                                      \
		DIRNAME=`dirname $$FILEPATH`;                       \
		                                                    \
		                                                    \
		$(RM) $(PREFIX)/share/$$DIRNAME/maxi-liberation.mo; \
	done
	
	# Plugin locales
	for FILEPATH in $(LOCALE_PLUGINS);    \
	do                                    \
		$(RM) $(PREFIX)/share/$$FILEPATH; \
	done

# Prepare deb/rpm/tgz packages
linux:
	fakeroot dpkg --build debian maxi-liberation_$(VER).deb
	fakeroot alien --to-rpm maxi-liberation_$(VER).deb
	fakeroot alien --to-tgz maxi-liberation_$(VER).deb

# Prepare Homebrew package
macos:
	tar czf maxi-liberation_$(VER).tar.gz -C macos --exclude maxi-liberation.rb ./
	$(CP) maxi-liberation_$(VER).tar.gz $$(brew --cache macos/maxi-liberation.rb)

# Delete XRC resource files.
# Delete POT file.
# Delete MO files.
clean:
	$(RM) locale/**.pot
	$(RM) locale/**/LC_MESSAGES/*.mo
	$(RM) maxi-liberation-*.rpm
	$(RM) maxi-liberation-*.tgz
	$(RM) maxi-liberation_*.deb
	$(RM) maxi-liberation_*.tar.gz
	$(RM) maxi-liberation.xrc
	$(RM) plugins/*.xrc
