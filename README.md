# NAME

Maxi Liberation

# VERSION

0.27

# DESCRIPTION

EPUB/FB2/FB3 book meta tag editor.

Maxi means maximum expression of digital text form.
Liberation is library.

The program is Free/Libre and Open Source Software (FLOSS). There is only one
right software license in the world.

The program is cross-platform: GNU/Linux, UNIX, M$ Windows, Apple macOS are
all supported. It is based on wxWidgets library, so the GUI is native for each
OS, for example, it uses GTK+ for GNU, MFC for Windows, Cocoa for macOS. No
fucking control emulation like Qt or Java does.

The program has been inspired and influenced by Ex Falso, popular meta tag
editor for audio files. Many thanks to its authors! Used it regularly for my
music collection.

The program is written in Perl. All modules like ZIP or XML are just bindings
to C dynamic libraries, so it is fast as C anyway.

Now EPUB, FB2 and FB3 formats are supported. AZW/MOBI are planned.

# MASCOT

![Alt Text](img/luna-libre.png "Maxi Liberation mascot")

**Luna butterfly** is a project mascot.

whereideasoverlap is designer of original 16x16 icon.

# SCREENSHOTS

![Alt Text](doc/screenshot-linux.png "Maxi Liberation screenshot under GNU/Linux")
*Maxi Liberation under GNU/Linux*

![Alt Text](doc/screenshot-win.png "Maxi Liberation screenshot under M$ Windows")
*Maxi Liberation under M$ Windows*

![Alt Text](doc/screenshot-macos.png "Maxi Liberation screenshot under Apple macOS")
*Maxi Liberation under Apple macOS*

# SCREENCAST

![Alt Text](doc/screencast.webm "Maxi Liberation screencast")

# INSTALL

INSTALL file describes how to install the Maxi Liberation from scratch.

You do not need it to use Maxi Liberation.

Download binaries and skip all the INSTALL instructions:

+ **Deb-based GNU/Linux distro**

  ![Alt Text](doc/logo-debian.png "Logo Debian") http://www.lovecry.pt/dl/maxi-liberation_0.27.deb

+ **RPM-based GNU/Linux distro**

  ![Alt Text](doc/logo-redhat.png "Logo Red Hat") http://www.lovecry.pt/dl/maxi-liberation-0.27-2.noarch.rpm

+ **Slackware and source-based GNU/Linux distros**

  ![Alt Text](doc/logo-slackware.png "Logo Slackware") http://www.lovecry.pt/dl/maxi-liberation-0.27.tgz

+ **M$ Windows**

  TODO

+ **Apple macOS**

  ![Alt Text](doc/logo-macos.png "Logo macOS")

      $ wget https://gitlab.com/neva_blyad/maxi-liberation/-/blob/0.27/macos/maxi-liberation.rb 
      $ brew install --verbose maxi-liberation.rb
      $ cpan -T -i Archive::Zip Config::Tiny File::Find::Rule File::HomeDir  \
            Locale::gettext Log::Log4perl MIME::Types Path::Tiny Tie::IxHash \
            XML::LibXML Wx
      $ cp -R '/usr/local/Cellar/maxi-liberation/0.27/Maxi Liberation.app' /Applications/

# COPYRIGHT AND LICENSE

    Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                       <neva_blyad@lovecri.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# AUTHORS

    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light

# WEB PAGE

http://www.lovecry.pt/maxi-liberation/
